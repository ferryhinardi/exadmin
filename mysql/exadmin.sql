-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 13, 2015 at 07:57 PM
-- Server version: 5.5.42-cll
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ferryhin_exmedia`
--

DELIMITER $$
--
-- Procedures
--
$$

$$

$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Company`
--

CREATE TABLE IF NOT EXISTS `Company` (
  `CompanyID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(200) DEFAULT NULL,
  `CompanyProfile` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LtLeaveType`
--

CREATE TABLE IF NOT EXISTS `LtLeaveType` (
  `LeaveTypeID` int(11) NOT NULL,
  `LeaveDescription` varchar(200) NOT NULL,
  PRIMARY KEY (`LeaveTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsClass`
--

CREATE TABLE IF NOT EXISTS `MsClass` (
  `ClassID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` varchar(100) NOT NULL,
  PRIMARY KEY (`ClassID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsCourse`
--

CREATE TABLE IF NOT EXISTS `MsCourse` (
  `CourseID` int(11) NOT NULL AUTO_INCREMENT,
  `FileID` varchar(50) NOT NULL,
  `CourseName` varchar(200) NOT NULL,
  PRIMARY KEY (`CourseID`,`FileID`),
  KEY `fk_MsCourse_TrFile1_idx` (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsLeaveReason`
--

CREATE TABLE IF NOT EXISTS `MsLeaveReason` (
  `LeaveID` int(11) NOT NULL AUTO_INCREMENT,
  `LeaveName` varchar(100) NOT NULL,
  PRIMARY KEY (`LeaveID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsLog`
--

CREATE TABLE IF NOT EXISTS `MsLog` (
  `LogID` varchar(100) NOT NULL,
  `AuditedUser` varchar(15) DEFAULT NULL,
  `AuditedTime` datetime DEFAULT NULL,
  `AuditedActivity` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsMenu`
--

CREATE TABLE IF NOT EXISTS `MsMenu` (
  `MenuID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(200) NOT NULL,
  `MenuLevel` int(11) DEFAULT NULL,
  `MenuHeader` int(11) DEFAULT NULL,
  `MenuIndex` int(11) DEFAULT NULL,
  `MenuUrl` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`MenuID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `MsMenu`
--

INSERT INTO `MsMenu` (`MenuID`, `MenuName`, `MenuLevel`, `MenuHeader`, `MenuIndex`, `MenuUrl`) VALUES
(1, 'Administrasi', 1, NULL, 1, NULL),
(2, 'Persiapan', 1, NULL, 2, NULL),
(3, 'Presentasi', 1, NULL, 3, NULL),
(4, 'Evaluasi', 1, NULL, 4, NULL),
(5, 'Master', 2, 1, 1, NULL),
(6, 'Materi', 2, 2, 1, NULL),
(7, 'Administrasi', 2, 2, 2, NULL),
(8, 'Belajar', 2, 3, 1, NULL),
(9, 'Ulangan', 2, 3, 2, NULL),
(10, 'Nilai', 2, 4, 1, NULL),
(11, 'Menu Management', 3, 0, 1, '#menu-management'),
(12, 'Menu Access', 3, 0, 2, '#menu-access');

-- --------------------------------------------------------

--
-- Table structure for table `MsPermission`
--

CREATE TABLE IF NOT EXISTS `MsPermission` (
  `PermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(15) NOT NULL,
  `LeaveTypeID` int(11) NOT NULL,
  `LeaveID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  PRIMARY KEY (`PermissionID`,`UserID`,`LeaveTypeID`,`LeaveID`),
  KEY `fk_MsPermission_MsUser1_idx` (`UserID`),
  KEY `fk_MsPermission_LtLeaveType1_idx` (`LeaveTypeID`),
  KEY `fk_MsPermission_MsLeaveReason1_idx` (`LeaveID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsRole`
--

CREATE TABLE IF NOT EXISTS `MsRole` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(100) NOT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsScore`
--

CREATE TABLE IF NOT EXISTS `MsScore` (
  `ScoreID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeScoreID` int(11) NOT NULL,
  `Score` int(11) DEFAULT NULL,
  PRIMARY KEY (`ScoreID`,`TypeScoreID`),
  KEY `fk_MsScore_TypeScore1_idx` (`TypeScoreID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsSession`
--

CREATE TABLE IF NOT EXISTS `MsSession` (
  `SessionID` varchar(50) NOT NULL,
  `UserID` varchar(15) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `IsLogout` bit(1) DEFAULT NULL,
  `LogoutTime` datetime DEFAULT NULL,
  PRIMARY KEY (`SessionID`,`UserID`),
  KEY `fk_MsSession_MsUser1_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsShift`
--

CREATE TABLE IF NOT EXISTS `MsShift` (
  `ShiftID` int(11) NOT NULL,
  `ShiftName` varchar(100) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `StartTime` time DEFAULT NULL,
  `EndTime` time DEFAULT NULL,
  PRIMARY KEY (`ShiftID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsTerm`
--

CREATE TABLE IF NOT EXISTS `MsTerm` (
  `YearID` int(11) NOT NULL AUTO_INCREMENT,
  `YearName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsUser`
--

CREATE TABLE IF NOT EXISTS `MsUser` (
  `UserID` varchar(15) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `YearID` int(11) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `BirthPlace` varchar(100) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` char(1) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`UserID`,`YearID`),
  KEY `fk_MsUser_MsTerm1_idx` (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrAttendance`
--

CREATE TABLE IF NOT EXISTS `TrAttendance` (
  `AttendanceID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(15) NOT NULL,
  `YearID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `StartTime` time DEFAULT NULL,
  `EndTime` time DEFAULT NULL,
  PRIMARY KEY (`AttendanceID`,`UserID`,`YearID`),
  KEY `fk_TrAttendance_MsUser1_idx` (`UserID`),
  KEY `fk_TrAttendance_MsTerm1_idx` (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrFile`
--

CREATE TABLE IF NOT EXISTS `TrFile` (
  `FileID` varchar(50) NOT NULL,
  `FileName` varchar(100) DEFAULT NULL,
  `FileURL` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrRoleMenu`
--

CREATE TABLE IF NOT EXISTS `TrRoleMenu` (
  `RoleID` int(11) NOT NULL,
  `MenuID` int(11) NOT NULL,
  PRIMARY KEY (`RoleID`,`MenuID`),
  KEY `fk_TrRoleMenu_MsMenu1_idx` (`MenuID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrRoleUser`
--

CREATE TABLE IF NOT EXISTS `TrRoleUser` (
  `UserID` varchar(15) NOT NULL,
  `RoleID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`RoleID`),
  KEY `fk_TrRoleUser_MsUser_idx` (`UserID`),
  KEY `fk_TrRoleUser_MsRole1_idx` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrSchedule`
--

CREATE TABLE IF NOT EXISTS `TrSchedule` (
  `ScheduleID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(15) NOT NULL,
  `ClassID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `ShiftID` int(11) NOT NULL,
  PRIMARY KEY (`ScheduleID`,`UserID`,`ClassID`,`CourseID`,`ShiftID`),
  KEY `fk_TrSchedule_MsUser1_idx` (`UserID`),
  KEY `fk_TrSchedule_MsClass1_idx` (`ClassID`),
  KEY `fk_TrSchedule_MsCourse1_idx` (`CourseID`),
  KEY `fk_TrSchedule_MsShift1_idx` (`ShiftID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrScore`
--

CREATE TABLE IF NOT EXISTS `TrScore` (
  `UserID` varchar(15) NOT NULL,
  `ScoreID` int(11) NOT NULL,
  `ClassID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  PRIMARY KEY (`UserID`,`ScoreID`,`ClassID`,`CourseID`),
  KEY `fk_TrScore_MsScore1_idx` (`ScoreID`),
  KEY `fk_TrScore_MsClass1_idx` (`ClassID`),
  KEY `fk_TrScore_MsCourse1_idx` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrUserClass`
--

CREATE TABLE IF NOT EXISTS `TrUserClass` (
  `ClassID` int(11) NOT NULL,
  `UserID` varchar(15) NOT NULL,
  PRIMARY KEY (`ClassID`,`UserID`),
  KEY `fk_TrUserClass_MsUser1_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TypeScore`
--

CREATE TABLE IF NOT EXISTS `TypeScore` (
  `TypeScoreID` int(11) NOT NULL AUTO_INCREMENT,
  `Type` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`TypeScoreID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `MsCourse`
--
ALTER TABLE `MsCourse`
  ADD CONSTRAINT `fk_MsCourse_TrFile1` FOREIGN KEY (`FileID`) REFERENCES `TrFile` (`FileID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsPermission`
--
ALTER TABLE `MsPermission`
  ADD CONSTRAINT `fk_MsPermission_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_MsPermission_LtLeaveType1` FOREIGN KEY (`LeaveTypeID`) REFERENCES `LtLeaveType` (`LeaveTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_MsPermission_MsLeaveReason1` FOREIGN KEY (`LeaveID`) REFERENCES `MsLeaveReason` (`LeaveID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsScore`
--
ALTER TABLE `MsScore`
  ADD CONSTRAINT `fk_MsScore_TypeScore1` FOREIGN KEY (`TypeScoreID`) REFERENCES `TypeScore` (`TypeScoreID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsSession`
--
ALTER TABLE `MsSession`
  ADD CONSTRAINT `fk_MsSession_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsUser`
--
ALTER TABLE `MsUser`
  ADD CONSTRAINT `fk_MsUser_MsTerm1` FOREIGN KEY (`YearID`) REFERENCES `MsTerm` (`YearID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrAttendance`
--
ALTER TABLE `TrAttendance`
  ADD CONSTRAINT `fk_TrAttendance_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrAttendance_MsTerm1` FOREIGN KEY (`YearID`) REFERENCES `MsTerm` (`YearID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrRoleMenu`
--
ALTER TABLE `TrRoleMenu`
  ADD CONSTRAINT `fk_TrRoleMenu_MsRole1` FOREIGN KEY (`RoleID`) REFERENCES `MsRole` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrRoleMenu_MsMenu1` FOREIGN KEY (`MenuID`) REFERENCES `MsMenu` (`MenuID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrRoleUser`
--
ALTER TABLE `TrRoleUser`
  ADD CONSTRAINT `fk_TrRoleUser_MsUser` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrRoleUser_MsRole1` FOREIGN KEY (`RoleID`) REFERENCES `MsRole` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrSchedule`
--
ALTER TABLE `TrSchedule`
  ADD CONSTRAINT `fk_TrSchedule_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrSchedule_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrSchedule_MsCourse1` FOREIGN KEY (`CourseID`) REFERENCES `MsCourse` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrSchedule_MsShift1` FOREIGN KEY (`ShiftID`) REFERENCES `MsShift` (`ShiftID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrScore`
--
ALTER TABLE `TrScore`
  ADD CONSTRAINT `fk_TrScore_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsScore1` FOREIGN KEY (`ScoreID`) REFERENCES `MsScore` (`ScoreID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsCourse1` FOREIGN KEY (`CourseID`) REFERENCES `MsCourse` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrUserClass`
--
ALTER TABLE `TrUserClass`
  ADD CONSTRAINT `fk_TrUserClass_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrUserClass_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
