-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema ferryhin_exmedia
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema ferryhin_exmedia
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `ferryhin_exmedia` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `ferryhin_exmedia` ;

-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsUser` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsUser` (
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `Password` VARCHAR(200) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`UserID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsRole`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsRole` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsRole` (
  `RoleID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `RoleName` VARCHAR(100) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`RoleID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrRoleUser`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrRoleUser` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrRoleUser` (
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `RoleID` INT(11) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`UserID`, `RoleID`)  COMMENT '',
  INDEX `fk_TrRoleUser_MsUser_idx` (`UserID` ASC)  COMMENT '',
  INDEX `fk_TrRoleUser_MsRole1_idx` (`RoleID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrRoleUser_MsUser`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrRoleUser_MsRole1`
    FOREIGN KEY (`RoleID`)
    REFERENCES `ferryhin_exmedia`.`MsRole` (`RoleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsMenu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsMenu` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsMenu` (
  `MenuID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `MenuName` VARCHAR(200) NOT NULL COMMENT '',
  `MenuLevel` INT NULL COMMENT '',
  `MenuHeader` INT NULL COMMENT '',
  `MenuIndex` INT NULL COMMENT '',
  `MenuWebPart` VARCHAR(200) NULL DEFAULT NULL COMMENT '',
  `MenuUrl` VARCHAR(100) NULL DEFAULT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`MenuID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrRoleMenu`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrRoleMenu` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrRoleMenu` (
  `RoleID` INT(11) NOT NULL COMMENT '',
  `MenuID` INT(11) NOT NULL COMMENT '',
  `View` INT NULL COMMENT '',
  `Create` INT NULL COMMENT '',
  `Update` INT NULL COMMENT '',
  `Delete` INT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`RoleID`, `MenuID`)  COMMENT '',
  INDEX `fk_TrRoleMenu_MsMenu1_idx` (`MenuID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrRoleMenu_MsRole1`
    FOREIGN KEY (`RoleID`)
    REFERENCES `ferryhin_exmedia`.`MsRole` (`RoleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrRoleMenu_MsMenu1`
    FOREIGN KEY (`MenuID`)
    REFERENCES `ferryhin_exmedia`.`MsMenu` (`MenuID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`Company`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`Company` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`Company` (
  `CompanyID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `CompanyName` VARCHAR(200) NULL COMMENT '',
  `CompanyProfile` VARCHAR(200) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`CompanyID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsShift`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsShift` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsShift` (
  `ShiftID` INT(11) NOT NULL COMMENT '',
  `ShiftName` VARCHAR(100) NULL COMMENT '',
  `Date` DATE NULL COMMENT '',
  `StartTime` TIME NULL COMMENT '',
  `EndTime` TIME NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`ShiftID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsClass`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsClass` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsClass` (
  `ClassID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `ClassName` VARCHAR(100) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`ClassID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsCourse`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsCourse` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsCourse` (
  `CourseID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `CourseName` VARCHAR(200) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`CourseID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsTerm`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsTerm` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsTerm` (
  `YearID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `YearName` VARCHAR(50) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`YearID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrCourseClass`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrCourseClass` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrCourseClass` (
  `CourseClassID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `ClassID` INT(11) NOT NULL COMMENT '',
  `CourseID` INT(11) NOT NULL COMMENT '',
  `YearID` INT(11) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`CourseClassID`, `ClassID`, `CourseID`, `YearID`)  COMMENT '',
  INDEX `fk_TrCourseClass_MsClass1_idx` (`ClassID` ASC)  COMMENT '',
  INDEX `fk_TrCourseClass_MsCourse1_idx` (`CourseID` ASC)  COMMENT '',
  INDEX `fk_TrCourseClass_MsTerm1_idx` (`YearID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrCourseClass_MsClass1`
    FOREIGN KEY (`ClassID`)
    REFERENCES `ferryhin_exmedia`.`MsClass` (`ClassID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrCourseClass_MsCourse1`
    FOREIGN KEY (`CourseID`)
    REFERENCES `ferryhin_exmedia`.`MsCourse` (`CourseID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrCourseClass_MsTerm1`
    FOREIGN KEY (`YearID`)
    REFERENCES `ferryhin_exmedia`.`MsTerm` (`YearID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrSchedule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrSchedule` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrSchedule` (
  `ScheduleID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `ShiftID` INT(11) NOT NULL COMMENT '',
  `CourseClassID` INT(11) NOT NULL COMMENT '',
  `IsVerified` INT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`ScheduleID`, `ShiftID`, `CourseClassID`)  COMMENT '',
  INDEX `fk_TrSchedule_MsShift1_idx` (`ShiftID` ASC)  COMMENT '',
  INDEX `fk_TrSchedule_TrCourseClass1_idx` (`CourseClassID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrSchedule_MsShift1`
    FOREIGN KEY (`ShiftID`)
    REFERENCES `ferryhin_exmedia`.`MsShift` (`ShiftID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrSchedule_TrCourseClass1`
    FOREIGN KEY (`CourseClassID`)
    REFERENCES `ferryhin_exmedia`.`TrCourseClass` (`CourseClassID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrAttendance`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrAttendance` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrAttendance` (
  `AttendanceID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `ScheduleID` INT(11) NOT NULL COMMENT '',
  `Date` DATE NULL COMMENT '',
  `Status` VARCHAR(45) NULL COMMENT '',
  `StartTime` TIME NULL COMMENT '',
  `EndTime` TIME NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`AttendanceID`, `UserID`, `ScheduleID`)  COMMENT '',
  INDEX `fk_TrAttendance_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  INDEX `fk_TrAttendance_TrSchedule1_idx` (`ScheduleID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrAttendance_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrAttendance_TrSchedule1`
    FOREIGN KEY (`ScheduleID`)
    REFERENCES `ferryhin_exmedia`.`TrSchedule` (`ScheduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsSession`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsSession` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsSession` (
  `SessionID` VARCHAR(50) NOT NULL COMMENT '',
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `CreatedDate` DATETIME NOT NULL COMMENT '',
  `IsLogout` BIT(1) NULL COMMENT '',
  `LogoutTime` DATETIME NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`SessionID`, `UserID`)  COMMENT '',
  INDEX `fk_MsSession_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsSession_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`LtLeaveType`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`LtLeaveType` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`LtLeaveType` (
  `LeaveTypeID` INT(11) NOT NULL COMMENT '',
  `LeaveDescription` VARCHAR(200) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`LeaveTypeID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsLeaveReason`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsLeaveReason` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsLeaveReason` (
  `LeaveID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `LeaveName` VARCHAR(100) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`LeaveID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsPermission`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsPermission` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsPermission` (
  `PermissionID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `LeaveTypeID` INT(11) NOT NULL COMMENT '',
  `LeaveID` INT(11) NOT NULL COMMENT '',
  `Date` DATE NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`PermissionID`, `UserID`, `LeaveTypeID`, `LeaveID`)  COMMENT '',
  INDEX `fk_MsPermission_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  INDEX `fk_MsPermission_LtLeaveType1_idx` (`LeaveTypeID` ASC)  COMMENT '',
  INDEX `fk_MsPermission_MsLeaveReason1_idx` (`LeaveID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsPermission_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_MsPermission_LtLeaveType1`
    FOREIGN KEY (`LeaveTypeID`)
    REFERENCES `ferryhin_exmedia`.`LtLeaveType` (`LeaveTypeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_MsPermission_MsLeaveReason1`
    FOREIGN KEY (`LeaveID`)
    REFERENCES `ferryhin_exmedia`.`MsLeaveReason` (`LeaveID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrUserClass`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrUserClass` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrUserClass` (
  `ClassID` INT(11) NOT NULL COMMENT '',
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`ClassID`, `UserID`)  COMMENT '',
  INDEX `fk_TrUserClass_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrUserClass_MsClass1`
    FOREIGN KEY (`ClassID`)
    REFERENCES `ferryhin_exmedia`.`MsClass` (`ClassID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrUserClass_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsLog`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsLog` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsLog` (
  `LogID` VARCHAR(100) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(15) NULL COMMENT '',
  `AuditedTime` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(100) NULL COMMENT '',
  PRIMARY KEY (`LogID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TypeScore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TypeScore` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TypeScore` (
  `TypeScoreID` INT(11) NOT NULL AUTO_INCREMENT COMMENT '',
  `CourseID` INT(11) NOT NULL COMMENT '',
  `Type` VARCHAR(100) NOT NULL COMMENT '',
  `Weight` DECIMAL NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`TypeScoreID`, `CourseID`)  COMMENT '',
  INDEX `fk_TypeScore_MsCourse1_idx` (`CourseID` ASC)  COMMENT '',
  CONSTRAINT `fk_TypeScore_MsCourse1`
    FOREIGN KEY (`CourseID`)
    REFERENCES `ferryhin_exmedia`.`MsCourse` (`CourseID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsScore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsScore` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsScore` (
  `ScoreID` INT(11) NOT NULL COMMENT '',
  `TypeScoreID` INT(11) NOT NULL COMMENT '',
  `Score` DECIMAL NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`ScoreID`, `TypeScoreID`)  COMMENT '',
  INDEX `fk_MsScore_TypeScore1_idx` (`TypeScoreID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsScore_TypeScore1`
    FOREIGN KEY (`TypeScoreID`)
    REFERENCES `ferryhin_exmedia`.`TypeScore` (`TypeScoreID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrScore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrScore` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrScore` (
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `ScoreID` INT(11) NOT NULL COMMENT '',
  `ClassID` INT(11) NOT NULL COMMENT '',
  `CourseID` INT(11) NOT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`UserID`, `ScoreID`, `ClassID`, `CourseID`)  COMMENT '',
  INDEX `fk_TrScore_MsScore1_idx` (`ScoreID` ASC)  COMMENT '',
  INDEX `fk_TrScore_MsClass1_idx` (`ClassID` ASC)  COMMENT '',
  INDEX `fk_TrScore_MsCourse1_idx` (`CourseID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrScore_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrScore_MsScore1`
    FOREIGN KEY (`ScoreID`)
    REFERENCES `ferryhin_exmedia`.`MsScore` (`ScoreID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrScore_MsClass1`
    FOREIGN KEY (`ClassID`)
    REFERENCES `ferryhin_exmedia`.`MsClass` (`ClassID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TrScore_MsCourse1`
    FOREIGN KEY (`CourseID`)
    REFERENCES `ferryhin_exmedia`.`MsCourse` (`CourseID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrFile`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrFile` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrFile` (
  `FileID` VARCHAR(50) NOT NULL COMMENT '',
  `FileName` VARCHAR(100) NULL COMMENT '',
  `FileURL` VARCHAR(200) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`FileID`)  COMMENT '')
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`TrNote`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`TrNote` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`TrNote` (
  `NoteID` INT(11) NOT NULL COMMENT '',
  `Description` TEXT NULL COMMENT '',
  `ScheduleID` INT(11) NOT NULL COMMENT '',
  `UserID` VARCHAR(15) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`NoteID`, `ScheduleID`)  COMMENT '',
  INDEX `fk_TrNote_TrSchedule1_idx` (`ScheduleID` ASC)  COMMENT '',
  CONSTRAINT `fk_TrNote_TrSchedule1`
    FOREIGN KEY (`ScheduleID`)
    REFERENCES `ferryhin_exmedia`.`TrSchedule` (`ScheduleID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsTeacher`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsTeacher` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsTeacher` (
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `YearID` INT(11) NOT NULL COMMENT '',
  `FullName` VARCHAR(200) NOT NULL COMMENT '',
  `BirthPlace` VARCHAR(100) NULL COMMENT '',
  `BirthDate` DATE NULL COMMENT '',
  `Gender` CHAR(1) NULL COMMENT '',
  `Address` VARCHAR(500) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`UserID`)  COMMENT '',
  INDEX `fk_MsTeacher_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsTeacher_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '		';


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsStudent`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsStudent` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsStudent` (
  `UserID` VARCHAR(15) NOT NULL COMMENT '',
  `YearID` INT(11) NOT NULL COMMENT '',
  `FullName` VARCHAR(200) NOT NULL COMMENT '',
  `BirthPlace` VARCHAR(100) NULL COMMENT '',
  `BirthDate` DATE NULL COMMENT '',
  `Gender` CHAR(1) NULL COMMENT '',
  `Address` VARCHAR(500) NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`UserID`)  COMMENT '',
  INDEX `fk_MsStudent_MsUser1_idx` (`UserID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsStudent_MsUser1`
    FOREIGN KEY (`UserID`)
    REFERENCES `ferryhin_exmedia`.`MsUser` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
COMMENT = '		';


-- -----------------------------------------------------
-- Table `ferryhin_exmedia`.`MsMaterial`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `ferryhin_exmedia`.`MsMaterial` ;

CREATE TABLE IF NOT EXISTS `ferryhin_exmedia`.`MsMaterial` (
  `MateriID` INT NOT NULL AUTO_INCREMENT COMMENT '',
  `FileID` VARCHAR(50) NULL COMMENT '',
  `CourseID` INT(11) NOT NULL COMMENT '',
  `Question` NVARCHAR(5000) NULL COMMENT '',
  `Option` INT NULL COMMENT '',
  `Answer` INT NULL COMMENT '',
  `AuditedUser` VARCHAR(100) NULL COMMENT '',
  `AuditedDate` DATETIME NULL COMMENT '',
  `AuditedActivity` VARCHAR(2) NULL COMMENT '',
  PRIMARY KEY (`MateriID`, `CourseID`)  COMMENT '',
  INDEX `fk_MsMaterial_TrFile1_idx` (`FileID` ASC)  COMMENT '',
  INDEX `fk_MsMaterial_MsCourse1_idx` (`CourseID` ASC)  COMMENT '',
  CONSTRAINT `fk_MsMaterial_TrFile1`
    FOREIGN KEY (`FileID`)
    REFERENCES `ferryhin_exmedia`.`TrFile` (`FileID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_MsMaterial_MsCourse1`
    FOREIGN KEY (`CourseID`)
    REFERENCES `ferryhin_exmedia`.`MsCourse` (`CourseID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
