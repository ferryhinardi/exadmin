-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Nov 07, 2015 at 04:45 PM
-- Server version: 10.0.22-MariaDB
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ferryhin_exmedia`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_addmenu`(menuIDParam INT, menuNameParam VARCHAR(200), menuLevelParam INT, menuHeaderParam INT, MenuWebPartParam VARCHAR(200), menuIndexParam INT, menuUrlParam VARCHAR(100))
BEGIN
	-- DECLARE EXIT HANDLER FOR SQLEXCEPTION SET has_error = 1;
    IF NOT EXISTS (SELECT NULL FROM ferryhin_exmedia.MsMenu WHERE MenuID = menuIDParam AND AuditedActivity <> 'D') 
    THEN
	BEGIN
		INSERT INTO ferryhin_exmedia.MsMenu
		(MenuName, MenuLevel, MenuHeader, MenuIndex, MenuWebPart, MenuUrl, AuditedDate, AuditedActivity)
		VALUE
		(menuNameParam, menuLevelParam, menuHeaderParam, menuIndexParam, MenuWebPartParam, menuUrlParam, SYSDATE(), 'A');
		SELECT 1 AS Result;
	END;
    ELSE
		BEGIN
			SET SQL_SAFE_UPDATES=0;
			UPDATE ferryhin_exmedia.MsMenu
			SET MenuName = menuNameParam,
				MenuLevel = menuLevelParam,  
				MenuHeader = menuHeaderParam, 
				MenuIndex = menuIndexParam, 
                MenuWebPart = MenuWebPartParam,
				MenuUrl = menuUrlParam,
                AuditedDate = SYSDATE()
			WHERE MenuID = menuIDParam;
		END;
		SELECT 2 AS Result;
	END IF;
    -- SELECT has_error;
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_add_accessmenurole`(RoleIDParam INT, MenuIDParam INT, ViewParam INT, CreateParam INT, UpdateParam INT, DeleteParam INT)
BEGIN
	IF NOT EXISTS(SELECT NULL FROM ferryhin_exmedia.TrRoleMenu WHERE RoleID = RoleIDParam AND MenuID = MenuIDParam) THEN
    BEGIN
		INSERT INTO `ferryhin_exmedia`.`TrRoleMenu`
		(`RoleID`, `MenuID`, `View`, `Create`, `Update`,`Delete`)
		VALUES
		(RoleIDParam, MenuIDParam, ViewParam, CreateParam, UpdateParam, DeleteParam);
    END;
    ELSE
		BEGIN
			SET SQL_SAFE_UPDATES=0;
			UPDATE `ferryhin_exmedia`.`TrRoleMenu`
			SET
			`View` = ViewParam,
			`Create` = CreateParam,
			`Update` = UpdateParam,
			`Delete` = DeleteParam
			WHERE `RoleID` = RoleIDParam AND `MenuID` = MenuIDParam;
		END;
	END IF;
    SELECT 1 AS Result;
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_create_company`(CompanyNameParam VARCHAR(200), CompanyProfileParam VARCHAR(200))
BEGIN
	INSERT INTO `ferryhin_exmedia`.`Company`
	(`CompanyName`, `CompanyProfile`, `AuditedDate`, `AuditedActivity`)
	VALUES
	(CompanyNameParam, CompanyProfileParam, SYSDATE(),'A');
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_deletemenu`(MenuIDParam INT)
BEGIN
	IF EXISTS (SELECT NULL FROM ferryhin_exmedia.MsMenu WHERE MenuID = menuIDParam AND AuditedActivity <> 'D') THEN
    BEGIN
		UPDATE ferryhin_exmedia.MsMenu
        SET AuditedActivity = 'D'
        WHERE MenuID = menuIDParam;
		SELECT 1 AS Result;
    END;
    ELSE
		BEGIN
			SELECT 0 AS Result;
		END;
	END IF;
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_get_accessmenurole`(RoleIDParam INT, MenuHeaderParam INT)
BEGIN
	SELECT `MenuID`, `View`, `Create`, `Update`, `Delete`
    FROM `ferryhin_exmedia`.`TrRoleMenu`
    WHERE RoleID = RoleIDParam
    AND (MenuHeaderParam IS NULL OR 
		MenuID IN (SELECT MenuID FROM `ferryhin_exmedia`.`MsMenu` WHERE MenuHeader = MenuHeaderParam))
    ORDER BY `MenuID`;
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_showrole`(roleIDParam INT)
BEGIN
	SELECT RoleID, RoleName
    FROM ferryhin_exmedia.MsRole
    WHERE (roleIDParam IS NULL OR RoleID = roleIDParam)
    AND AuditedActivity <> 'D';
END$$

CREATE DEFINER=`ferryhin`@`localhost` PROCEDURE `sp_showmenu`(menuLevelParam INT, menuHeaderParam INT)
BEGIN
	SELECT 
		a.MenuID, 
        MenuName, 
        MenuLevel, 
        COALESCE(a.MenuHeader, '') AS MenuHeader, 
        COALESCE(MenuHeaderName, '') AS MenuHeaderName, 
        COALESCE(MenuWebPart, '') AS MenuWebPart, 
        MenuIndex, COALESCE(MenuUrl, '') AS MenuUrl
    FROM ferryhin_exmedia.MsMenu a
    LEFT JOIN (SELECT MenuID, MenuName AS MenuHeaderName, MenuHeader FROM ferryhin_exmedia.MsMenu) b
		ON a.MenuHeader = b.MenuID
    WHERE (menuLevelParam IS NULL OR MenuLevel = menuLevelParam)
    AND (menuHeaderParam IS NULL OR a.MenuHeader = menuHeaderParam)
    AND AuditedActivity <> 'D'
    ORDER BY MenuIndex;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Company`
--

CREATE TABLE IF NOT EXISTS `Company` (
  `CompanyID` int(11) NOT NULL AUTO_INCREMENT,
  `CompanyName` varchar(200) DEFAULT NULL,
  `CompanyProfile` varchar(200) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`CompanyID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LtLeaveType`
--

CREATE TABLE IF NOT EXISTS `LtLeaveType` (
  `LeaveTypeID` int(11) NOT NULL,
  `LeaveDescription` varchar(200) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`LeaveTypeID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsClass`
--

CREATE TABLE IF NOT EXISTS `MsClass` (
  `ClassID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` varchar(100) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ClassID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsCourse`
--

CREATE TABLE IF NOT EXISTS `MsCourse` (
  `CourseID` int(11) NOT NULL AUTO_INCREMENT,
  `CourseName` varchar(200) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsLeaveReason`
--

CREATE TABLE IF NOT EXISTS `MsLeaveReason` (
  `LeaveID` int(11) NOT NULL AUTO_INCREMENT,
  `LeaveName` varchar(100) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`LeaveID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsLog`
--

CREATE TABLE IF NOT EXISTS `MsLog` (
  `LogID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuID` int(11) DEFAULT NULL,
  `AuditedUser` varchar(15) DEFAULT NULL,
  `AuditedTime` datetime DEFAULT NULL,
  `AuditedActivity` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`LogID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsMenu`
--

CREATE TABLE IF NOT EXISTS `MsMenu` (
  `MenuID` int(11) NOT NULL AUTO_INCREMENT,
  `MenuName` varchar(200) NOT NULL,
  `MenuLevel` int(11) DEFAULT NULL,
  `MenuHeader` int(11) DEFAULT NULL,
  `MenuIndex` int(11) DEFAULT NULL,
  `MenuWebPart` varchar(200) DEFAULT NULL,
  `MenuUrl` varchar(100) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`MenuID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `MsMenu`
--

INSERT INTO `MsMenu` (`MenuID`, `MenuName`, `MenuLevel`, `MenuHeader`, `MenuIndex`, `MenuWebPart`, `MenuUrl`, `AuditedUser`, `AuditedDate`, `AuditedActivity`) VALUES
(1, 'Administrasi', 1, NULL, 1, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(2, 'Persiapan', 1, NULL, 2, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(3, 'Presentasi', 1, NULL, 3, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(4, 'Evaluasi', 1, NULL, 4, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(5, 'Master', 2, 1, 1, '', 'administrasi/master', 'Administrator', '2015-09-19 17:58:37', 'A'),
(6, 'Materi', 2, 2, 1, NULL, 'persiapan/materi', 'Administrator', '2015-09-15 22:55:57', 'A'),
(7, 'Administrasi', 2, 2, 2, '', 'persiapan/administrasi', 'Administrator', '2015-09-19 19:22:40', 'A'),
(8, 'Belajar', 2, 3, 1, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(9, 'Ulangan', 2, 3, 2, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(10, 'Nilai', 2, 4, 1, NULL, NULL, 'Administrator', '2015-09-13 20:33:28', 'A'),
(11, 'Menu Management', 3, 0, 1, NULL, '#menu-management', 'Administrator', '2015-09-13 20:33:28', 'A'),
(12, 'Menu Access', 3, 0, 2, NULL, '#menu-access', 'Administrator', '2015-09-13 20:33:28', 'A'),
(13, 'Profile Sekolah', 3, 5, 1, 'profile_school', '#profile-school', NULL, '2015-09-19 18:09:38', 'A'),
(14, 'Data Guru', 3, 5, 2, 'teacher_data', '#teacher-data', NULL, '2015-09-19 18:09:49', 'A'),
(15, 'Data Siswa', 3, 5, 3, 'student_data', '#student-data', NULL, '2015-09-19 18:09:59', 'A'),
(16, 'Data Kelas', 3, 5, 4, 'class_data', '#class-data', NULL, '2015-09-19 18:22:05', 'A'),
(17, 'Data Jadwal', 3, 5, 6, '', '', NULL, '2015-09-19 18:25:05', 'A'),
(18, 'Data Matapelajaran', 3, 5, 5, 'course_data', '#course-data', NULL, '2015-09-19 18:27:54', 'A'),
(19, 'Input Matapelajaran', 3, 6, 1, 'input_matapelajaran', '#input-matapelajaran', NULL, '2015-09-19 18:10:57', 'A'),
(20, 'Pengaturan Ujian', 3, 6, 2, NULL, '', NULL, NULL, 'A'),
(21, 'Absence', 3, 7, 1, 'absence', '#absence', NULL, '2015-09-19 19:17:26', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `MsPermission`
--

CREATE TABLE IF NOT EXISTS `MsPermission` (
  `PermissionID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(15) NOT NULL,
  `LeaveTypeID` int(11) NOT NULL,
  `LeaveID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`PermissionID`,`UserID`,`LeaveTypeID`,`LeaveID`),
  KEY `fk_MsPermission_MsUser1_idx` (`UserID`),
  KEY `fk_MsPermission_LtLeaveType1_idx` (`LeaveTypeID`),
  KEY `fk_MsPermission_MsLeaveReason1_idx` (`LeaveID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsRole`
--

CREATE TABLE IF NOT EXISTS `MsRole` (
  `RoleID` int(11) NOT NULL AUTO_INCREMENT,
  `RoleName` varchar(100) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`RoleID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `MsRole`
--

INSERT INTO `MsRole` (`RoleID`, `RoleName`, `AuditedUser`, `AuditedDate`, `AuditedActivity`) VALUES
(1, 'Super Admin', 'Administrator', '2015-09-14 00:00:00', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `MsScore`
--

CREATE TABLE IF NOT EXISTS `MsScore` (
  `ScoreID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeScoreID` int(11) NOT NULL,
  `Score` decimal(10,0) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ScoreID`,`TypeScoreID`),
  KEY `fk_MsScore_TypeScore_idx` (`TypeScoreID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsSession`
--

CREATE TABLE IF NOT EXISTS `MsSession` (
  `SessionID` varchar(50) NOT NULL,
  `UserID` varchar(15) NOT NULL,
  `CreatedDate` datetime NOT NULL,
  `IsLogout` bit(1) DEFAULT NULL,
  `LogoutTime` datetime DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`SessionID`,`UserID`),
  KEY `fk_MsSession_MsUser1_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsShift`
--

CREATE TABLE IF NOT EXISTS `MsShift` (
  `ShiftID` int(11) NOT NULL,
  `ShiftName` varchar(100) DEFAULT NULL,
  `Date` date DEFAULT NULL,
  `StartTime` time DEFAULT NULL,
  `EndTime` time DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ShiftID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `MsTerm`
--

CREATE TABLE IF NOT EXISTS `MsTerm` (
  `YearID` int(11) NOT NULL AUTO_INCREMENT,
  `YearName` varchar(50) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `MsUser`
--

CREATE TABLE IF NOT EXISTS `MsUser` (
  `UserID` varchar(15) NOT NULL,
  `Password` varchar(200) NOT NULL,
  `YearID` int(11) NOT NULL,
  `FullName` varchar(200) NOT NULL,
  `BirthPlace` varchar(100) DEFAULT NULL,
  `BirthDate` date DEFAULT NULL,
  `Gender` char(1) DEFAULT NULL,
  `Address` varchar(500) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`UserID`,`YearID`),
  KEY `fk_MsUser_MsTerm1_idx` (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrAttendance`
--

CREATE TABLE IF NOT EXISTS `TrAttendance` (
  `AttendanceID` int(11) NOT NULL AUTO_INCREMENT,
  `UserID` varchar(15) NOT NULL,
  `YearID` int(11) NOT NULL,
  `Date` date DEFAULT NULL,
  `Status` varchar(45) DEFAULT NULL,
  `StartTime` time DEFAULT NULL,
  `EndTime` time DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`AttendanceID`,`UserID`,`YearID`),
  KEY `fk_TrAttendance_MsUser1_idx` (`UserID`),
  KEY `fk_TrAttendance_MsTerm1_idx` (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrCourseClass`
--

CREATE TABLE IF NOT EXISTS `TrCourseClass` (
  `CourseClassID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `YearID` int(11) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`CourseClassID`,`ClassID`,`CourseID`,`YearID`),
  KEY `fk_TrCourseClass_MsClass1_idx` (`ClassID`),
  KEY `fk_TrCourseClass_MsCourse1_idx` (`CourseID`),
  KEY `fk_TrCourseClass_MsTerm1_idx` (`YearID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrFile`
--

CREATE TABLE IF NOT EXISTS `TrFile` (
  `FileID` varchar(50) NOT NULL,
  `FileName` varchar(100) DEFAULT NULL,
  `FileURL` varchar(200) DEFAULT NULL,
  `ScheduleID` int(11) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`FileID`,`ScheduleID`),
  KEY `fk_TrFile_TrSchedule1_idx` (`ScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrNote`
--

CREATE TABLE IF NOT EXISTS `TrNote` (
  `NoteID` int(11) NOT NULL,
  `Description` text,
  `ScheduleID` int(11) NOT NULL,
  `UserID` varchar(15) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`NoteID`,`ScheduleID`),
  KEY `fk_TrNote_TrSchedule1_idx` (`ScheduleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrRoleMenu`
--

CREATE TABLE IF NOT EXISTS `TrRoleMenu` (
  `RoleID` int(11) NOT NULL,
  `MenuID` int(11) NOT NULL,
  `View` int(11) DEFAULT NULL,
  `Create` int(11) DEFAULT NULL,
  `Update` int(11) DEFAULT NULL,
  `Delete` int(11) DEFAULT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`RoleID`,`MenuID`),
  KEY `fk_TrRoleMenu_MsMenu1_idx` (`MenuID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `TrRoleMenu`
--

INSERT INTO `TrRoleMenu` (`RoleID`, `MenuID`, `View`, `Create`, `Update`, `Delete`, `AuditedUser`, `AuditedDate`, `AuditedActivity`) VALUES
(1, 1, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 2, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 3, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 4, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 5, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 6, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 7, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 8, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 9, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 10, 1, 0, 0, 0, NULL, NULL, NULL),
(1, 11, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 12, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 13, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 14, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 15, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 16, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 17, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 18, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 19, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 20, 1, 1, 1, 1, NULL, NULL, NULL),
(1, 21, 1, 1, 1, 1, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `TrRoleUser`
--

CREATE TABLE IF NOT EXISTS `TrRoleUser` (
  `UserID` varchar(15) NOT NULL,
  `RoleID` int(11) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`UserID`,`RoleID`),
  KEY `fk_TrRoleUser_MsUser_idx` (`UserID`),
  KEY `fk_TrRoleUser_MsRole1_idx` (`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrSchedule`
--

CREATE TABLE IF NOT EXISTS `TrSchedule` (
  `ScheduleID` int(11) NOT NULL AUTO_INCREMENT,
  `ShiftID` int(11) NOT NULL,
  `CourseClassID` int(11) NOT NULL,
  `IsVerified` int(11) DEFAULT '0',
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ScheduleID`,`ShiftID`,`CourseClassID`),
  KEY `fk_TrSchedule_MsShift1_idx` (`ShiftID`),
  KEY `fk_TrSchedule_TrCourseClass1_idx` (`CourseClassID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `TrScore`
--

CREATE TABLE IF NOT EXISTS `TrScore` (
  `UserID` varchar(15) NOT NULL,
  `ScoreID` int(11) NOT NULL,
  `ClassID` int(11) NOT NULL,
  `CourseID` int(11) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`UserID`,`ScoreID`,`ClassID`,`CourseID`),
  KEY `fk_TrScore_MsScore1_idx` (`ScoreID`),
  KEY `fk_TrScore_MsClass1_idx` (`ClassID`),
  KEY `fk_TrScore_MsCourse1_idx` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TrUserClass`
--

CREATE TABLE IF NOT EXISTS `TrUserClass` (
  `ClassID` int(11) NOT NULL,
  `UserID` varchar(15) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`ClassID`,`UserID`),
  KEY `fk_TrUserClass_MsUser1_idx` (`UserID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `TypeScore`
--

CREATE TABLE IF NOT EXISTS `TypeScore` (
  `TypeScoreID` int(11) NOT NULL AUTO_INCREMENT,
  `CourseID` int(11) NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Weight` decimal(10,0) NOT NULL,
  `AuditedUser` varchar(100) DEFAULT NULL,
  `AuditedDate` datetime DEFAULT NULL,
  `AuditedActivity` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`TypeScoreID`),
  KEY `fk_TypeScore_MsCourse_idx` (`CourseID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `MsPermission`
--
ALTER TABLE `MsPermission`
  ADD CONSTRAINT `fk_MsPermission_LtLeaveType1` FOREIGN KEY (`LeaveTypeID`) REFERENCES `LtLeaveType` (`LeaveTypeID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_MsPermission_MsLeaveReason1` FOREIGN KEY (`LeaveID`) REFERENCES `MsLeaveReason` (`LeaveID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_MsPermission_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsScore`
--
ALTER TABLE `MsScore`
  ADD CONSTRAINT `fk_MsScore_TypeScore` FOREIGN KEY (`TypeScoreID`) REFERENCES `TypeScore` (`TypeScoreID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsSession`
--
ALTER TABLE `MsSession`
  ADD CONSTRAINT `fk_MsSession_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `MsUser`
--
ALTER TABLE `MsUser`
  ADD CONSTRAINT `fk_MsUser_MsTerm1` FOREIGN KEY (`YearID`) REFERENCES `MsTerm` (`YearID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrAttendance`
--
ALTER TABLE `TrAttendance`
  ADD CONSTRAINT `fk_TrAttendance_MsTerm1` FOREIGN KEY (`YearID`) REFERENCES `MsTerm` (`YearID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrAttendance_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrCourseClass`
--
ALTER TABLE `TrCourseClass`
  ADD CONSTRAINT `fk_TrCourseClass_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrCourseClass_MsCourse1` FOREIGN KEY (`CourseID`) REFERENCES `MsCourse` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrCourseClass_MsTerm1` FOREIGN KEY (`YearID`) REFERENCES `MsTerm` (`YearID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrFile`
--
ALTER TABLE `TrFile`
  ADD CONSTRAINT `fk_TrFile_TrSchedule1` FOREIGN KEY (`ScheduleID`) REFERENCES `TrSchedule` (`ScheduleID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrNote`
--
ALTER TABLE `TrNote`
  ADD CONSTRAINT `fk_TrNote_TrSchedule1` FOREIGN KEY (`ScheduleID`) REFERENCES `TrSchedule` (`ScheduleID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrRoleMenu`
--
ALTER TABLE `TrRoleMenu`
  ADD CONSTRAINT `fk_TrRoleMenu_MsMenu1` FOREIGN KEY (`MenuID`) REFERENCES `MsMenu` (`MenuID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrRoleMenu_MsRole1` FOREIGN KEY (`RoleID`) REFERENCES `MsRole` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrRoleUser`
--
ALTER TABLE `TrRoleUser`
  ADD CONSTRAINT `fk_TrRoleUser_MsRole1` FOREIGN KEY (`RoleID`) REFERENCES `MsRole` (`RoleID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrRoleUser_MsUser` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrSchedule`
--
ALTER TABLE `TrSchedule`
  ADD CONSTRAINT `fk_TrSchedule_MsShift1` FOREIGN KEY (`ShiftID`) REFERENCES `MsShift` (`ShiftID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrSchedule_TrCourseClass1` FOREIGN KEY (`CourseClassID`) REFERENCES `TrCourseClass` (`CourseClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrScore`
--
ALTER TABLE `TrScore`
  ADD CONSTRAINT `fk_TrScore_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsCourse1` FOREIGN KEY (`CourseID`) REFERENCES `MsCourse` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsScore1` FOREIGN KEY (`ScoreID`) REFERENCES `MsScore` (`ScoreID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrScore_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TrUserClass`
--
ALTER TABLE `TrUserClass`
  ADD CONSTRAINT `fk_TrUserClass_MsClass1` FOREIGN KEY (`ClassID`) REFERENCES `MsClass` (`ClassID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_TrUserClass_MsUser1` FOREIGN KEY (`UserID`) REFERENCES `MsUser` (`UserID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `TypeScore`
--
ALTER TABLE `TypeScore`
  ADD CONSTRAINT `fk_TypeScore_MsCourse` FOREIGN KEY (`CourseID`) REFERENCES `MsCourse` (`CourseID`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
