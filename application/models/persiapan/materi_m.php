<?php
class Materi_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getMateri($MateriIDParam, $CourseIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_get_materi(?,?,?)', array($MateriIDParam, $CourseIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveMateri($CourseIDParam, $YearIDParam, $MateriIDParam, $FileIDParam, $SubjectParam, $DescriptionParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_materi(?,?,?,?,?,?,?)', array($CourseIDParam, $YearIDParam, $MateriIDParam, $FileIDParam, $SubjectParam, $DescriptionParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteMateri($MateriIDParam) {
		$query = $this->db->query('CALL sp_delete_materi(?)', array($MateriIDParam));
		return $query->result_array();
	}
}