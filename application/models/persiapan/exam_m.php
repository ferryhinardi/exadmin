<?php
class Exam_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getExercise($CourseIDParam, $YearIDParam) {
		$exercise = array();
		$exam = $this->db->query('CALL sp_get_exam(?,?,?)', array(null, $CourseIDParam, $YearIDParam));
		$resultExam = $exam->result_array();

		$exam->next_result();
		$exam->free_result();

		foreach ($resultExam as $key => $value) {
			# code...
			$exercise[$key] = $value;
			$option = $this->db->query('CALL sp_get_option(?,?)', array(null, $value["ExamID"]));
			$resultOption = $option->result_array();
			$option->next_result();
			$option->free_result();
			$exercise[$key]["Option"] = $resultOption;
		}
		return $exercise;
	}

	function getExam($ExamIDParam, $CourseIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_get_exam(?,?,?)', array($ExamIDParam, $CourseIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveExam($CourseIDParam, $YearIDParam, $ExamIDParam, $FileIDParam, $NoParam, $QuestionParam, $AnswerParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_exam(?,?,?,?,?,?,?,?)', array($CourseIDParam, $YearIDParam, $ExamIDParam, $FileIDParam, $NoParam, $QuestionParam, $AnswerParam, $AuditedUserParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function deleteExam($ExamIDParam) {
		$query = $this->db->query('CALL sp_delete_exam(?)', array($ExamIDParam));
		return $query->result_array();
	}

	function getOption($OptionIDParam, $ExamIDParam) {
		$query = $this->db->query('CALL sp_get_option(?,?)', array($OptionIDParam, $ExamIDParam));
		return $query->result_array();
	}

	function optionExists($OptionIDParam) {
		$this->db->where('OptionID',$OptionIDParam);
		$query = $this->db->get('TrQuestionOption');
		if ($query->num_rows() > 0) return true;
		else return false;
	}

	function saveOption($OptionIDParam, $ExamIDParam, $FileIDParam, $OptionParam, $DescriptionParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_option(?,?,?,?,?,?)', array($OptionIDParam, $ExamIDParam, $FileIDParam, $OptionParam, $DescriptionParam, $AuditedUserParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function getScoreType($TypeScoreIDParam, $CourseIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_get_typescore(?,?,?)', array($TypeScoreIDParam, $CourseIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveScoreType($TypeScoreIDParam, $CourseParam, $YearIDParam, $TypeParam, $WeightParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_typescore(?,?,?,?,?,?)', array($TypeScoreIDParam, $CourseParam, $YearIDParam, $TypeParam, $WeightParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteScoreType($TypeScoreIDParam) {
		$query = $this->db->query('CALL sp_delete_typescore(?)', array($TypeScoreIDParam));
		return $query->result_array();
	}

	function saveScore($ScoreIDParam, $TypeScoreIDParam, $ScoreParam, $UserIDParam, $ClassIDParam, $CourseIDParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_score(?,?,?,?,?,?,?)', array($ScoreIDParam, $TypeScoreIDParam, $ScoreParam, $UserIDParam, $ClassIDParam, $CourseIDParam, $AuditedUserParam));
		return $query->result_array();
	}
}