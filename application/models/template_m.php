<?php
class Template_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getAll()
	{
		$this->db->select('table_name, table_type, engine');
		$query = $this->db->get('information_schema.tables');
		$this->db->where('table_schema', 'db5');
		$this->db->order_by("table_name", "desc");
		return $query->result_array();
	}
}