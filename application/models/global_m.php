<?php
class Global_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function checkLogin($UserIDParam, $PasswordParam)
	{
		$User = array();
		$this->db->where('UserID', $UserIDParam);
		$this->db->where('`AuditedActivity` !=', 'D');
		$query = $this->db->get('MsUser');
		if ($query->num_rows() > 0) {
			$User = $query->result_array();
			$UserID = $User[0]["UserID"];
			$Password = $User[0]["Password"];
			$PasswordParam = md5($PasswordParam);
			if ($PasswordParam == $Password) {
				$SessionID = uniqid();
				$this->session->set_userdata('sessionid', $SessionID);
				$Session = $this->db->query("CALL sp_save_session(?,?,?)", array($SessionID, $UserID, FALSE));
				$resultSession = $Session->result_array();
				$Session->next_result();
				$Session->free_result();
				if ($resultSession[0]["Result"] == 1) {
					return 1;
				}
			}
			else return -1;
		}
		else return -2;
	}

	function getInfoLogin($UserIDParam) 
	{
		$query = $this->db->query('CALL sp_get_login_info(?)',array($UserIDParam));	
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveUserRole($UserIDParam, $RoleIDParam, $AuditedUserParam)
	{
		$query = $this->db->query('CALL sp_save_userrole(?,?,?)',array($UserIDParam, $RoleIDParam, $AuditedUserParam));	
		return $query->result_array();	
	}

	function getYear($YearIDParam)
	{
		$query = $this->db->query('CALL sp_get_year(?)',array($YearIDParam));

		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function getTypeFile($TypeID)
	{
		$query = $this->db->get_where('TypeFile', array('TypeID' => $TypeID));
		return $query->result_array();
	}

	function getFile($FileIDParam) 
	{
		$query = $this->db->query('CALL sp_get_file(?)',array($FileIDParam));

		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;	
	}

	function saveFile($FileIDParam, $FileNameParam, $FileURL, $TypeParam, $AuditedUserParam)
	{
		$query = $this->db->query('CALL sp_save_file(?,?,?,?,?)',array($FileIDParam, $FileNameParam, $FileURL, $TypeParam, $AuditedUserParam));
		return $query->result_array();
	}

	function getLog($UserIDParam, $MenuIDParam) 
	{
		$query = $this->db->query('CALL sp_get_log(?,?)',array($UserIDParam, $MenuIDParam));
		return $query->result_array();
	}

	function saveLog($MenuIDParam, $AuditedUserParam, $AuditedActivityParam)
	{
		$query = $this->db->query('CALL sp_save_log(?,?,?)',array($MenuIDParam, $AuditedUserParam, $AuditedActivityParam));
		return $query->result_array();
	}
}