<?php
class Training_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getTraining($CourseIDParam, $YearIDParam) {
		$data = array();
		$exam = $this->db->query('CALL sp_get_exam(?,?,?)', array(null, $CourseIDParam, $YearIDParam));
		$resultExam = $exam->result_array();
		$exam->next_result();
		$exam->free_result();
		$course = $this->db->query('CALL sp_get_course(?)', array($CourseIDParam));
		$resultCourse = $course->result_array();
		$course->next_result();
		$course->free_result();
		foreach ($resultExam as $key => $value) {
			# code...
			$option = $this->db->query('CALL sp_get_option(?,?)', array(null, $value["ExamID"]));
			$resultOption = $option->result_array();
			$option->next_result();
			$option->free_result();
			$data[$key] = $value;
			if (count($resultCourse) > 0) {
				$data[$key]["Course"] = $resultCourse[0]["CourseName"];
			}
			if ($value["FileID"] != null) {
				$file = $this->db->query('CALL sp_get_file(?)', array($value["FileID"]));
				$resultFileQuestion = $file->result_array();
				$data[$key]["File"] = $resultFileQuestion;
				$file->next_result();
				$file->free_result();
			}
			$data[$key]["Option"] = $resultOption;
			foreach ($resultOption as $keyOption => $valueOption) {
				# code...
				if ($valueOption["FileID"] != null) {
					$fileOption = $this->db->query('CALL sp_get_file(?)', array($valueOption["FileID"]));
					$resultFileOption = $fileOption->result_array();
					$data[$key]["Option"][$keyOption]["File"] = $resultFileOption;
					$fileOption->next_result();
					$fileOption->free_result();
				}
			}
		}
		return $data;
	}
}