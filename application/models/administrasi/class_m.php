<?php
class Class_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getClass($ClassIDParam) {
		$query = $this->db->query('CALL sp_get_class(?)', array($ClassIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveClass($ClassIDParam, $ClassNameParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_class(?,?,?)', array($ClassIDParam, $ClassNameParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteClass($ClassIDParam) {
		$query = $this->db->query('CALL sp_delete_class(?)', array($ClassIDParam));
		return $query->result_array();
	}

	function getStudentMapping($ClassIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_get_student_mapping(?,?)', array($ClassIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveStudentMapping($UserIDParam, $ClassIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_save_mapping_class_student(?,?,?)', array($UserIDParam ,$ClassIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}
}