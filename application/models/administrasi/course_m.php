<?php
class Course_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getCourse($CourseIDParam) {
		$query = $this->db->query('CALL sp_get_course(?)', array($CourseIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveCourse($CourseIDParam, $CourseNameParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_course(?,?,?)', array($CourseIDParam, $CourseNameParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteCourse($CourseIDParam) {
		$query = $this->db->query('CALL sp_delete_course(?)', array($CourseIDParam));
		return $query->result_array();
	}

	function getClassMapping($CourseIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_get_class_mapping(?,?)', array($CourseIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveClassMapping($ClassIDParam, $CourseIDParam, $YearIDParam) {
		$query = $this->db->query('CALL sp_save_mapping_course_class(?,?,?)', array($ClassIDParam, $CourseIDParam, $YearIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}
}