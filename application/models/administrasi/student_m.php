<?php
class Student_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getStudent($UserIDParam) {
		$query = $this->db->query('CALL sp_get_student(?)', array($UserIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveStudent($UserIDParam, $YearIDParam, $FullNameParam, $BirthPlaceParam, $BirthDateParam, $GenderParam, $AddressParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_student(?,?,?,?,?,?,?,?)', array($UserIDParam, $YearIDParam, $FullNameParam, $BirthPlaceParam, $BirthDateParam, $GenderParam, $AddressParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteStudent($UserIDParam) {
		$query = $this->db->query('CALL sp_delete_student(?)', array($UserIDParam));
		return $query->result_array();
	}
}