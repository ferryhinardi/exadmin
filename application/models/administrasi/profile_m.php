<?php
class Profile_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getProfile() {
		$query = $this->db->query('CALL sp_get_company_profile', array());
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		return $result;
	}

	function saveProfile($ComapanyIDParam, $CompanyNameParam, $CompanyProfileParam) {
		$query = $this->db->query('CALL sp_insert_company_profile(?,?,?,?)', array($ComapanyIDParam, $CompanyNameParam, $CompanyProfileParam, 'Admin'));
		return $query->result_array();
	}
}