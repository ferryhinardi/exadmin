<?php
class Schedule_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getShift($ShiftIDParam) {
		$query = $this->db->query('CALL sp_get_shift(?)', array($ShiftIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveShift($ShiftIDParam, $ShiftNameParam, $StartTimeParam, $EndTimeParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_shift(?,?,?,?,?)', array($ShiftIDParam, $ShiftNameParam, $StartTimeParam, $EndTimeParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteShift($ShiftIDParam) {
		$query = $this->db->query('CALL sp_delete_shift(?)', array($ShiftIDParam));
		return $query->result_array();
	}

	function getSchedule($ScheduleIDParam) {
		$query = $this->db->query('CALL sp_get_schedule(?)', array($ScheduleIDParam));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function saveSchedule($ScheduleIDParam, $DateParam, $ShiftIDParam, $ClassIDParam, $CourseIDParam, $YearIDParam, $IsVerifiedParam, $AuditedUserParam) {
		$query = $this->db->query('CALL sp_save_schedule(?,?,?,?,?,?,?,?)', array($ScheduleIDParam, $DateParam, $ShiftIDParam, $ClassIDParam, $CourseIDParam, $YearIDParam, $IsVerifiedParam, $AuditedUserParam));
		return $query->result_array();
	}

	function deleteSchedule($ScheduleIDParam) {
		$query = $this->db->query('CALL sp_delete_schedule(?)', array($ScheduleIDParam));
		return $query->result_array();
	}
}