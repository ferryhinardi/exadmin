<?php
class Menu_m extends CI_Model {
	function __construct()
	{
		// Call the Model constructor
		parent::__construct();
	}

	function getMenu($menulevel, $menuheader)
	{
		$query = $this->db->query('CALL sp_showmenu(?,?)',array($menulevel, $menuheader));

		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function createMenu($MenuIDParam, $MenuNameParam, $MenuLevelParam, $MenuHeaderParam, $MenuWebPartParam, $MenuIndexParam, $MenuUrlParam)
	{
		$data = array($MenuIDParam, $MenuNameParam, $MenuLevelParam, $MenuHeaderParam, $MenuWebPartParam, $MenuIndexParam, $MenuUrlParam);
		$query = $this->db->query('CALL sp_save_menu(?, ?, ?, ?, ?, ?, ?)', $data);

		return $query->result_array();
	}

	function deleteMenu($MenuIDParam)
	{
		$query = $this->db->query('CALL sp_deletemenu(?)', array($MenuIDParam));
		return $query->result_array();
	}

	function getRole($RoleID)
	{
		$query = $this->db->query('CALL sp_showrole(?)',array($RoleID));
		return $query->result_array();
	}

	function createUpdateAccessRole($RoleID, $MenuID, $view, $create, $update, $delete)
	{
		$query = $this->db->query('CALL sp_save_accessmenurole(?,?,?,?,?,?)', array($RoleID, $MenuID, $view, $create, $update, $delete));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}

	function getAccessRole($RoleID, $MenuID)
	{
		$query = $this->db->query('CALL sp_get_accessmenurole(?,?)', array($RoleID, $MenuID));
		$result = $query->result_array();
		$query->next_result();
		$query->free_result();
		
		return $result;
	}
}