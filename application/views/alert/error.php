<div class="alert alert-danger tempNotification" style="float:none; position:fixed; right:10px; z-index: 100;">
	<button type="button" class="close" data-dismiss="alert">x</button>
	<strong>Error , </strong> <?=(isset($alertMessage))?$alertMessage:'';?>
	<input type="hidden" name="section" value="<?=(isset($section))?$section:'';?>" />
</div>