<?php $domain = $this->config->item('domain');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Company System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="<?=$domain?>/media/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/login.css" rel="stylesheet">

	<script src="<?=$domain?>/media/js/jquery.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/bootstrap/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>

	<script>
		var main_domain = "<?=$domain?>";
	</script>
	<script type="text/javascript">
	$(function() {
		/*$("#form-login").submit(function(e) {
			e.preventDefault();
			var data = {
				UserID: $("#login").val(),
				Password: $("#password").val()
			}

			$.post(main_domain + "/login/doLogin", data, function(data) {
				window.location.reload();
			});
		});*/
	});
	</script>
</head>
<body>
	<noscript> 
		<style>
			html, body { overflow: hidden; height: 100% }
			body { margin:0; padding:0; }
		</style>
		<div  style="position: fixed; font-size: 18px; z-index: 2; cursor: help; background: #F5E391; width: 100%; color: black; padding: 5px 5px 5px 5px; border: 1px solid;  top:60px; border-color: #000000; height: 100%; text-align: left;">
			<span style="font: bold 20px Arial; color:#000000; background: #F5E391; vertical-align: middle">Please Enable Javascript</span>
			<br /> 
			Sorry this site will not function properly without the use of javascripts.
			The scripts are safe and will not harm your computer in anyway. 
			Adjust your settings to allow scripts for this site and reload the site.
		</div> 
	</noscript>

	<div id="container">
		<form method="post" action="<?=$domain?>/login/doLogin" id="form-login">
			<ul class="inputs black-input large">
				<li>
					<i class="fa fa-user"></i>
					<input type="text" name="UserID" maxlength="15" class="form-control input-unstyled" id="login" placeholder="Login" autocomplete="off" />
				</li>
				<li>
					<i class="fa fa-lock"></i>
					<input type="password" name="Password" class="form-control input-unstyled" id="password" placeholder="Password" autocomplete="off" />
				</li>
			</ul>
			<input type="submit" class="btn btn-success" style="width:100%" value="Login" />
		</form>
		<?=(isset($erroralert))?$erroralert:''; ?>
	</div>
</body>