<?php $domain = $this->config->item('domain'); ?>

<script type="text/javascript">
	$(function() {
		var options = [{
			selector: "content-absence",
			name: "absence-table",
			items: [{
				header:[
					{ text: "", attr: "<input type='checkbox' data-attr='checkAll' id='checkAll' value=1 />", attrHeader: "style='text-align:center;'" }, 
					"ID Siswa", "Nama Siswa", "Keterangan", "", "", ""
				],
				row: [
					[{ type: "checkbox", name: "absence-1", cell:"style='text-align:center;'", attr:"class='checklist' data-attr='checklist'", items: [{ value:1 }] },
					"1", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
					[{ type: "checkbox", name: "absence-2", cell:"style='text-align:center;'", attr:"class='checklist' data-attr='checklist'", items: [{ value:1 }] }, "2", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
					[{ type: "checkbox", name: "absence-3", cell:"style='text-align:center;'", attr:"class='checklist' data-attr='checklist'", items: [{ value:1 }] }, "3", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
				]
			}]
		}];
		Apps.LoadForm([{
			selector: "content-absence",
			name: "absence-form",
			legend: "Absence",
			items: [{
				type: "label",
				text: "Kelas :",
				label: "01PKJ",
				span: [2, 2]
			}, {
				type: "label",
				text: "Matapelajaran :",
				label: "Biologi",
				span: [2, 2],
				space: [3]
			}, {
				type: "label",
				text: "Shift :",
				label: "1",
				span: [2, 2]
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnSave", text: "Save" }
			]
		}]);
		Apps.LoadTable(options);

		function CheckedAllCb(element) {
			var totalCb = $(".checklist").length,
				totalCbChecked = $(".checked").length;

			if (totalCbChecked == totalCb) element.prop("checked", true);
			else element.prop("checked", false);
		}

		$("#checkAll").change(function(e) {
			e.preventDefault();
			if ($(this).is(':checked')) {
				$(".checklist").prop("checked", true);
				$(".checklist").addClass('checked');
				$(".checklist").popover('hide');
			}
			else {
				$(".checklist").prop("checked", false);
				$(".checklist").removeClass('checked');	
				$(".checklist").popover('show');
			}
		}).prop("checked", true).trigger('change');

		$(".checklist").popover({
			title: "Keterangan",
			html: true,
			content:"<div>" +
					"<input type='radio' name='absence' value='S' /> Sakit " +
					"<input type='radio' name='absence' value='I' /> Izin " + 
					"<select name='permission' disabled><option>-- Select Option --</option><option>Option 1</option></select>" +
					"<input type='radio' name='absence' value='A' /> Alfa " + 
					"</div>" 
		}).on('shown.bs.popover', function () {
			var _this = this;
			$("input[name=absence]").off('change').change(function() {
				if ($(this).val() == 'I') {
					$(this).siblings('select').removeAttr('disabled');
				}
				else {
					$(_this).popover('hide');
				}
			});

			$("select[name=permission]").off('change').change(function() {
				$(_this).popover('hide');
			});
		});

		$(".checklist").change(function(e) {
			if ($(this).is(':checked')) {
				$(this).addClass("checked");
				$(this).popover('hide');
			}
			else {
				$(this).removeClass("checked");
				$(this).popover('show');
			}

			CheckedAllCb($("#checkAll"));
		});
	});
</script>

<div class="row" id="absence">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Absence</h1></div>
			<div class="panel-body" id="content-absence"></div>
		</div>
	</div>
</div>