<?php $domain = $this->config->item('domain'); ?>
<?php $menu = (isset($menuAccess) ? $menuAccess : array()); ?>

<script type="text/javascript">
	var menus = <?php echo json_encode($menu); ?>;
</script>
<script type="text/javascript">
	$(function() {
		var rows = [], role = [], menuAccess = [], optionselect = "<option value=''>None</option>";

		/* Menu Management */
		$.ajax({
			url: main_domain+'/index.php/menu/getAllMenu',
			type: 'POST',
			dataType: 'JSON',
			success: function(data, textStatus, jqXHR) {
				$.map(data.data || [], function( val, i ) {
					/* Table */
					rows[i] = [];
					rows[i].push(val.MenuID);
					rows[i].push(val.MenuName);
					rows[i].push(val.MenuLevel);
					rows[i].push(val.MenuHeader);
					rows[i].push(val.MenuHeaderName);
					rows[i].push(val.MenuWebPart);
					rows[i].push(val.MenuIndex);
					rows[i].push(val.MenuUrl);

					/* Menu Header*/ 
					optionselect += "<option value="+val.MenuID+">"+val.MenuID+". "+val.MenuName+"</option>";
				});
			},
			async: false
		}).done(function( data, textStatus, jqXHR ) {
		}).fail(function( jqXHR, textStatus, errorThrown ) {});

		var options = [{
					selector: "content-management",
					name: "menu-form",
					legend: "Menu",
					items: [{
						header:["Menu ID", "Menu Name", "Menu Level", { text: "Menu Header ID", hidden:true }, "Menu Header", "Menu WebPart", "Menu Index", "Menu Url"],
						row: rows
					}],
					toolbar: [
						{ type: "button", cls: "btn-danger", icon: "remove", name: "btnDelete", text: "Delete" },
						{ type: "button", cls: "btn-primary", icon: "plus", name: "btnAdd", text: "Add" }
					]
				}];

		Apps.LoadTable(options);
		Apps.Datatable({ selector: '#menu-form', sort: true, 
			onCallback: function() {
				$('#content-management tbody tr').dblclick(function(e) {
					var id = $(this).find('td:first').text(),
						name = $(this).find('td:nth-child(2)').text(),
						level = $(this).find('td:nth-child(3)').text(),
						header = $(this).find('td:nth-child(4)').text(),
						webPart = $(this).find('td:nth-child(6)').text(),
						index = $(this).find('td:nth-child(7)').text(),
						url = $(this).find('td:nth-child(8)').text();
					setValuePopup(id, name, level, header, webPart, index, url);
					$("#menu-popup").modal("show");
				});
			} 
		});

		Apps.Popup({
			name: "menu-popup",
			title: "Add Menu",
			body: "<form>" +
				"<input type='hidden' id='menuIDPopup' name='menuIDPopup' />" +
				"<div class='form-group'>" +
				"<label for='menuNamePopup' class='control-label'>Menu Name</label>" +
				"<input type='text' class='form-control' id='menuNamePopup' />" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='menuLevelPopup' class='control-label'>Menu Level</label>" +
				"<input type='number' class='form-control' id='menuLevelPopup' />" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='menuHeaderPopup' class='control-label'>Menu Header</label>" +
				"<select class='form-control' id='menuHeaderPopup'>" +
				optionselect +
				"</select>" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='menuWebPartPopup' class='control-label'>Menu WebPart</label>" +
				"<input type='text' class='form-control' id='menuWebPartPopup' />" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='menuIndexPopup' class='control-label'>Menu Index</label>" +
				"<input type='number' class='form-control' id='menuIndexPopup' />" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='menuUrlPopup' class='control-label'>Menu Url</label>" +
				"<input type='text' class='form-control' id='menuUrlPopup' />" +
				"</div>" +
				"</form>",
			footer: "<input type='submit' class='btn btn-primary' id='btnSubmit' name='btnSubmit' />"
		});

		Apps.Popup({
			name: "confirm-popup",
			title: "Confirmation",
			body: "<input type='hidden' id='menuIDDeletePopup' name='menuIDDeletePopup' />Are You Sure Want to Delete Menu <b><span id='menuNameConfirmation'></span></b>?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm'>Yes</button>"
		});

		$('#btnAdd').click(function(e) {
			e.preventDefault();
			setValuePopup("", "", "", "", "", "", "");
			$("#menu-popup").modal("show");
		});

		$('#btnDelete').click(function(e) {
			e.preventDefault();
			if ($('#content-management tbody tr').hasClass('selected')){
				$('#menuIDDeletePopup').val($('#content-management tbody tr.selected').find('td:first').text());
				$('#menuNameConfirmation').text($('#content-management tbody tr.selected').find('td:nth-child(2)').text());
				$("#confirm-popup").modal("show");
			}
		});

		$('#btnSubmit').click(function(e) {
			e.preventDefault();
			var data = { 
				MenuID: $('#menuIDPopup').val(), 
				MenuName: $('#menuNamePopup').val(), 
				MenuLevel: $('#menuLevelPopup').val(), 
				MenuHeader: $('#menuHeaderPopup').val(), 
				MenuWebPart: $('#menuWebPartPopup').val(), 
				MenuIndex: $('#menuIndexPopup').val(), 
				MenuUrl: $('#menuUrlPopup').val() 
			};

			$.post(main_domain+'/index.php/menu/addEditMenu', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		$('#menuNameConfirmation').click(function(e) {
			e.preventDefault();
			$.post(main_domain+'/index.php/menu/deleteMenu', { MenuID: $('#menuIDDeletePopup').val() }, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		function setValuePopup(id, name, level, header, webPart, index, url) {
			$('#menuIDPopup').val(id);
			$('#menuNamePopup').val(name);
			$('#menuLevelPopup').val(level);
			$('#menuHeaderPopup').val(header);
			$('#menuWebPartPopup').val(webPart);
			$('#menuIndexPopup').val(index);
			$('#menuUrlPopup').val(url);
		}

		/* Menu Access */
		$.ajax({
			url: main_domain+'/index.php/menu/getAllRole',
			type: 'POST',
			dataType: 'JSON',
			success: function(data, textStatus, jqXHR) {
				$.map(data.data || [], function( val, i ) {
					role.push({ value: val.RoleID, text: val.RoleName });
				});
			},
			async: false
		}).done(function( data, textStatus, jqXHR ) {
		}).fail(function( jqXHR, textStatus, errorThrown ) {});

		function createMenu(val, index) {
			/* Menu Access */
			menuAccess[index] = [];
			menuAccess[index].push(val.MenuID);
			if (val.MenuLevel == 3)
				menuAccess[index].push({ text: val.MenuName, custom: "style=padding-left:4em" });
			else if (val.MenuLevel == 2)
				menuAccess[index].push({ text: val.MenuName, custom: "style=padding-left:2em" });
			else 
				menuAccess[index].push(val.MenuName);

			menuAccess[index].push({ type: "checkbox", name: "view-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbView' data-attr='View'", items: [{ value:1 }] });
			if (val.MenuLevel < 3) {
				menuAccess[index].push({ type: "checkbox", name: "create-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbCreate' data-attr='Create'" });
				menuAccess[index].push({ type: "checkbox", name: "update-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbUpdate' data-attr='Update'" });
				menuAccess[index].push({ type: "checkbox", name: "delete-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbDelete' data-attr='Delete'" });
			}
			else {
				menuAccess[index].push({ type: "checkbox", name: "create-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbCreate' data-attr='Create'", items: [{ value:1 }] });
				menuAccess[index].push({ type: "checkbox", name: "update-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbUpdate' data-attr='Update'", items: [{ value:1 }] });
				menuAccess[index].push({ type: "checkbox", name: "delete-"+val.MenuID, cell:"style='text-align:center;'", attr:"class='cbDelete' data-attr='Delete'", items: [{ value:1 }] });
			}
		}

		var index = 0;
		$.map(menus, function(val, i) {
			createMenu(val, index);
			index++;
			$.map(val.item || [], function(val2, j) {
				createMenu(val2, index);
				index++;
				$.map(val2.item || [], function(val3, k) {
					createMenu(val3, index);
					index++;
				});
			});
		})

		var options_access = [{
					selector: "content-access",
					name: "access-form",
					legend: "Access",
					items: [{
						type: "select",
						name: "role-access",
						text: "Role",
						items: role
					}],
					table: [{
						name: "access-menu",
						items:[{
							header: [{ text: "Menu ID", hidden:true }, 
										"Menu Name",
										{ text: "View", attr: "<input type='checkbox' data-attr='View' id='cbViewAll' value=1 />" }, 
										{ text: "Create", attr: "<input type='checkbox' data-attr='Create' id='cbCreateAll' />" }, 
										{ text: "Update", attr: "<input type='checkbox' data-attr='Update' id='cbUpdateAll' />" }, 
										{ text: "Delete", attr: "<input type='checkbox' data-attr='Delete' id='cbDeleteAll' />" }
									],
							row: menuAccess
						}],
					}],
					toolbar: [
						{ type: "button", cls: "btn-primary", icon: "plus", name: "btnSave", text: "Save" }
					]
				}];

		Apps.LoadForm(options_access);
		Apps.Datatable({ selector: '#access-menu', paging: false });

		$("#cbViewAll, #cbCreateAll, #cbUpdateAll, #cbDeleteAll").change(function(e) {
			e.preventDefault();
			if ($(this).is(':checked')) {
				$(".cb"+$(this).attr('data-attr')).prop("checked", true);
				$(".cb"+$(this).attr('data-attr')).addClass('checked'+$(this).attr('data-attr'));
			}
			else {
				$(".cb"+$(this).attr('data-attr')).prop("checked", false);
				$(".cb"+$(this).attr('data-attr')).removeClass('checked'+$(this).attr('data-attr'));	
			}
		});

		function CheckedAllCb(element) {
			var totalCb = $(".cb"+element.attr('data-attr')).length,
				totalCbChecked = $(".checked"+element.attr('data-attr')).length;

			if (totalCbChecked == totalCb) element.prop("checked", true);
			else element.prop("checked", false);
		}

		$(".cbView, .cbCreate, .cbUpdate, .cbDelete").change(function(e) {
			var checkboxAll = $("#cb"+$(this).attr('data-attr')+"All");
			if ($(this).is(':checked')) {
				$(this).addClass("checked"+checkboxAll.attr('data-attr'));
			}
			else {
				$(this).removeClass("checked"+checkboxAll.attr('data-attr'));
			}

			CheckedAllCb(checkboxAll);
		});

		$('#role-access').change(function(e) {
			$.ajax({
				url: main_domain+'/index.php/menu/getAccessMenu',
				type: 'POST',
				data: { RoleID: $(this).val() },
				dataType: 'JSON',
				success: function( data, textStatus, jqXHR ) {
					var row = $('table#access-menu tbody tr');

					/* Reset Checkbox */
					$(".cbView, .cbCreate, .cbUpdate, .cbDelete").prop("checked", false);
					$(".cbView").removeClass("checkedView")
					$(".cbCreate").removeClass("checkedCreate")
					$(".cbUpdate").removeClass("checkedUpdate")
					$(".cbDelete").removeClass("checkedDelete")
					/* ============== */

					$.map(row, function( val, i ) {
						var MenuID = $(val).find('td:first').text();
						$.map(data, function( val2, j ) {
							if (val2.MenuID == MenuID) {
								if (val2.View == "1") 
									$(val).find('td:nth-child(3) input[type=checkbox]').prop('checked', true).addClass("checkedView");
								else
									$(val).find('td:nth-child(3) input[type=checkbox]').prop('checked', false).removeClass("checkedView");

								if (val2.Create == "1")
									$(val).find('td:nth-child(4) input[type=checkbox]').prop('checked', true).addClass("checkedCreate");
								else
									$(val).find('td:nth-child(4) input[type=checkbox]').prop('checked', false).removeClass("checkedCreate");

								if (val2.Update == "1") 
									$(val).find('td:nth-child(5) input[type=checkbox]').prop('checked', true).addClass("checkedUpdate");
								else
									$(val).find('td:nth-child(5) input[type=checkbox]').prop('checked', false).removeClass("checkedUpdate");

								if (val2.Delete == "1") 
									$(val).find('td:nth-child(6) input[type=checkbox]').prop('checked', true).addClass("checkedDelete");
								else
									$(val).find('td:nth-child(6) input[type=checkbox]').prop('checked', false).removeClass("checkedDelete");
							}
						});
					});

					CheckedAllCb($('#cbViewAll'));
					CheckedAllCb($('#cbCreateAll'));
					CheckedAllCb($('#cbUpdateAll'));
					CheckedAllCb($('#cbDeleteAll'));
				}
			}).done(function( data, textStatus, jqXHR ) {
			}).fail(function( jqXHR, textStatus, errorThrown ) {});
		}).trigger('change');

		$("#btnSave").click(function(e) {
			e.preventDefault();
			var row = $('table#access-menu tbody tr'), data = [];
			$.map(row, function( val, i ) {
				var viewCb = ($(val).find('td:nth-child(3) input[type=checkbox]').is(':checked')) ? 
								$(val).find('td:nth-child(3) input[type=checkbox]').val() : 0,
					createCb = ($(val).find('td:nth-child(4) input[type=checkbox]').is(':checked')) ? 
								$(val).find('td:nth-child(4) input[type=checkbox]').val() : 0,
					updateCb = ($(val).find('td:nth-child(5) input[type=checkbox]').is(':checked')) ? 
								$(val).find('td:nth-child(5) input[type=checkbox]').val() : 0,
					deleteCb = ($(val).find('td:nth-child(6) input[type=checkbox]').is(':checked')) ? 
								$(val).find('td:nth-child(6) input[type=checkbox]').val() : 0;
				data.push({ 
					RoleID: $("#role-access").val(), 
					MenuID: $(val).find('td:first').text(),
					View: viewCb,
					Insert: createCb,
					Update: updateCb,
					Delete: deleteCb
				});
			});

			$.ajax({
				url: main_domain+'/index.php/menu/addAccessMenu',
				type: 'POST',
				data: { data: data },
				dataType: 'JSON',
				success: function(data, textStatus, jqXHR) {
				}
			}).done(function( data, textStatus, jqXHR ) {
			}).fail(function( jqXHR, textStatus, errorThrown ) {});
			
			window.location.reload();
		});
	});
</script>

<div class="row" id="menu-management">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Menu Management</h1></div>
			<div class="panel-body" id="content-management"></div>
		</div>
	</div>
</div>

<div class="row" id="menu-access">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Menu Access</h1></div>
			<div class="panel-body" id="content-access"></div>
		</div>
	</div>
</div>