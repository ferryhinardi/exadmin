<?php $domain = $this->config->item('domain'); ?>
<script type="text/javascript">
	$(function() {
		var options = [{
					selector: "content",
					name: "form",
					legend: "Regular",
					items: [{
						type: "text",
						name: "text",
						text: "Text"
					}, {
						type: "autocomplete",
						name: "autocomplete",
						text: "Autocomplete",
						// data: [{ url: main_domain+'/index.php/menu/getAllMenu' }],
						data: [
							'Alabama',
							'Alaska',
							'Arizona',
							'Arkansas',
							'California',
							'Colorado',
							'Connecticut',
							'Delaware',
							'Florida',
							'Georgia',
							'Hawaii',
							'Idaho',
							'Illinois',
							'Indiana',
							'Iowa',
							'Kansas',
							'Kentucky',
							'Louisiana',
							'Maine',
							'Maryland',
							'Massachusetts',
							'Michigan',
							'Minnesota',
							'Mississippi',
							'Missouri',
							'Montana',
							'Nebraska',
							'Nevada',
							'New Hampshire',
							'New Jersey',
							'New Mexico',
							'New York',
							'North Carolina',
							'North Dakota',
							'Ohio',
							'Oklahoma',
							'Oregon',
							'Pennsylvania',
							'Rhode Island',
							'South Carolina',
							'South Dakota',
							'Tennessee',
							'Texas',
							'Utah',
							'Vermont',
							'Virginia',
							'Washington',
							'West Virginia',
							'Wisconsin',
							'Wyoming'
						],
						event: function() {
							// alert('masuk');
						}
					}, {
						type: "email",
						name: "email",
						text: "Email"
					}, {
						type: "password",
						name: "password",
						text: "Password"
					}, {
						type: "datepicker",
						name: "datepicker",
						icon: false,
						text: "Date Picker"
					}, {
						type: "timepicker",
						name: "timepicker",
						text: "Time Picker"
					}, {
						type: "file",
						name: "file",
						text: "File"
					}, {
						type: "button",
						items: [{
							typeButton: "submit",
							text: "Submit"
						},{
							typeButton: "reset",
							text: "Reset"
						}, {
							typeButton: "button",
							text: "Button",
							name: "button",
							mode: "success"
						}]
					}]
				}, {
					selector: "content",
					name: "data",
					legend: "Advance",
					items: [{
						type: "select",
						name: "select",
						text: "Select",
						items: [{
							text: "Option 1",
							value: "1"
						}, {
							text: "Option 2",
							value: "2"
						}]
					}, {
						type: "radio",
						name: "radio",
						items: [{
							text: "Radio Group 1",
							value: "1"
						}, {
							text: "Radio Group 2",
							value: "2"
						}]
					}, {
						type: "checkbox",
						name: "checkbox",
						items: [{
							text: "Checkbox 1",
							value: "1"
						}, {
							text: "Checkbox 2",
							value: "2"
						}]
					}, {
						type: "textarea",
						name: "textarea",
						text: "TextArea",
						row: 5
					}, {
						type: "upload",
						name: "upload",
						text: "Upload"
					}]
				}];
		var widget = Apps.LoadForm(options);

		var table = [{
					selector: "table",
					name: "table",
					legend: "Table",
					items: [{
						header:["#", "Table heading", "Table heading", "Table heading", "Table heading", "Table heading", "Table heading"],
						row: [
							["1", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
							["2", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
							["3", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell", "Table cell"],
						]
					}]
				}, {
					selector: "table",
					name: "customTable",
					legend: "Custom Table",
					items: [{
						header:["#", "Table heading", "Table heading", "Table heading", "Table heading", "Table heading", "Table heading"],
						row: [
							["1",
							{ type: "text", name: "text1", text: "Text1" },
							{ type: "text", name: "text2", text: "Text2" },
							{ type: "text", name: "text3", text: "Text3" },
							{ type: "text", name: "text4", text: "Text4" },
							{ type: "text", name: "text5", text: "Text5" },
							{ type: "text", name: "text6", text: "Text6" }],
							["2",
							{ type: "email", name: "email1", text: "Email1" },
							{ type: "email", name: "email2", text: "Email2" },
							{ type: "email", name: "email3", text: "Email3" },
							{ type: "email", name: "email4", text: "Email4" },
							{ type: "email", name: "email5", text: "Email5" },
							{ type: "email", name: "email6", text: "Email6" }],
							["3",
							{ type: "datepicker", name: "datepicker1", icon: false, text: "Date Picker 1" },
							{ type: "datepicker", name: "datepicker2", icon: false, text: "Date Picker 2" },
							{ type: "datepicker", name: "datepicker3", icon: false, text: "Date Picker 3" },
							{ type: "datepicker", name: "datepicker4", icon: false, text: "Date Picker 4" },
							{ type: "datepicker", name: "datepicker5", icon: false, text: "Date Picker 5" },
							{ type: "datepicker", name: "datepicker6", icon: false, text: "Date Picker 6" }]
						]
					}]
				}];

		var widgetTable = Apps.LoadTable(table);

		Apps.Popup({
			name: "modal",
			title: "Modal title",
			body: "<form>" +
				"<div class='form-group'>" +
				"<label for='recipient-name' class='control-label'>Recipient:</label>" +
				"<input type='text' class='form-control' id='recipient-name'>" +
				"</div>" +
				"<div class='form-group'>" +
				"<label for='message-text' class='control-label'>Message:</label>" +
				"<textarea class='form-control' id='message-text'></textarea>" +
				"</div>" +
				"</form>",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-primary'>Send message</button>"
		});

		Apps.Popup({
			name: "modalGenerate",
			title: "Modal title",
			body: [{
				type: "text",
				name: "recipient-name",
				text: "Recipient"
			}, {
				type: "textarea",
				name: "message-text",
				text: "Message"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-primary'>Send message</button>"
		});

		$("#button").click(function(e) {
			e.preventDefault();
			$('#modalGenerate').modal('show');
		});

		Apps.DataTableServer({
			selector: "#content-table",
			url: main_domain + "/index.php/home/getAll",
			field: [
				{ "title": "Table Name", "data": "table_name" },
				{ "title": "Type", "data": "table_type" },
				{ "title": "Engine", "data": "engine" },
			]
		});

		Apps.Container({
			selector: "content",
			items: [{
				content: "1501171970",
				data_attr: "1501171970",
				selected: true
			}, {
				content: "1501171971",
				data_attr: "1501171971"
			}, {
				content: "1501171972",
				data_attr: "1501171972"
			}, {
				content: "1501171973",
				data_attr: "1501171973"
			}, {
				content: "1501171974",
				data_attr: "1501171974"
			}]
		});
	})
</script>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Template Form</h1></div>
			<div class="panel-body" id="content"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Template Data Table</h1></div>
			<div class="panel-body" id="table"></div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Table Server</h1></div>
			<div class="panel-body" id="table-server"><table id="content-table" class="table table-striped table-bordered table-hover"></table></div>
		</div>
	</div>
</div>