<?php $domain = $this->config->item('domain'); ?>
<?php $course = (isset($course) ? $course : array()); ?>
<?php $year = (isset($year) ? $year : array()); ?>

<script type="text/javascript">
	var courses = <?php echo json_encode($course); ?>;
	var years = <?php echo json_encode($year); ?>;
</script>

<script type="text/javascript">
	$(function() {
		var last_option = "C";

		function setFormMaterial(value) {
			value = value || {};
			var MateriID = (typeof value.MateriID == "undefined") ? "" : value.MateriID,
				Subject = (typeof value.Subject == "undefined") ? "" : value.Subject,
				Description = (typeof value.Description == "undefined") ? "" : value.Description;
			$("select#course-material").val($("#course-filter-material").val());
			$("select#year-material").val($("#year-filter-material").val());

			$("#materiID").val(MateriID);
			$("#subject-material").val(Subject);
			$("#description-material").val(Description);
		}

		function setFormExam(value) {
			value = value || {};
			var ExamID = (typeof value.ExamID == "undefined") ? "" : value.ExamID,
				No = (typeof value.No == "undefined") ? "" : value.No,
				Question = (typeof value.Question == "undefined") ? "" : value.Question,
				Answer = (typeof value.Answer == "undefined") ? "" : value.Answer;
			$("select#course-exam").val($("#course-filter-exam").val());
			$("select#year-exam").val($("#year-filter-exam").val());

			$("#examID").val(ExamID);
			$("#no").val(No);
			$("#questionText").val(Question);
			$("#answer").val(Answer);
		}

		function setOptionExam(value, setVal) {
			value = value || [];
			last_option = "C";
			var rows = [], row = [];
			var option_init = "A";
			if (value.length == 0) {
				$("#answer").empty();
				for (var i = 0; i < 3; i++) {
					row = [];
					row.push({ type: "hidden", name: "optId" + option_init });
					row.push({ type: "text", name: "opt" + option_init, text: option_init, attr: "maxlength='2'", cls: "txtOpt" });
					row.push({ type: "textarea", name: "desc" + option_init });
					row.push({ type: "file", name: "image" + option_init });
					row.push("");
					rows.push(row);
					$("#answer").append("<option value='"+option_init+"'>"+option_init+"</option>");
					option_init = String.fromCharCode(option_init.charCodeAt(0) + 1);
				}
			}
			else {
				$("#answer").empty();
				$.map(value, function(item, index) {
					row = [];
					row.push({ type: "hidden", name: "optId" + item.Option, value: item.OptionID });
					row.push({ type: "text", name: "opt" + item.Option, text: item.Option, attr: "maxlength='2'", value: item.Option, cls: "txtOpt" });
					row.push({ type: "textarea", name: "desc" + item.Option, text: item.Description });
					row.push({ type: "file", name: "image" + item.Option });
					row.push("");
					rows.push(row);
					last_option = item.Option;
					$("#answer").append("<option value='"+item.Option+"'>"+item.Option+"</option>");
				});
				$("#answer").val(setVal);
			}
			$("#table-option").empty();
			Apps.LoadTable([{
				selector: "table-option",
				items: [{
					header:[{ text: "Option ID", hidden:true }, { text: "Option", attrHeader: "style='width:10%'" }, { text: "Description", attrHeader: "style='width:50%'" }, { text: "Image", attrHeader: "style='width:30%'" }, { text: "Action", attrHeader: "style='width:10%'" }],
					row: rows
				}],
				onCallback: function() {
					$(document).on('keyup', '.txtOpt', function(e) {
						e.preventDefault();
						this.value = this.value.toUpperCase();
					})
				}
			}]);
		}

		function setFormScore(value) {
			value = value || {};
			$("select#course-score").val($("#course-filter-score").val());
			$("select#year-score").val($("#year-filter-score").val());
			$("#typeScoreID").val(value.TypeScoreID);
			$("#type-score").val(value.Type);
			$("#weight-score").val(value.Weight);
		}

		function prepareUpload(event){
			files = event.target.files;
			if (files && files[0]) {
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);
			}
		}

		function MaterialDataTable(CourseID, YearID) {
			Apps.MaterialDataTable = Apps.DataTableServer({
				selector: "#content-table-material",
				onDeclare: true,
				url: main_domain + "/index.php/persiapan/materi/getMateri",
				param: [{ "name": "CourseID", "value": CourseID }, { "name": "YearID", "value": YearID }],
				field: [
					{ "title": "Materi ID", "data": "MateriID", "visible": false },
					{ "title": "Subject", "data": "Subject" },
					{ "title": "Deskripsi", "data": "Description" },
					{ "title": "File", "data": "FileID",
						"render": function ( data, type, full, meta ) { 
							return "<a href='"+main_domain + full.FileURL+"' data-attr='"+data+"'>"+full.FileName+"</a>";
						}
					},
					{ 
						"title": "Action",
						"className": 'iActionInputMateri inline',
						"sortable": false,
						"data": "MateriID",
						"render": function ( data, type, full, meta ) { 
							return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
						}
					}
				],
				onCallback: function() {
					$('.iActionInputMateri .iEdit').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$.post(main_domain + "/index.php/persiapan/materi/getMateri", { MateriID: id }, function(data) {
							if (data.status == "OK") {
								data = data.data;
								if (data.length)
									setFormMaterial(data[0]);
							}
						}, "json");
						$("#modalNewMateri").modal("show");
					});

					$('.iActionInputMateri .iDelete').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$("#materiIDDeletePopup").val(id);
						$("#confirm-popup-materi").modal("show");
					});
				}
			});	
		}

		function ExamDataTable (CourseID, YearID) {
			Apps.ExamDataTable = Apps.DataTableServer({
				selector: "#content-table-exam",
				onDeclare: true,
				url: main_domain + "/index.php/persiapan/materi/getExercise",
				param: [{ "name": "CourseID", "value": CourseID }, { "name": "YearID", "value": YearID }],
				field: [
					{ "title": "Exam ID", "data": "ExamID", "visible": false },
					{ "title": "No", "data": "No" },
					{ "title": "Question", "data": "Question" },
					{ "title": "Option", "data": "Option",
						"render": function ( data, type, full, meta ) { 
							var ans = full.Answer;
							if (type == "display") {
								var html = "";
								html += "<ul>";
								$.map(data, function(item, index) {
									html += "<li data-attr="+item.OptionID+">";
									if (item.Option == ans)
										html += "<b>" + item.Option + ". " + ((item.FileID == null) ? item.Description : "[File]") + "</b>";
									else
										html += item.Option + ". " + ((item.FileID == null) ? item.Description : "[File]");
									html += "</li>";
								});
								html += "</ul>";
								return html;
							}
							else
								return data;
						}
					},
					{ 
						"title": "Action",
						"className": 'iActionInputExam inline',
						"sortable": false,
						"data": "ExamID",
						"render": function ( data, type, full, meta ) { 
							return (
								// "<a class='btn btn-default iView'  data-attr="+data+"><i class='glyphicon glyphicon-eye-open'></i></a>"+
								" <a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a>"+
								" <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>")
						}
					}
				],
				onCallback: function() {
					$('.iActionInputExam .iEdit').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$.post(main_domain + "/index.php/persiapan/materi/getExam", { ExamID: id }, function(dataExam) {
							if (dataExam.status == "OK") {
								dataExam = dataExam.data;
								if (dataExam.length)
									setFormExam(dataExam[0]);
								$.post(main_domain + "/index.php/persiapan/materi/getOption", { ExamID: id }, function(dataOption) {
									if (dataOption.status == "OK") {
										dataOption = dataOption.data;
										if (dataOption.length)
											setOptionExam(dataOption, dataExam[0].Answer);
									}
								}, "json");
							}
						}, "json");
						$("#modalNewExam").modal("show");
					});

					$('.iActionInputExam .iDelete').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$("#examIDDeletePopup").val(id);
						$("#confirm-popup-exam").modal("show");
					});
				}
			});
		} 

		function ScoreDataTable (CourseID, YearID) {
			Apps.ScoreDataTable = Apps.DataTableServer({
				selector: "#content-table-score",
				onDeclare: true,
				url: main_domain + "/index.php/persiapan/materi/getScoreType",
				param: [{ "name": "CourseID", "value": CourseID }, { "name": "YearID", "value": YearID }],
				field: [
					{ "title": "Type Score ID", "data": "TypeScoreID", "visible": false },
					{ "title": "Jenis", "data": "Type" },
					{ "title": "Bobot", "data": "Weight" },
					{ 
						"title": "Action",
						"className": 'iActionInputScore inline',
						"sortable": false,
						"data": "TypeScoreID",
						"render": function ( data, type, full, meta ) { 
							return (
								" <a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a>"+
								" <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>")
						}
					}
				],
				onCallback: function() {
					$('.iActionInputScore .iEdit').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$.post(main_domain + "/index.php/persiapan/materi/getScoreType", { TypeScoreID: id }, function(data) {
							if (data.status == "OK") {
								data = data.data;
								setFormScore(data);
							}
						}, "json");
						$("#modalNewScoreType").modal("show");
					});

					$('.iActionInputScore .iDelete').click(function(e) {
						e.preventDefault();
						var id = $(this).attr('data-attr');
						$("#scoreIDDeletePopup").val(id);
						$("#confirm-popup-score").modal("show");
					});
				}
			});
		} 

		/********************* Materi Setting ****************************/

		Apps.LoadForm([{
			selector: "content-filter-material",
			name: "form-filter",
			legend: "MATERIAL SETTING",
			items: [{
				type: "select",
				name: "course-filter-material",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-filter-material",
				text: "Year",
				items: years
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewMateri", text: "New" },
			],
		}]);

		$("#course-filter-material").change(function(e) {
			e.preventDefault();
			Apps.MaterialDataTable.destroy();
			$("#content-table-material").empty();
			MaterialDataTable($(this).val(), $("#year-filter-material").val());
		});

		$("#year-filter-material").change(function(e) {
			e.preventDefault();
			Apps.MaterialDataTable.destroy();
			$("#content-table-material").empty();
			MaterialDataTable($("#course-filter-material").val(), $(this).val());
		});

		$("#btnNewMateri").click(function(e) {
			e.preventDefault();
			setFormMaterial();
			$("#modalNewMateri").modal("show");
		});
		
		MaterialDataTable(courses[0].value, years[0].value);

		Apps.Popup({
			name: "modalNewMateri",
			title: "Form Materi",
			body: [{
				type: "select",
				name: "course-material",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-material",
				text: "Year",
				items: years
			}, {
				type: "spread"
			}, {
				type: "hidden",
				name: "materiID"
			}, {
				type: "text",
				name: "subject-material",
				text: "Subject"
			}, {
				type: "textarea",
				name: "description-material",
				text: "Deskripsi"
			}, {
				type: "file",
				name: "file-material",
				text: "File"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveMateri' class='btn btn-primary'>Save</button>"
		});

		Apps.Popup({
			name: "confirm-popup-materi",
			title: "Confirmation",
			body: "<input type='hidden' id='materiIDDeletePopup' name='materiIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-materi'>Yes</button>"
		});

		$("#btnConfirm-materi").off().click(function(e) {
			var id = $("#materiIDDeletePopup").val();
			$.post(main_domain + "/index.php/persiapan/materi/deleteMateri", {id: id}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		$("input#file-material").change(function(e) {
			attachmentFile = e.target.files;

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();
				
				Apps.item.form = new FormData();
				Apps.item.form.append('upload', attachmentFile[0]);
			}
			prepareUpload(e);
		});

		$("#btnSaveMateri").click(function(e) {
			e.preventDefault();
			if (typeof Apps.item.form == 'undefined') {
				saveMateri(null);
			}
			else {
				Apps.item.form.append('save', JSON.stringify({ CourseID: $("#course-material").val(), YearID: $("#year-material").val(), Type: 0 }));
				$.ajax({
					url: main_domain + '/index.php/persiapan/materi/upload', 
					dataType : "json",
					data: Apps.item.form, 
					cache: false,
					secureuri : false,
					processData : false,
					contentType : false,
					type: "POST",
					success: function(data) {
						var result = data.result;
						saveMateri((result.Result > 0) ? result.File : null);
					}
				});
			}
		});

		function saveMateri(FileID) {
			var materi = {};
			materi.MateriID = (($("#materiID").val() == "") ? null : $("#materiID").val());
			materi.CourseID = $("#course-material").val();
			materi.YearID = $("#year-material").val();
			materi.Subject = $("#subject-material").val();
			materi.Description = $("#description-material").val();
			materi.FileID = FileID;

			$.post(main_domain + '/index.php/persiapan/materi/saveMateri', materi, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		}

		/********************* Exam Setting ****************************/

		Apps.LoadForm([{
			selector: "content-filter-exam",
			name: "form-filter",
			legend: "EXAM SETTING",
			items: [{
				type: "select",
				name: "course-filter-exam",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-filter-exam",
				text: "Year",
				items: years
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewExam", text: "New" },
			],
		}]);

		$("#course-filter-exam").change(function(e) {
			e.preventDefault();
			Apps.ExamDataTable.destroy();
			$("#content-table-exam").empty();
			ExamDataTable($(this).val(), $("#year-filter-exam").val());
		});

		$("#year-filter-exam").change(function(e) {
			e.preventDefault();
			Apps.ExamDataTable.destroy();
			$("#content-table-exam").empty();
			ExamDataTable($("#course-filter-exam").val(), $(this).val());
		});

		$("#btnNewExam").click(function(e) {
			e.preventDefault();
			setFormExam();
			setOptionExam(null, null);
			$("#modalNewExam").modal("show");
		});

		ExamDataTable(courses[0].value, years[0].value);

		Apps.Popup({
			name: "modalNewExam",
			width: "75%",
			title: "Form Exam",
			body: [{
				type: "select",
				name: "course-exam",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-exam",
				text: "Year",
				items: years
			}, {
				type: "spread"
			}, {
				type: "hidden",
				name: "examID"
			}, {
				type: "number",
				name: "no",
				text: "No"
			}, {
				type: "textarea",
				name: "questionText",
				text: "Question",
				additional: {
					type: "file",
					name: "questionFile",
				}
			}, {
				type: "table",
				table: [{
					name: "table-option",
					items: [{
						header:[{ text: "Option ID", hidden:true }, { text: "Option", attrHeader: "style='width:10%'" }, { text: "Description", attrHeader: "style='width:50%'" }, { text: "Image", attrHeader: "style='width:30%'" }, { text: "Action", attrHeader: "style='width:10%'" }],
						row: [
							[{ type: "hidden", name: "optIdA" }, { type: "text", name: "optA", text: "A", attr: "maxlength='2'" }, { type: "textarea", name: "descA" }, { type: "file", name: "imageA" }, ""], // {type: "button", typeButton: "button", name: "deleteOptA", mode: "danger", text: "<i class='glyphicon glyphicon-remove'></i>"}
							[{ type: "hidden", name: "optIdB" }, { type: "text", name: "optB", text: "B", attr: "maxlength='2'" }, { type: "textarea", name: "descB" }, { type: "file", name: "imageB" }, ""],
							[{ type: "hidden", name: "optIdC" }, { type: "text", name: "optC", text: "C", attr: "maxlength='2'" }, { type: "textarea", name: "descC" }, { type: "file", name: "imageC" }, ""],
						]
					}]
				}]
			}, {
				type: "select",
				name: "answer",
				text: "Answer",
				items: [
					{ text: "A", value: "A" }, 
					{ text: "B", value: "B" }, 
					{ text: "C", value: "C" }
				]
			}, {
				type: "button",
				items: [{
					typeButton: "button",
					text: "Add Option",
					name: "btnAddOption",
					mode: "primary"
				}]
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveExam' class='btn btn-primary'>Save</button>"
		});

		$(document).on('keyup', '.txtOpt', function(e) {
			e.preventDefault();
			this.value = this.value.toUpperCase();
		})

		Apps.Popup({
			name: "confirm-popup-exam",
			title: "Confirmation",
			body: "<input type='hidden' id='examIDDeletePopup' name='examIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-exam'>Yes</button>"
		});

		$("#btnConfirm-exam").off().click(function(e) {
			var id = $("#examIDDeletePopup").val();
			$.post(main_domain + "/index.php/persiapan/materi/deleteExam", {id: id}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});
		
		$("#btnAddOption").click(function(e) {
			last_option = "C";
			last_option = String.fromCharCode(last_option.charCodeAt(0) + 1);
			e.preventDefault();
			$("#table-option tbody").append(
				'<tr>' + 
				'<td style="display:none;"><input class="form-control" type="hidden" name="optId'+last_option+'" id="optId'+last_option+'"></td>' +
				'<td><input class="form-control" type="text" name="opt'+last_option+'" id="opt'+last_option+'" placeholder="'+last_option+'"></td>' + 
				'<td><textarea class="form-control" id="desc'+last_option+'" name="desc'+last_option+'" rows="3"></textarea></td>' +
				'<td><input class="form-control" type="file" name="image'+last_option+'" id="image'+last_option+'"></td>' +
				'<td><button name="deleteOpt'+last_option+'" id="deleteOpt'+last_option+'" class="btn btn-danger"><i class="glyphicon glyphicon-remove"></i></button> &nbsp;</td>' + 
				'</tr>');
			$("#answer").append('<option value="'+last_option+'">'+last_option+'</option>');
		});

		$(document).on('click', '#table-option tbody .btn-danger', function(e) {
			e.preventDefault();
			var id = $(this).attr('id');
			var optionName = id.replace("deleteOpt", "");
			$(this).closest('tr').remove();
			$("#answer option[value='"+optionName+"']").remove();
		});

		$("input#questionFile").change(function(e) {
			attachmentFile = e.target.files;
			var question = {};

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();
				
				question = new FormData();
				question.append('upload', attachmentFile[0]);
				Apps.item.question = question;
			}
			prepareUpload(e);
		});

		$(document).on('change', '#table-option tbody tr td input[type=file]', function(e) {
			alert();
			attachmentFile = e.target.files;

			var id = e.target.id;
			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();
				formData = new FormData();
				formData.append('upload', attachmentFile[0]);
				Apps.prop = $.grep(Apps.prop, function(item, index) {
					return item.type != id;
				});
				Apps.prop.push({ type: id, file: formData });
			}
			prepareUpload(e);
		});

		$("#btnSaveExam").click(function(e) {
			e.preventDefault();
			var paramOption = [], paramQuestion = null;
			Apps.prop = Apps.prop.sort(function(obj1, obj2) {
				if (obj1.type > obj2.type)
					return 1;
				else if (obj1.type < obj2.type)
					return -1;
				else
					return 0;
			});

			if (typeof Apps.item.question !== 'undefined') {
				var imageQuestion = Apps.item.question;
				imageQuestion.append('save', JSON.stringify({ CourseID: $("#course-exam").val(), YearID: $("#year-exam").val(), Type: 1 }));

				$.ajax({
					url: main_domain + '/index.php/persiapan/materi/upload', 
					dataType : "json",
					data: imageQuestion, 
					cache: false,
					secureuri : false,
					processData : false,
					contentType : false,
					type: "POST",
					success: function(data) {
						var result = data.result;
						paramQuestion = (result.Result > 0) ? result.File : null;
					},
					async: false
				});
			}
			for (var i in Apps.prop) {
				var imageOption = Apps.prop[i].file;
				imageOption.append('save', JSON.stringify({ CourseID: $("#course-exam").val(), YearID: $("#year-exam").val(), Type: 2 }));

				$.ajax({
					url: main_domain + '/index.php/persiapan/materi/upload', 
					dataType : "json",
					data: imageOption, 
					cache: false,
					secureuri : false,
					processData : false,
					contentType : false,
					type: "POST",
					success: function(data) {
						var result = data.result;
						paramOption.push((result.Result > 0) ? result.File : null);
					},
					async: false
				});
			}
			saveExam(paramQuestion, ((paramOption.length == 0) ? null : paramOption));
		});

		function saveExam(FileIDQuestion, FileIDOption) {
			var exam = {};
			exam.CourseID = $("#course-exam").val();
			exam.YearID = $("#year-exam").val();
			exam.ExamID = (($("#examID").val() == "") ? null : $("#examID").val());
			exam.FileID = FileIDQuestion;
			exam.No = $("#no").val();
			exam.Question = $("#questionText").val();
			exam.Answer = $("#answer").val();
			exam.option = [];
			$.map($("#table-option tbody tr"), function(item, index) {
				var optionID = $(item).find('td input[type=hidden]').val();
				var optionName = $(item).find('td input[type=text]').val();
				var description = $(item).find('td textarea').val();
				exam.option.push({ OptionID: optionID, Option: optionName, Description: description, FileID: ((FileIDOption != null && FileIDOption.length > 0) ? FileIDOption[index] : null) });
			});

			if (exam.option.length < 3)
				return;
			
			$.post(main_domain + '/index.php/persiapan/materi/saveExam', exam, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		}

		/********************* Score Setting ****************************/

		Apps.LoadForm([{
			selector: "content-filter-score",
			name: "form-filter",
			legend: "SCORE SETTING",
			items: [{
				type: "select",
				name: "course-filter-score",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-filter-score",
				text: "Year",
				items: years
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewScoreType", text: "New" },
			],
		}]);

		$("#course-filter-score").change(function(e) {
			e.preventDefault();
			Apps.ScoreDataTable.destroy();
			$("#content-table-score").empty();
			ScoreDataTable($(this).val(), $("#year-filter-score").val());
		});

		$("#year-filter-score").change(function(e) {
			e.preventDefault();
			Apps.ScoreDataTable.destroy();
			$("#content-table-score").empty();
			ScoreDataTable($("#course-filter-score").val(), $(this).val());
		});

		$("#btnNewScoreType").click(function(e) {
			e.preventDefault();
			setFormScore();
			$("#modalNewScoreType").modal("show");
		});
		
		ScoreDataTable(courses[0].value, years[0].value);

		Apps.Popup({
			name: "modalNewScoreType",
			title: "Form Score",
			body: [{
				type: "select",
				name: "course-score",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year-score",
				text: "Year",
				items: years
			}, {
				type: "spread"
			}, {
				type: "hidden",
				name: "typeScoreID"
			}, {
				type: "text",
				name: "type-score",
				text: "Jenis"
			}, {
				type: "number",
				name: "weight-score",
				text: "Bobot"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveScoreType' class='btn btn-primary'>Save</button>"
		});

		Apps.Popup({
			name: "confirm-popup-score",
			title: "Confirmation",
			body: "<input type='hidden' id='scoreIDDeletePopup' name='scoreIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-score'>Yes</button>"
		});

		$("#btnConfirm-score").off().click(function(e) {
			var id = $("#scoreIDDeletePopup").val();
			$.post(main_domain + "/index.php/persiapan/materi/deleteScoreType", {id: id}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		$("#btnSaveScoreType").click(function(e) {
			e.preventDefault();
			var data = {
				TypeScoreID: (($("#typeScoreID").val() == "") ? null : $("#typeScoreID").val()),
				CourseID: $("#course-score").val(),
				YearID: $("#year-score").val(),
				Type: $("#type-score").val(),
				Weight: $("#weight-score").val()
			}

			$.post(main_domain+'/index.php/persiapan/materi/saveScoreType', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});
	});
</script>

<div class="row" id="class-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Pengaturan Persiapan</h1></div>
			<div class="panel-body">
				<div id="material">
					<div id="content-filter-material"></div>
					<div id="content-table-material"></div>
				</div>
				<div id="exam">
					<div id="content-filter-exam"></div>
					<div id="content-table-exam"></div>
				</div>
				<div id="score">
					<div id="content-filter-score"></div>
					<div id="content-table-score"></div>
				</div>
			</div>
		</div>
	</div>
</div>