<?php $domain = $this->config->item('domain'); ?>
<script type="text/javascript">
	$(function() {
		$("#wrapper").css('padding-left', 0);
		var options_profile = [{
					selector: "content-profile",
					name: "profile-form",
					legend: "Data",
					items: [{
						type: "text",
						name: "userID",
						text: "User ID",
						attr: "disabled"
					}, {
						type: "text",
						name: "name",
						text: "Nama",
					}, {
						type: "upload",
						name: "photo",
						text: "Avatar",
						default: true,
						// photo: "userpicture/no-picture.png"
					}],
				}];
		var options_password = [{
					selector: "content-profile",
					name: "change-password-form",
					legend: "Change Password",
					items: [{
						type: "password",
						name: "password",
						text: "Password",
					}, {
						type: "password",
						name: "newPassword",
						text: "New Password",
					}, {
						type: "password",
						name: "confirmPassword",
						text: "Confirm Password",
					}],
				}];
		var options_submit = [{
					selector: "content-footer",
					name: "submit-button",
					items: [{
						type: "button",
						name: "btnSave",
						text: "Save"
					}]
				}];
		Apps.LoadForm(options_profile);
		Apps.LoadForm(options_password);
		Apps.LoadForm(options_submit);
	});
</script>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Profile</h1></div>
			<div class="panel-body" id="content-profile"></div>
			<div class="panel-footer" id="content-footer"></div>
		</div>
	</div>
</div>