<?php $domain = $this->config->item('domain'); ?>
<?php $cards_course = (isset($cards_course) ? $cards_course : array()); ?>
<?php $cards_year = (isset($cards_year) ? $cards_year : array()); ?>

<script type="text/javascript">
	var cards_course = <?php echo json_encode($cards_course); ?>;
	var cards_year = <?php echo json_encode($cards_year); ?>;
</script>

<script type="text/javascript">
	var views = ['course','year', 'main'],
		timer, totalNo,
		time = 2 * 60 * 60, elapse = 0;
	var flag = 0;

	function init() {
		Apps.Container({
			selector: "exam-course",
			items: cards_course
		});
		$("#exam-course").addClass("active");
		$("#btnAction").hide();
	}

	function changeView() {
		localStorage.view = localStorage.curr == 'main' ? 'main' : 'intro';
		$('#btnAction').css('display', (localStorage.curr == 'course' || localStorage.curr == 'main') ? 'none' : 'block')
						.attr('data-goto', views[views.indexOf(localStorage.curr) - 1]);
		$('#content-exam').attr('data-curr', localStorage.curr);
		$('#exam .panel .panel-body').attr('data-view', localStorage.view);
	}

	function changeCourse() {
		$(".active").slideUp(500, function() {
			$(this).removeClass("active").empty();

			$('#exam-course').addClass("active").html(function() {
				return Apps.Container({
					items: cards_course
				});
			}).slideDown(500);
			changeView();
		});
	}

	function changeYear() {
		$(".active").slideUp(500, function() {
			$(this).removeClass("active").empty();

			$('#exam-year').addClass("active").html(function() {
				return Apps.Container({
					items: cards_year
				});
			}).slideDown(500);
			changeView();
		});
	}

	function backHome() {
		localStorage.curr = "course";
		changeCourse();
	}

	function changeClosePage(countEmptyAns, correctAnswer, persentaseAnswer) {
		$(".active").slideUp(500, function() {
			$(this).html(function(index, oldhtml) {
				return "<div><h4>"+countEmptyAns+" belum dijawab</h4></div>" + "<div>Jawaban Benar : <h3>"+correctAnswer+"</h3></div>" + "<div>Persentase : <h3>"+persentaseAnswer+" %</h3></div> <div><button onclick='backHome()' class='btn btn-default'>Kembali</button></div>";
			}).slideDown(500);
		});
	}

	function changeNo(no) {
		var data = Apps.prop[no-1],
			case_question = $('.no-case').siblings(),
			case_option = $('[class*=no-case-]').siblings(),
			dataOption = [];
		$('.curr-no').removeClass('curr-no'); //remove box from current number
		$('.no-' + no).parent().addClass('curr-no'); //add box to selected number
		$('.no-case').text(no);
		
		case_question.html("");
		if (data.FileID != null) 
			case_question.append("<img src='" + main_domain + data.File[0].FileURL + "' width='100%' height='100%' />");
		case_question.append(data.Question);

		if (data.Option.length) {
			$.map(data.Option, function(item, index) {
				var obj = case_option[index];
				$(obj).html(function(index, oldhtml) {
					var newHtml = "";
					var classOpt = $(this).siblings().attr('class').split(' ')[1].split('-')[2];
					if (item.Option == classOpt) {
						if (item.FileID != null)
							newHtml += "<img src='" + main_domain + item.File[0].FileURL + "' width='100%' height='100%' />";
						newHtml += item.Description;
					}
					return newHtml;
				});
			});
		}
	}

	function refreshBoard(boardParam) {
		var board = {};
		boardParam = boardParam || {};
		board.title = boardParam.Course;
		board.no = boardParam.No;
		board.question = boardParam.Question;
		board.options = boardParam.Option;
		if (boardParam.FileID != null) {
			if (boardParam.File.length > 0)
				board.questionImage = main_domain + boardParam.File[0].FileURL;
		}
		$('#exam-main').addClass("active").html(function() {
			return Apps.Board({
				items: board
			});
		}).slideDown(500);
		
		$("#board-header").height($("#board-ratio").height() - $("#board-content").height());
	}

	function refreshCase() {
		$(".active").slideUp(500, function() {
			$(this).removeClass("active").empty();
			$.post(main_domain + "/index.php/presentasi/training/getTraining", { CourseID: localStorage.course, YearID: localStorage.year }, function(data) {
				if (data.status == "OK") {
					data = data.data;
					if (data.length > 0) {
						refreshBoard(data[0]);
						changeView();
						refreshTime();
						changeNo(1);
					}
				}
			}, "json");
		});

		$.ajax({
			url: main_domain + "/index.php/presentasi/training/getTraining", 
			type: "POST",
			data: { CourseID: localStorage.course, YearID: localStorage.year }, 
			dataType: "JSON",
			success: function(data) {
				if (data.status == "OK") {
					data = data.data;
					if (data.length > 0) {
						Apps.prop = data;
						totalNo = data.length;
					} 
				}
			},
			async: false
		});
	}

	function updateTime() {
		var h = ('0' + parseInt((time - elapse) / 3600)).slice(-2),
			m = ('0' + parseInt((time - elapse) % 3600 / 60)).slice(-2),
			s = ('0' + ((time - elapse) % 3600 % 60)).slice(-2);
		$('#timer').text('Time left: ' + h + ':' + m + ':' + s);
		if (elapse++ == time) {
			clearInterval(timer);
			flag = 1; // give popups message dialog persentase
			$('#btnFinish').trigger('click'); // times up : popup message dialog
		}
	}

	function refreshTime() {
		if (timer) {
			clearInterval(timer);
		}
		if ($('#content-exam').attr('data-view') == 'main') {
			elapse = 0;
			updateTime();
			timer = setInterval(updateTime, 1000);
		}
	}

	function checkEmptyAns(){
		var currNo = $('.no-case').text();
		var nextAns = $('.curr-no').find('div.label-user-answer').attr('class').split(' ')[1];
		
		if($('.odd-answer, .even-answer').find('.ans-none').length != 0){
			if(nextAns != 'ans-none'){
				/* pindah ke nextAns yang ans-none */
				currNo = parseInt(currNo) + 1;
				changeNo(currNo);
				checkEmptyAns();
			}
		}
	}

	function getLastEmptyNumber(){
		var lastNumber = $('.odd-answer, .even-answer').find('.label-user-answer').length;
		var countEmptyAns = $('.odd-answer, .even-answer').find('.ans-none').length;
		var flag = 0;
		for(var i=0; i<=countEmptyAns; i++){
			if($('.no-'+lastNumber).siblings().attr('class').split(' ')[1] != 'ans-none'){
				lastNumber -= 1;
				flag = 1;
			}
		}
		if (flag == 1) 
			if (countEmptyAns != 0) 
				lastNumber = $('.odd-answer, .even-answer').find('.ans-none').siblings().attr('class').split(' ')[1].split('-')[1];
		return lastNumber;
	}

	function tutorialCorection(answer){
		for(var i in Apps.prop){
			if(answer[i] != Apps.prop[i]['Answer']){
				$('.no-'+(parseInt(i)+1)).parent().css("border","1px solid rgb(255, 0, 0)");
			}
			else{
				$('.no-'+(parseInt(i)+1)).parent().css("border","1px solid rgb(46, 240, 38)");
			}
		}
	}

	$(function() {
		$("#wrapper").css('padding-left', 0);
		init();

		$("#btnAction button").click(function(e) {
			e.preventDefault();
			localStorage.curr = $(this).parent().attr("data-goto");
			changeView();
			if (localStorage.curr == "year")
				changeYear()
			else if (localStorage.curr == "course")
				changeCourse()
		});

		$(document).on("click", ".course_card", function(e) {
			e.preventDefault();
			var CourseID = $(this).attr("data-attr");
			localStorage.curr = $(this).attr("data-goto");
			localStorage.course = CourseID;
			changeYear();
		}).on("click", ".year_card", function(e) {
			e.preventDefault();
			var YearID = $(this).attr("data-attr");
			localStorage.curr = $(this).attr("data-goto");
			localStorage.year = YearID;
			refreshCase();
		}).on('click', '.odd-answer, .even-answer', function(e) { //event click for jumping to number
			e.preventDefault();
			var classNo = $(this).children().attr('class').split(' ')[1];
			changeNo(classNo.substr(classNo.indexOf('-') + 1));
		}).on('click', '.wrapper-case:not(:first-child)', function(e) { //event click for answering current case
			var ans = $(this).children().attr('class').split(' ')[1].substr($(this).children().attr('class').split(' ')[1].indexOf('-',7)+1); //user answer
			var currNo = $('.no-case').text(); //current number
			$('.no-' + currNo).siblings() //change answer for current number
				.removeAttr('class').addClass('label-user-answer ans-' + ans).text(ans);
			var emptyAns = $('.odd-answer, .even-answer').find('.ans-none').siblings().attr('class');

			if(currNo < getLastEmptyNumber()){ //validasi sampai nomor terakhir sdh di isi 
				checkEmptyAns();
			}
			else if($('.odd-answer, .even-answer').find('.ans-none').length != 0){
				if (emptyAns != undefined) {
					var nextNo = emptyAns.split('-')[3];
					changeNo(nextNo);
				}
			}
		}).on('click', '#btnPass', function(e) {
			e.preventDefault();
			var currNo = parseInt($('.no-case').text());
			var nextNo = currNo == totalNo ? 1 : currNo + 1;
			changeNo(nextNo);
		}).on('click', '#btnFinish', function(e) {
			e.preventDefault();
			var countEmptyAns = $('.odd-answer, .even-answer').find('.ans-none').length;
			var countNumber = $('.odd-answer, .even-answer').find('.label-user-answer').length;
			var jawabanUser = [];
			for(var i=0; i<countNumber; i++) {
				var classAnswer = $('.no-'+(parseInt(i)+1)).siblings().attr('class').split(' ')[1];
				jawabanUser[i] = classAnswer.substr(classAnswer.indexOf('-')+1).toUpperCase();
			}
			var correctAnswer = 0;
			var wrongAnswer = 0;
			for(var i in Apps.prop){
				if(jawabanUser[i] != Apps.prop[i]['Answer']){
					wrongAnswer += 1;
				}
				else{
					correctAnswer += 1;	
				}
			}
			var persentaseAnswer = Math.floor(parseFloat(correctAnswer)/parseFloat(countNumber)*1000)/10;
			
			//validasi pembulatan jika persentase bilangan bulat
			// if(persentaseAnswer.substr(persentaseAnswer.indexOf('.')+1) == 0)
			// 	persentaseAnswer = (parseFloat(correctAnswer)/parseFloat(wrongAnswer)*100).toFixed(0);
			
			changeClosePage(countEmptyAns, correctAnswer, persentaseAnswer);
		});

		if (localStorage.curr != "main") {
			localStorage.removeItem('curr');
			localStorage.removeItem('view');
			localStorage.removeItem('course');
			localStorage.removeItem('year');
		}
		if ( ! localStorage.curr) {
			localStorage.curr = $('#content-exam').attr('data-curr');
			localStorage.view = $('#exam .panel .panel-body').attr('data-view');
		} else {
			if (localStorage.view == 'main') {
				refreshCase();
			}
			else {
				changeCourse();
				changeYear();
			}
			changeView();
		}
	});
</script>

<div class="row" id="exam">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Pelatihan</h1></div>
			<div class="panel-body" id="content-exam" data-view>
				<div id="btnAction"><button class='btn btn-default'><i class='glyphicon glyphicon-arrow-left'></i></button></div>
				<div id="exam-course"></div>
				<div id="exam-year" style="display: none;"></div>
				<div id="exam-main" style="display: none;"></div>
			</div>
		</div>
	</div>
</div>