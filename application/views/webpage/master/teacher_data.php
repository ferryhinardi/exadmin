<?php $domain = $this->config->item('domain'); ?>
<?php $year = (isset($year) ? $year : array()); ?>

<script type="text/javascript">
	var years = <?php echo json_encode($year); ?>;
</script>

<script type="text/javascript">
	$(function() {
		function setTeacher (value) {
			value = value || {};
			var UserID = (typeof value.UserID == "undefined") ? "" : value.UserID,
				FullName = (typeof value.FullName == "undefined") ? "" : value.FullName,
				BirthPlace = (typeof value.BirthPlace == "undefined") ? "" : value.BirthPlace,
				BirthDate = (typeof value.BirthDate == "undefined") ? "" : value.BirthDate,
				Gender = (typeof value.Gender == "undefined") ? "L" : value.Gender,
				Address = (typeof value.Address == "undefined") ? "" : value.Address;

			$("#teacherID").val(UserID);
			
			if (value.YearID == null)
				$('#yearTeacher').prop('selectedIndex', 0)
			else
				$("#yearTeacher").val(value.YearID);

			$("#teacherName").val(FullName);
			$("#birthPlaceTeacher").val(BirthPlace);
			if (BirthDate != "") 
				$("#birthDateTeacher").val(moment(BirthDate).format(Apps.dateFormat));
			else
				$("#birthDateTeacher").val(moment(new Date()).format(Apps.dateFormat));	
			
			$("#genderTeacher[value='"+Gender+"']").prop('checked', true);
			$("#addressTeacher").val(Address);
		}

		function prepareUpload(event){
			files = event.target.files;
			if (files && files[0]) {
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);
			}
		}

		Apps.Popup({
			name: "confirm-popup",
			title: "Confirmation",
			body: "<input type='hidden' id='teacherIDDeletePopup' name='teacherIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-teacher'>Yes</button>"
		});

		Apps.DataTableServer({
			selector: "#content-teacher",
			url: main_domain + "/index.php/administrasi/teacher/getTeacher",
			field: [
				{ "title": "Teacher ID", "data": "UserID" },
				{ "title": "FullName", "data": "FullName" },
				{ "title": "BirthPlace", "data": "BirthPlace" },
				{ "title": "BirthDate", "data": "BirthDate",
					"render": function ( data, type, full, meta ) { 
						return moment(data).format(Apps.dateFormat)
					}
				},
				{ "title": "Gender", "data": "Gender",
					"render": function ( data, type, full, meta ) { 
						if (data == 'L')
							return 'Male';
						else if (data == 'P')
							return 'Female';
						else
							return '';
					}
				},
				{ "title": "Address", "data": "Address" },
				{
					"title": "Action",
					"className": 'iActionTeacher inline',
					"sortable": false,
					"data": "UserID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewTeacher", text: "New" },
				{ type: "button", cls: "btn-success", icon: "open-file", name: "btnImportTeacher", text: "Import" },
			],
			onCallback: function() {
				$('#btnNewTeacher').click(function(e) {
					e.preventDefault();
					setTeacher();
					$("#teacherID").prop("disabled", false);
					$('#modalNewTeacher').modal('show');
				});

				$('#btnImportTeacher').click(function(e) {
					e.preventDefault();
					$('#modalUploadTeacher').modal('show');
				});
				
				$('.iActionTeacher .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/teacher/getTeacher", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var teacher = data[0];
								setTeacher(teacher);
							}
						}
					}, "json");
					$('#modalNewTeacher').modal('show');
				});

				$('.iActionTeacher .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#teacherIDDeletePopup").val(id);
					$("#confirm-popup").modal("show");
				});

				$("#btnConfirm-teacher").off().click(function(e) {
					var id = $("#teacherIDDeletePopup").val();
					$.post(main_domain + "/index.php/administrasi/teacher/deleteTeacher", {id: id}, function(data) {
						if (data == 'refresh') window.location.reload();
					});
				});
			}
		});

		Apps.Popup({
			name: "modalNewTeacher",
			title: "Form Teacher",
			body: [{
				type: "text",
				name: "teacherID",
				text: "Nomor Guru",
				attr: "disabled"
			}, {
				type: "select",
				name: "yearTeacher",
				text: "Tahun Angkatan",
				items: years
			}, {
				type: "text",
				name: "teacherName",
				text: "Nama"
			}, {
				type: "text",
				name: "birthPlaceTeacher",
				text: "Tempat Lahir"
			}, {
				type: "datepicker",
				name: "birthDateTeacher",
				text: "Tanggal Lahir"
			}, {
				type: "radio",
				name: "genderTeacher",
				text: "Jenis Kelamin",
				items: [{
					text: 'Laki - Laki',
					value: 'L',
					attr: 'checked'
				}, {
					text: 'Perempuan',
					value: 'P'
				}]
			}, {
				type: "textarea",
				name: "addressTeacher",
				text: "Alamat"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveTeacher' class='btn btn-primary'>Save</button>"
		});

		$("#btnSaveTeacher").click(function(e) {
			var data = {
				userID: $("#teacherID").val(),
				yearID: $("#yearTeacher").val(),
				name: $("#nameTeacher").val(),
				birthPlace: $("#birthPlaceTeacher").val(),
				birthDate: moment($("#birthDateTeacher").val()).format("YYYY-MM-DD"),
				gender: $("#genderTeacher:checked").val(),
				address: $("#addressTeacher").val()
			}

			$.post(main_domain+"/index.php/administrasi/teacher/saveTeacher", data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		Apps.Popup({
			name: "modalUploadTeacher",
			title: "Upload Excel",
			body: [{
				type: "file",
				name: "file",
				text: "Upload File",
				attr: "accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'"
			}, {
				type: "link",
				name: "download-templete-teacher",
				link: main_domain + "/media/upload - user.xlsx",
				linkText: "Templete-Upload-Teacher.xlsx"
			}],
			files: true,
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-success' id='btnRunTeacher'>Run</button>"
		});

		$("input[type=file]").change(function(e) {
			attachmentFile = e.target.files;

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();

				if(extension != 'xls' && extension != 'xlsx' && extension != 'csv') {
					alert("File extension must be Excel Format");
					attachmentFile = null;
				}
				else {
					Apps.item.form = new FormData();
					Apps.item.form.append('import', attachmentFile[0]);
				}
			}
			prepareUpload(e);
		});

		$("#btnRunTeacher").click(function(e) {
			var data = JSON.stringify({ table: ['MsTeacher', 'MsUser'], page: 'administrasi/teacher' });
			Apps.item.form.append('save', data);
			$.ajax({
				url: main_domain + '/index.php/import', 
				dataType : "json",
				data: Apps.item.form, 
				cache: false,
				secureuri : false,
				processData : false,
				contentType : false,
				type: "POST",
				success: function(data) {
					if (typeof data === 'object')
						window.location.reload();
				}
			});
		});
	});
</script>

<div class="row" id="teacher-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Guru</h1></div>
			<div class="panel-body" id="content-teacher"></div>
		</div>
	</div>
</div>