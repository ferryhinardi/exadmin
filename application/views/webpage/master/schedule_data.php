<?php $domain = $this->config->item('domain'); ?>
<?php $years = (isset($years) ? $years : array()); ?>
<?php $classes = (isset($classes) ? $classes : array()); ?>
<?php $courses = (isset($courses) ? $courses : array()); ?>
<?php $shifts = (isset($shifts) ? $shifts : array()); ?>

<script type="text/javascript">
	var years = <?php echo json_encode($years); ?>;
	var classes = <?php echo json_encode($classes); ?>;
	var courses = <?php echo json_encode($courses); ?>;
	var shifts = <?php echo json_encode($shifts); ?>;
</script>

<script type="text/javascript">
	$(function() {

		function setShift (value) {
			value = value || {};
			var ShiftID = (typeof value.ShiftID == "undefined") ? "" : value.ShiftID,
				ShiftName = (typeof value.ShiftName == "undefined") ? "" : value.ShiftName,
				DateShift = (typeof value.Date == "undefined") ? "" : value.Date,
				StartTime = (typeof value.StartTime == "undefined") ? "" : value.StartTime,
				EndTime = (typeof value.EndTime == "undefined") ? "" : value.EndTime;

			$("#shiftID").val(ShiftID);
			$("#shiftName").val(ShiftName);
			if (DateShift != "") 
				$("#date").val(moment(DateShift).format(Apps.dateFormat));
			else
				$("#date").val(moment(new Date()).format(Apps.dateFormat));
			if (StartTime != "") 
				$("#startTime").val(StartTime);
			else
				$("#startTime").val(moment(new Date()).format(Apps.timeFormat));
			if (EndTime != "") 
				$("#endTime").val(EndTime);
			else
				$("#endTime").val(moment(new Date()).format(Apps.timeFormat));
		}

		function setSchedule (value) {
			value = value || {};
			$("#scheduleID").val(value.ScheduleID);
			if (value.ShiftID == null)
				$('#shiftSchedule').prop('selectedIndex', 0)
			else
				$("#shiftSchedule").val(value.ShiftID);
			if (value.YearID == null)
				$('#yearSchedule').prop('selectedIndex', 0)
			else
				$("#yearSchedule").val(value.YearID);
			if (value.ClassID == null)
				$('#classSchedule').prop('selectedIndex', 0)
			else
				$("#classSchedule").val(value.ClassID);
			if (value.CourseID == null)
				$('#courseSchedule').prop('selectedIndex', 0)
			else
				$("#courseSchedule").val(value.CourseID);
		}

		Apps.Popup({
			name: "confirm-popup-shift",
			title: "Confirmation",
			body: "<input type='hidden' id='shiftIDDeletePopup' name='shiftIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-shift'>Yes</button>"
		});

		Apps.Popup({
			name: "modalNewShift",
			title: "Form Shift",
			body: [{
				type: "hidden",
				name: "shiftID"
			}, {
				type: "text",
				name: "shiftName",
				text: "Nama Shift"
			}, {
				type: "datepicker",
				name: "date",
				text: "Date"
			}, {
				type: "timepicker",
				name: "startTime",
				text: "Start Time"
			}, {
				type: "timepicker",
				name: "endTime",
				text: "End Time"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveShift' class='btn btn-primary'>Save</button>"
		});

		$("#btnSaveShift").click(function(e) {
			e.preventDefault();
			var data = {
				ShiftID: (($("#shiftID").val() == "") ? null : $("#shiftID").val()),
				ShiftName: $("#shiftName").val(),
				Date: moment($("#date").val()).format("YYYY-MM-DD"),
				StartTime: $("#startTime").val(),
				EndTime: $("#endTime").val()
			}
			$.post(main_domain+'/index.php/administrasi/schedule/saveShift', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		})

		Apps.Popup({
			name: "confirm-popup-shift",
			title: "Confirmation",
			body: "<input type='hidden' id='shiftIDDeletePopup' name='shiftIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-shift'>Yes</button>"
		});

		Apps.DataTableServer({
			selector: "#content-shift",
			url: main_domain + "/index.php/administrasi/schedule/getShift",
			field: [
				{ "title": "ShiftID", "data": "ShiftID", "visible": false },
				{ "title": "Shift Name", "data": "ShiftName" },
				{ "title": "Date", "data": "Date",
					"render": function ( data, type, full, meta ) { 
						return moment(data).format(Apps.dateFormat)
					} 
				},
				{ "title": "Start Time", "data": "StartTime",
					"render": function ( data, type, full, meta ) { 
						return data.substring(0,5);
					} 
				},
				{ "title": "End Time", "data": "EndTime",
					"render": function ( data, type, full, meta ) { 
						return data.substring(0,5);
					} 
				},
				{ 
					"title": "Action",
					"className": 'iActionShift inline',
					"sortable": false,
					"data": "ShiftID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION SHIFT",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewShift", text: "New" },
			],
			onCallback: function() {
				$('#btnNewShift').click(function(e) {
					e.preventDefault();
					setShift();
					$('#modalNewShift').modal('show');
				});
				
				$('.iActionShift .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/schedule/getShift", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var shift = data[0];
								setShift(shift);
							}
						}
					}, "json");
					$('#modalNewShift').modal('show');
				});

				$('.iActionShift .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#shiftIDDeletePopup").val(id);
					$("#confirm-popup-shift").modal("show");
				});

				$("#btnConfirm-shift").off().click(function(e) {
					var id = $("#shiftIDDeletePopup").val();
					$.post(main_domain + "/index.php/administrasi/schedule/deleteShift", {id: id}, function(data) {
						if (data == 'refresh') window.location.reload();
					});
				});
			}
		});

		Apps.Popup({
			name: "modalNewSchedule",
			title: "Form Schedule",
			body: [{
				type: "hidden",
				name: "scheduleID"
			}, {
				type: "select",
				name: "shiftSchedule",
				text: "Shift",
				items: shifts
			}, {
				type: "select",
				name: "yearSchedule",
				text: "Year",
				items: years
			}, {
				type: "select",
				name: "classSchedule",
				text: "Class",
				items: classes
			}, {
				type: "select",
				name: "courseSchedule",
				text: "Course",
				items: courses
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveSchedule' class='btn btn-primary'>Save</button>"
		});

		$("#btnSaveSchedule").click(function(e) {
			e.preventDefault();
			var data = {
				ScheduleID: (($("#scheduleID").val() == "") ? null : $("#scheduleID").val()),
				ShiftID: $("#shiftSchedule").val(),
				YearID: $("#yearSchedule").val(),
				ClassID: $("#classSchedule").val(),
				CourseID: $("#courseSchedule").val()
			}

			$.post(main_domain+'/index.php/administrasi/schedule/saveSchedule', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		Apps.Popup({
			name: "confirm-popup-schedule",
			title: "Confirmation",
			body: "<input type='hidden' id='scheduleIDDeletePopup' name='scheduleIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-schedule'>Yes</button>"
		});

		Apps.DataTableServer({
			selector: "#content-schedule",
			url: main_domain + "/index.php/administrasi/schedule/getSchedule",
			field: [
				{ "title": "ScheduleID", "data": "ScheduleID", "visible": false },
				{ "title": "Shift", "data": "ShiftName" },
				{ "title": "Year", "data": "YearName" },
				{ "title": "Class", "data": "ClassName" },
				{ "title": "Course", "data": "CourseName" },
				{ 
					"title": "Action",
					"className": 'iActionSchedule inline',
					"sortable": false,
					"data": "ScheduleID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION SCHEDULE",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewSchedule", text: "New" },
			],
			onCallback: function() {
				$('#btnNewSchedule').click(function(e) {
					e.preventDefault();
					setSchedule();
					$('#modalNewSchedule').modal('show');
				});

				$('.iActionSchedule .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/schedule/getSchedule", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var schedule = data[0];
								setSchedule(schedule);
							}
						}
					}, "json");
					$('#modalNewSchedule').modal('show');
				});

				$('.iActionSchedule .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#scheduleIDDeletePopup").val(id);
					$("#confirm-popup-schedule").modal("show");
				});

				$("#btnConfirm-schedule").off().click(function(e) {
					var id = $("#scheduleIDDeletePopup").val();
					$.post(main_domain + "/index.php/administrasi/schedule/deleteSchedule", {id: id}, function(data) {
						if (data == 'refresh') window.location.reload();
					});
				});
			}
		});
	});
</script>

<div class="row" id="schedule-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Jadwal</h1></div>
			<div class="panel-body">
				<div id="content-shift"></div>
				<div id="content-schedule"></div>
			</div>
		</div>
	</div>
</div>