<?php $domain = $this->config->item('domain'); ?>
<?php $courses = (isset($courses) ? $courses : array()); ?>
<?php $years = (isset($years) ? $years : array()); ?>
<?php $mappingClasses = (isset($mappingClasses) ? $mappingClasses : array()); ?>

<script type="text/javascript">
	var courses = <?php echo json_encode($courses); ?>;
	var years = <?php echo json_encode($years); ?>;
	var mappingClasses = <?php echo json_encode($mappingClasses); ?>;
</script>

<script type="text/javascript">
	$(function() {

		function setCourse (value) {
			value = value || {};
			var CourseID = (typeof value.CourseID == "undefined") ? "" : value.CourseID,
				CourseName = (typeof value.CourseName == "undefined") ? "" : value.CourseName;

			$("#courseID").val(CourseID);
			$("#courseName").val(CourseName);
		}

		function prepareUpload(event){
			files = event.target.files;
			if (files && files[0]) {
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);
			}
		}

		function reloadCard(CourseIDParam, YearIDParam) {
			$.post(main_domain + "/index.php/administrasi/course/getClassMapping", { CourseID: CourseIDParam, YearID: YearIDParam }, function(data) {
				$(".well.well-lg").empty();
				var data_thumbnail = [];
				if (data.status == "OK") {
					data = data.data;
					$.each(data, function(idx, item) {
						data_thumbnail.push({
							data_attr: item.ClassID,
							content: item.ClassName,
							selected: ((item.IsMapping == 1) ? true : false)
						});
					});
				}
				$(".well.well-lg").append(Apps.Thumbnail(data_thumbnail));
			}, "json");
		}

		Apps.Popup({
			name: "confirm-popup",
			title: "Confirmation",
			body: "<input type='hidden' id='courseIDDeletePopup' name='courseIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-course'>Yes</button>"
		});

		Apps.DataTableServer({
			selector: "#content-course",
			url: main_domain + "/index.php/administrasi/course/getCourse",
			field: [
				{ "title": "CourseID", "data": "CourseID", "visible": false },
				{ "title": "CourseName", "data": "CourseName" },
				{ 
					"title": "Action",
					"className": 'iActionCourse inline',
					"sortable": false,
					"data": "CourseID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewCourse", text: "New" },
				{ type: "button", cls: "btn-success", icon: "open-file", name: "btnImportCourse", text: "Import" },
			],
			onCallback: function() {
				$('#btnNewCourse').click(function(e) {
					e.preventDefault();
					$('#modalNewCourse').modal('show');
				});

				$('#btnImportCourse').click(function(e) {
					e.preventDefault();
					$('#modalUploadCourse').modal('show');
				});

				$('.iActionCourse .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/course/getCourse", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var course = data[0];
								setCourse(course);
							}
						}
					}, "json");
					$('#modalNewCourse').modal('show');
				});

				$('.iActionCourse .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#courseIDDeletePopup").val(id);
					$("#confirm-popup").modal("show");
				});

				$("#btnConfirm-course").off().click(function(e) {
					var id = $("#courseIDDeletePopup").val();
					$.post(main_domain + "/index.php/administrasi/course/deleteCourse", {id: id}, function(data) {
						if (data == 'refresh') window.location.reload();
					});
				});
			}
		});

		Apps.Popup({
			name: "modalNewCourse",
			title: "Form Course",
			body: [{
				type: "hidden",
				name: "courseID"
			}, {
				type: "text",
				name: "courseName",
				text: "Nama Matapelajaran"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveCourse' class='btn btn-primary'>Save</button>"
		});

		$("#btnSaveCourse").click(function(e) {
			e.preventDefault();
			var data = {
				CourseID: (($("#courseID").val() == "") ? null : $("#courseID").val()),
				CourseName: $("#courseName").val()
			}

			$.post(main_domain+'/index.php/administrasi/course/saveCourse', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		Apps.Popup({
			name: "modalUploadCourse",
			title: "Upload Excel",
			body: [{
				type: "file",
				name: "file",
				text: "Upload File",
				attr: "accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'"
			}, {
				type: "link",
				name: "download-templete-course",
				link: main_domain + "/media/upload - course.xlsx",
				linkText: "Templete-Upload-Course.xlsx"
			}],
			files: true,
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-success' id='btnRunCourse'>Run</button>"
		});

		$("input[type=file]").change(function(e) {
			attachmentFile = e.target.files;

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();

				if(extension != 'xls' && extension != 'xlsx' && extension != 'csv') {
					alert("File extension must be Excel Format");
					attachmentFile = null;
				}
				else {
					Apps.item.form = new FormData();
					Apps.item.form.append('import', attachmentFile[0]);
				}
			}
			prepareUpload(e);
		});

		$("#btnRunCourse").click(function(e) {
			var data = JSON.stringify({ table: 'MsCourse', page: 'administrasi/course' });
			Apps.item.form.append('save', data);
			$.ajax({
				url: main_domain + '/index.php/import', 
				dataType : "json",
				data: Apps.item.form, 
				cache: false,
				secureuri : false,
				processData : false,
				contentType : false,
				type: "POST",
				success: function(data) {
					if (typeof data === 'object')
						window.location.reload();
				}
			});
		});

		/* Mapping Class & Student */
		Apps.LoadForm([{
			selector: "courses-mapping",
			name: "mapping-course",
			legend: "MAPPING MATAPELAJARAN",
			items: [{
				type: "select",
				name: "course-mapping",
				text: "Course",
				items: courses
			}, {
				type: "select",
				name: "year",
				text: "Year",
				items: years
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "floppy-save", name: "btnSaveMapping-Course", text: "Save" },
			]
		}]);

		Apps.Container({
			selector: "courses-mapping",
			items: mappingClasses
		});

		$("#course-mapping").change(function(e) {
			var CourseID = $(this).val(),
				YearID = $("#year").val();
			reloadCard(CourseID, YearID);
			Apps.prop = [];
		});

		$("#year").change(function(e) {
			var CourseID = $("#course-mapping").val(),
				YearID = $(this).val();
			reloadCard(CourseID, YearID);
			Apps.prop = [];
		});

		$(document).on('click', '.thumbnail', function(e) {
			var ClassID = $(this).attr('data-attr');
			var CourseID = $("#course-mapping").val();
			var YearID = $("#year").val();
			var flag = true;
			Apps.prop = $.grep(Apps.prop, function(item, index) {
				if (item.ClassID === ClassID)
					flag = false;
				return (item.ClassID !== ClassID);
			});
			if (flag)
				Apps.prop.push({ ClassID: ClassID, CourseID: CourseID, YearID: YearID });
		});

		$("#btnSaveMapping-Course").click(function(e) {
			e.preventDefault();
			$.post(main_domain + '/index.php/administrasi/course/saveClassMapping', {data: JSON.stringify(Apps.prop)}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});
	});
</script>

<div class="row" id="course-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Matapelajaran</h1></div>
			<div class="panel-body">
				<div id="content-course"></div>
				<div id="courses-mapping"></div>
			</div>
		</div>
	</div>
</div>