<?php $domain = $this->config->item('domain'); ?>
<?php $classes = (isset($classes) ? $classes : array()); ?>
<?php $years = (isset($years) ? $years : array()); ?>
<?php $mappingStudents = (isset($mappingStudents) ? $mappingStudents : array()); ?>

<script type="text/javascript">
	var classes = <?php echo json_encode($classes); ?>;
	var years = <?php echo json_encode($years); ?>;
	var mappingStudents = <?php echo json_encode($mappingStudents); ?>;
</script>

<script type="text/javascript">
	$(function() {

		function setClass (value) {
			value = value || {};
			var ClassID = (typeof value.ClassID == "undefined") ? "" : value.ClassID,
				ClassName = (typeof value.ClassName == "undefined") ? "" : value.ClassName;

			$("#classID").val(ClassID);
			$("#className").val(ClassName);
		}

		function prepareUpload(event){
			files = event.target.files;
			if (files && files[0]) {
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);
			}
		}

		function reloadCard(ClassIDParam, YearIDParam) {
			$.post(main_domain + "/index.php/administrasi/classes/getStudentMapping", { ClassID: ClassIDParam, YearID: YearIDParam }, function(data) {
				$(".well.well-lg").empty();
				var data_thumbnail = [];
				if (data.status == "OK") {
					data = data.data;
					$.each(data, function(idx, item) {
						data_thumbnail.push({
							data_attr: item.StudentID,
							content: item.Name,
							selected: ((item.IsMapping == 1) ? true : false)
						});
					});
				}
				$(".well.well-lg").append(Apps.Thumbnail(data_thumbnail));
			}, "json");
		}

		Apps.Popup({
			name: "confirm-popup",
			title: "Confirmation",
			body: "<input type='hidden' id='classIDDeletePopup' name='classIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-class'>Yes</button>"
		});

		$("#btnConfirm-class").off().click(function(e) {
			var id = $("#classIDDeletePopup").val();
			$.post(main_domain + "/index.php/administrasi/classes/deleteClass", {id: id}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		Apps.DataTableServer({
			selector: "#content-class",
			url: main_domain + "/index.php/administrasi/classes/getClass",
			field: [
				{ "title": "ClassID", "data": "ClassID", "visible": false },
				{ "title": "ClassName", "data": "ClassName" },
				{ 
					"title": "Action",
					"className": 'iActionClass inline',
					"sortable": false,
					"data": "ClassID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewClass", text: "New" },
				{ type: "button", cls: "btn-success", icon: "open-file", name: "btnImportClass", text: "Import" },
			],
			onCallback: function() {
				$('#btnNewClass').click(function(e) {
					e.preventDefault();
					$('#modalNewClass').modal('show');
				});

				$('#btnImportClass').click(function(e) {
					e.preventDefault();
					$('#modalUploadClass').modal('show');
				});
				
				$('.iActionClass .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/classes/getClass", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var classes = data[0];
								setClass(classes);
							}
						}
					}, "json");
					$('#modalNewClass').modal('show');
				});

				$('.iActionClass .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#classIDDeletePopup").val(id);
					$("#confirm-popup").modal("show");
				});
			}
		});

		Apps.Popup({
			name: "modalNewClass",
			title: "Form Class",
			body: [{
				type: "hidden",
				name: "classID"
			}, {
				type: "text",
				name: "className",
				text: "Nama Kelas"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveClass' class='btn btn-primary'>Save</button>"
		});

		$("#btnSaveClass").click(function(e) {
			e.preventDefault();
			var data = {
				ClassID: (($("#classID").val() == "") ? null : $("#classID").val()),
				ClassName: $("#className").val()
			}

			$.post(main_domain+'/index.php/administrasi/classes/saveClass', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		Apps.Popup({
			name: "modalUploadClass",
			title: "Upload Excel",
			body: [{
				type: "file",
				name: "file",
				text: "Upload File",
				attr: "accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'"
			},{
				type: "link",
				name: "download-templete-class",
				link: main_domain + "/media/upload - class.xlsx",
				linkText: "Templete-Upload-Class.xlsx"
			}],
			files: true,
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-success' id='btnRunClass'>Run</button>"
		});

		$("input[type=file]").change(function(e) {
			attachmentFile = e.target.files;

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();

				if(extension != 'xls' && extension != 'xlsx' && extension != 'csv') {
					alert("File extension must be Excel Format");
					attachmentFile = null;
				}
				else {
					Apps.item.form = new FormData();
					Apps.item.form.append('import', attachmentFile[0]);
				}
			}
			prepareUpload(e);
		});

		$("#btnRunClass").click(function(e) {
			var data = JSON.stringify({ table: 'MsClass', page: 'administrasi/classes' });
			Apps.item.form.append('save', data);
			$.ajax({
				url: main_domain + '/index.php/import', 
				dataType : "json",
				data: Apps.item.form, 
				cache: false,
				secureuri : false,
				processData : false,
				contentType : false,
				type: "POST",
				success: function(data) {
					if (typeof data === 'object')
						window.location.reload();
				}
			});
		});

		/* Mapping Class & Student */
		Apps.LoadForm([{
			selector: "classes-mapping",
			name: "mapping-class",
			legend: "MAPPING KELAS",
			items: [{
				type: "select",
				name: "class-mapping",
				text: "Class",
				items: classes
			}, {
				type: "select",
				name: "year",
				text: "Year",
				items: years
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "floppy-save", name: "btnSaveMapping-Class", text: "Save" },
			]
		}]);

		Apps.Container({
			selector: "classes-mapping",
			items: mappingStudents
		});

		$("#class-mapping").change(function(e) {
			var ClassID = $(this).val(),
				YearID = $("#year").val();
			reloadCard(ClassID, YearID);
			Apps.prop = [];
		});

		$("#year").change(function(e) {
			var ClassID = $("#class-mapping").val(),
				YearID = $(this).val();
			reloadCard(ClassID, YearID);
			Apps.prop = [];
		});

		$(document).on('click', '.thumbnail', function(e) {
			var StudentID = $(this).attr('data-attr');
			var ClassID = $("#class-mapping").val();
			var YearID = $("#year").val();
			var flag = true;
			Apps.prop = $.grep(Apps.prop, function(item, index) {
				if (item.UserID === StudentID)
					flag = false;
				return (item.UserID !== StudentID);
			});
			if (flag)
				Apps.prop.push({ UserID: StudentID, ClassID: ClassID, YearID: YearID });
		});

		$("#btnSaveMapping-Class").click(function(e) {
			e.preventDefault();
			$.post(main_domain + '/index.php/administrasi/classes/saveStudentMapping', {data: JSON.stringify(Apps.prop)}, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});
	});
</script>

<div class="row" id="class-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Kelas</h1></div>
			<div class="panel-body">
				<div id="content-class"></div>
				<div id="classes-mapping"></div>
			</div>
		</div>
	</div>
</div>	