<?php $domain = $this->config->item('domain'); ?>
<?php $profile = (isset($profile) ? $profile : array()); ?>

<script type="text/javascript">
	var profiles = <?php echo json_encode($profile); ?>;
</script>
<script type="text/javascript">
	$(function() {
		var options = [{
			selector: "content-school",
			name: "form",
			legend: "Entry Data",
			items: [{
				type: "hidden",
				name: "profileID",
				attr: "value='"+((typeof profiles.CompanyID == 'undefined') ? '' : profiles.CompanyID)+"'"
			}, {
				type: "text",
				name: "schoolName",
				text: "Nama Sekolah",
				attr: "value='"+((typeof profiles.CompanyName == 'undefined') ? '' : profiles.CompanyName)+"'"
			}, {
				type: "textarea",
				name: "profileSchool",
				text: "Profile Sekolah"
			}],
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnSaveSchool", text: "Save" },
				{ type: "button", cls: "btn-default", icon: "pencil", name: "btnEditSchool", text: "Edit" },
			]
		}];

		Apps.LoadForm(options);
		$("#profileSchool").html(profiles.CompanyProfile);

		if (typeof profiles == 'object' && typeof profiles.length == 'undefined') {
			$("#schoolName, #profileSchool, #btnSaveSchool").prop("disabled", true);
		}

		$("#btnSaveSchool").click(function(e) {
			e.preventDefault();
			var data = {
				companyID: (($("#profileID").val() == '') ? null : $("#profileID").val()),
				companyName: $("#schoolName").val(),
				companyProfile: $("#profileSchool").val()
			}
			$.post(main_domain+'/index.php/administrasi/profile/saveProfile', data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});

		$("#btnEditSchool").click(function(e) {
			e.preventDefault();
			$("#schoolName, #profileSchool, #btnSaveSchool").prop("disabled", false);
			checkProfile($(this));
		});

		function checkProfile(element) {
			if (typeof profiles == 'object' && typeof profiles.length == 'undefined') {
				$(element).prop("disabled", false);
			}
			else {
				$(element).prop("disabled", true);
			}
		}
	});
</script>

<div class="row" id="profile-school">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Profile Sekolah</h1></div>
			<div class="panel-body" id="content-school"></div>
		</div>
	</div>
</div>