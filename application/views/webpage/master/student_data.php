<?php $domain = $this->config->item('domain'); ?>
<?php $year = (isset($year) ? $year : array()); ?>

<script type="text/javascript">
	var years = <?php echo json_encode($year); ?>;
</script>

<script type="text/javascript">
	$(function() {
		function setStudent (value) {
			value = value || {};
			var UserID = (typeof value.UserID == "undefined") ? "" : value.UserID,
				FullName = (typeof value.FullName == "undefined") ? "" : value.FullName,
				BirthPlace = (typeof value.BirthPlace == "undefined") ? "" : value.BirthPlace,
				BirthDate = (typeof value.BirthDate == "undefined") ? "" : value.BirthDate,
				Gender = (typeof value.Gender == "undefined") ? "L" : value.Gender,
				Address = (typeof value.Address == "undefined") ? "" : value.Address;

			$("#studentID").val(UserID);
			
			if (value.YearID == null)
				$('#yearStudent').prop('selectedIndex', 0)
			else
				$("#yearStudent").val(value.YearID);

			$("#studentName").val(FullName);
			$("#birthPlaceStudent").val(BirthPlace);
			if (BirthDate != "") 
				$("#birthDateStudent").val(moment(BirthDate).format(Apps.dateFormat));
			else
				$("#birthDateStudent").val(moment(new Date()).format(Apps.dateFormat));
			$("#genderStudent[value='"+Gender+"']").prop('checked', true);
			$("#addressStudent").val(Address);
		}

		function prepareUpload(event){
			files = event.target.files;
			if (files && files[0]) {
				var reader = new FileReader();
				reader.readAsDataURL(files[0]);
			}
		}

		Apps.Popup({
			name: "confirm-popup",
			title: "Confirmation",
			body: "<input type='hidden' id='studentIDDeletePopup' name='studentIDDeletePopup' />Are You Sure Want to Delete?",
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>No</button>" +
					"<button type='button' class='btn btn-primary' id='btnConfirm-student'>Yes</button>"
		});

		Apps.DataTableServer({
			selector: "#content-student",
			url: main_domain + "/index.php/administrasi/student/getStudent",
			field: [
				{ "title": "Student ID", "data": "UserID" },
				{ "title": "FullName", "data": "FullName" },
				{ "title": "BirthPlace", "data": "BirthPlace" },
				{ "title": "BirthDate", "data": "BirthDate",
					"render": function ( data, type, full, meta ) { 
						return moment(data).format(Apps.dateFormat)
					}
				},
				{ "title": "Gender", "data": "Gender",
					"render": function ( data, type, full, meta ) { 
						if (data == 'L')
							return 'Male';
						else if (data == 'P')
							return 'Female';
						else
							return '';
					}
				},
				{ "title": "Address", "data": "Address" },
				{ 
					"title": "Action",
					"className": 'iActionStudent inline',
					"sortable": false,
					"data": "UserID",
					"render": function ( data, type, full, meta ) { 
						return "<a class='btn btn-default iEdit'  data-attr="+data+"><i class='glyphicon glyphicon-pencil'></i></a> <a class='btn btn-danger iDelete' data-attr="+data+"><i class='glyphicon glyphicon-remove'></i></a></td>"
					}
				}
			],
			legend: "INFORMATION",
			toolbar: [
				{ type: "button", cls: "btn-primary", icon: "plus", name: "btnNewStudent", text: "New" },
				{ type: "button", cls: "btn-success", icon: "open-file", name: "btnImportStudent", text: "Import" },
			],
			onCallback: function() {
				$('#btnNewStudent').click(function(e) {
					e.preventDefault();
					setStudent();
					$("#studentID").prop("disabled", false);
					$('#modalNewStudent').modal('show');
				});

				$('#btnImportStudent').click(function(e) {
					e.preventDefault();
					$('#modalUploadStudent').modal('show');
				});

				$('.iActionStudent .iEdit').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$.post(main_domain + "/index.php/administrasi/student/getStudent", {id: id}, function(data) {
						if (data.status == "OK") {
							data = data.data;
							if (data.length) {
								var student = data[0];
								setStudent(student);
							}
						}
					}, "json");
					$('#modalNewStudent').modal('show');
				});

				$('.iActionStudent .iDelete').click(function(e) {
					e.preventDefault();
					var id = $(this).attr('data-attr');
					$("#studentIDDeletePopup").val(id);
					$("#confirm-popup").modal("show");
				});

				$("#btnConfirm-student").off().click(function(e) {
					var id = $("#teacherIDDeletePopup").val();
					$.post(main_domain + "/index.php/administrasi/student/deleteStudent", {id: id}, function(data) {
						if (data == 'refresh') window.location.reload();
					});
				});
			}
		});

		Apps.Popup({
			name: "modalNewStudent",
			title: "Form Student",
			body: [{
				type: "text",
				name: "studentID",
				text: "Nomor Siswa",
				attr: "disabled"
			}, {
				type: "select",
				name: "yearStudent",
				text: "Tahun Angkatan",
				items: years
			}, {
				type: "text",
				name: "studentName",
				text: "Nama"
			}, {
				type: "text",
				name: "birthPlaceStudent",
				text: "Tempat Lahir"
			}, {
				type: "datepicker",
				name: "birthDateStudent",
				text: "Tanggal Lahir"
			}, {
				type: "radio",
				name: "genderStudent",
				text: "Jenis Kelamin",
				items: [{
					text: 'Laki - Laki',
					value: 'L',
					attr: 'checked'
				}, {
					text: 'Perempuan',
					value: 'P'
				}]
			}, {
				type: "textarea",
				name: "addressStudent",
				text: "Alamat"
			}],
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' id='btnSaveStudent' class='btn btn-primary'>Save</button>"
		});

		Apps.Popup({
			name: "modalUploadStudent",
			title: "Upload Excel",
			body: [{
				type: "file",
				name: "file",
				text: "Upload File",
				attr: "accept='.csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel'"
			}, {
				type: "link",
				name: "download-templete-student",
				link: main_domain + "/media/upload - user.xlsx",
				linkText: "Templete-Upload-Student.xlsx"
			}],
			files: true,
			footer: "<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>" +
					"<button type='button' class='btn btn-success' id='btnRunStudent'>Run</button>"
		});

		$("input[type=file]").change(function(e) {
			attachmentFile = e.target.files;

			if(attachmentFile.length) {
				extension = attachmentFile[0].name.substr(attachmentFile[0].name.indexOf(".") + 1).toLowerCase();

				if(extension != 'xls' && extension != 'xlsx' && extension != 'csv') {
					alert("File extension must be Excel Format");
					attachmentFile = null;
				}
				else {
					Apps.item.form = new FormData();
					Apps.item.form.append('import', attachmentFile[0]);
				}
			}
			prepareUpload(e);
		});

		$("#btnRunStudent").click(function(e) {
			var data = JSON.stringify({ table: ['MsStudent', 'MsUser'], page: 'administrasi/student' });
			Apps.item.form.append('save', data);
			$.ajax({
				url: main_domain + '/index.php/import', 
				dataType : "json",
				data: Apps.item.form, 
				cache: false,
				secureuri : false,
				processData : false,
				contentType : false,
				type: "POST",
				success: function(data) {
					if (typeof data === 'object')
						window.location.reload();
				}
			});
		});

		$("#btnSaveStudent").click(function(e) {
			e.preventDefault();
			var data = {
				userID: $("#studentID").val(),
				yearID: $("#yearStudent").val(),
				name: $("#studentName").val(),
				birthPlace: $("#birthPlaceStudent").val(),
				birthDate: moment($("#birthDateStudent").val()).format("YYYY-MM-DD"),
				gender: $("#genderStudent:checked").val(),
				address: $("#addressStudent").val()
			}

			$.post(main_domain+"/index.php/administrasi/student/saveStudent", data, function(data) {
				if (data == 'refresh') window.location.reload();
			});
		});
	});
</script>

<div class="row" id="student-data">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading"><h1 class="panel-title">Data Siswa</h1></div>
			<div class="panel-body" id="content-student"></div>
		</div>
	</div>
</div>