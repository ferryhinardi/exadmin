<?php $domain = $this->config->item('domain');?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Company System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<link rel="icon" href="<?=$domain?>/media/image/resource/favicon.ico">
	<link href='http://fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
	<link href="<?=$domain?>/media/css/animate/animate-custom.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/datatable/dataTables.bootstrap.min.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/bootstrap/simple-sidebar.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/bootstrap/logo-nav.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/typeahead/typeaheadjs.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/custom.css" rel="stylesheet" type="text/css">
	<link href="<?=$domain?>/media/css/board.css" rel="stylesheet" type="text/css">

	<script>
		var main_domain = "<?=$domain?>";
	</script>
	<script src="<?=$domain?>/media/js/jquery.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/moment/moment.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/datatable/jquery.dataTables.min.js" type="text/javascript"></script>

	<!-- Javascript Bootstrap -->
	<script src="<?=$domain?>/media/js/bootstrap/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="<?=$domain?>/media/js/datatable/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/bootstrap/javascript/transition.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/bootstrap/javascript/collapse.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/bootstrap/javascript/modal.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/bootstrap/bootstrap-datetimepicker.js" type="text/javascript"></script>

	<!-- Javascript Typeahead -->
	<script src="<?=$domain?>/media/js/typeahead/bloodhound.min.js" type="text/javascript"></script>
	<script src="<?=$domain?>/media/js/typeahead/typeahead.bundle.min.js" type="text/javascript"></script>

	<script type="text/javascript">
		$(function() {
			$.ajaxSetup({
		 		beforeSend: function (xhr){
	 				var horizontal = ($(window).width() - $(".preloader").width()) / 2;
	 				var vertical = ($(window).height() - $(".preloader").height()) / 2;
		 			$(".preloader").css({
	 					left: horizontal,
	 					top: vertical
		 			}).show();
		 		},
 				complete: function(xhr, status) {
		 			$(".preloader").hide();
 				}
			});
		})
	</script>
	<script src="<?=$domain?>/media/js/core.js" type="text/javascript"></script>
</head>
<body>
	<noscript> 
		<style>
			html, body { overflow: hidden; height: 100% }
			body { margin:0; padding:0; }
		</style>
		<div  style="position: fixed; font-size: 18px; z-index: 2; cursor: help; background: #F5E391; width: 100%; color: black; padding: 5px 5px 5px 5px; border: 1px solid;  top:60px; border-color: #000000; height: 100%; text-align: left;">
			<span style="font: bold 20px Arial; color:#000000; background: #F5E391; vertical-align: middle">Please Enable Javascript</span>
			<br /> 
			Sorry this site will not function properly without the use of javascripts.
			The scripts are safe and will not harm your computer in anyway. 
			Adjust your settings to allow scripts for this site and reload the site.
		</div> 
	</noscript>

	<div id="wrapper">
		
		<img src="<?=$domain?>/media/image/resource/loading-x.gif" style="display:none;" class="preloader" alt="" />
		<?= (isset($modalDialog)) ? $modalDialog : ''; ?>

		<!-- Navigation -->
		<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
			<div class="container">
				<!-- Brand and toggle get grouped for better mobile display -->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="<?=$domain?>">
						<img src="http://placehold.it/150x50&text=Logo" alt="">
					</a>
				</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav">
						<?php 
							$navBar = (isset($allowedHeader["navBar"]) ? $allowedHeader["navBar"] : '');
							$menuAccess = (isset($accessRole) ? $accessRole : array());
							foreach ($navBar as $key => $value) { 
							# code...
								foreach ($menuAccess as $menu) {
									# code...
									if ($value["MenuID"] == $menu["MenuID"]) {
										if ($menu["View"] == "1") {
											$item = (isset($value["item"]) ? $value["item"] : array());
											if (Count($item) == 0) {
						?>
						<li><a href="<?=$value["MenuUrl"]?>"><?=$value["MenuName"]?></a></li>
						<?php 
											}
											else {
						?>
						<li role="<?=$value["MenuName"]?>" class="dropdown">
							<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" href="<?=$value["MenuUrl"]?>"><?=$value["MenuName"]?> <span class="caret"></span></a>
							<ul class="dropdown-menu">
						<?php
												foreach ($item as $key2 => $value2) {
													# code...
													foreach ($menuAccess as $menu2) {
														# code...
														if ($value2["MenuID"] == $menu2["MenuID"]) {
															if ($menu2["View"] == "1") {			
						?>							
								<li><a href="<?=$domain.'/'.$value2["MenuUrl"]?>"><?=$value2["MenuName"]?></a></li>
						<?php 
															}
														}
													}
												}
						?>
							</ul>
						</li>
						<?php
											}
										}
									}
								}
							}
						?>
					</ul>
					<ul class="nav navbar-top pull-right">
					<!--
						<li class="dropdown">
							<a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><label id="fullnameLabel" style="display:inline-block;white-space: nowrap;overflow:hidden;width:auto;"><span class="glyphicon glyphicon-bell" aria-hidden="true"></span></label> <b class="caret"></b></a>
							<ul class="dropdown-menu" role="notif">
								<li><a tabindex="-1" href="<?=$domain?>"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Notif 1</a></li>
							</ul>
						</li>
					-->
						<li class="dropdown">
							<a id="drop2" href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"><label id="fullnameLabel" style="display:inline-block;white-space: nowrap;overflow:hidden;width:auto;"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> <?= isset($name) ? $name : ''; ?></label> <b class="caret"></b></a>
							<ul class="dropdown-menu" role="menu">
								<li><a tabindex="-1" href="<?=$domain?>/profile"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> Profile</a></li>
								<li class="divider"></li>
								<li>
									<a tabindex="-1" href="<?=$domain?>/login/logout"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<!-- /.navbar-collapse -->
			</div>
			<!-- /.container -->
		</nav>

		<!-- Put Javascript Here -->
		<script src="<?=$domain?>/media/js/ajaxfileupload/ajaxfileupload.js" type="text/javascript"></script>
		<script src="<?=$domain?>/media/js/globalsetting.js" type="text/javascript"></script>
		<script src="<?=$domain?>/media/js/master.js" type="text/javascript"></script>

		<!-- Put Page Content Here -->
		<?= (isset($pageContent)) ? $pageContent : ''; ?>

	</div>
	<!-- /#wrapper -->
</body>
</html>