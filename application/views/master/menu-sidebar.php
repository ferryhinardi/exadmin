<?php 
	$data = (isset($sideBar)) ? $sideBar : array();
	$domain = $this->config->item('domain');
	$menuAccess = (isset($accessRole) ? $accessRole : array());

	if (count($data) > 0) {
?>
<!-- Sidebar -->
<div id="sidebar-wrapper">
	<ul class="sidebar-nav">
		<?php 
		foreach ($data as $key => $value) { 
			# code...
			foreach ($menuAccess as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) {
		?>
		<li>
			<a href="<?=$value["MenuUrl"]?>"><?=$value["MenuName"]?></a>
		</li>
		<?php 
					}
				}
			}
		} 
		?>
	</ul>
</div>
<!-- /#sidebar-wrapper -->
<?php 
	} 
?>

<!-- Page Content -->
<div id="page-content-wrapper">
	<div class="container-fluid animated fadeIn">
		<div style="padding-bottom:10px;"><a href="#menu-toggle" class="btn btn-default" id="menu-toggle" style="display:none;">Show Menu <span class="glyphicon glyphicon-arrow-right" aria-hidden="true"></span></a></div>

		<!-- PUT ALERT HERE -->
		<?=(isset($alert))?$alert:''; ?>
		<!--PUT WEBPART HERE -->
		<?php foreach ($webpart as $webpartitem):?>
			<?php echo $webpartitem;?>
		<?php endforeach;?>
	</div>
</div>
<!-- /#page-content-wrapper -->