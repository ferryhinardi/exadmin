<?php
class AjaxServer extends CI_Controller {
	
	public function douploadimage(){
		//Load Uploader Library
		$config['upload_path'] = $this->config->item('path').'temp/';
		$config['allowed_types'] = 'gif|jpg|png';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload('qqfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$this->output->set_output( json_encode(array('status' => "-1", 'msg' => $error)));
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());

			//crop image
			$config['image_library'] = 'gd2';
			$config['source_image'] = $config['upload_path'].$data['upload_data']['file_name'];

			//Get file extention
			$info = pathinfo($data['upload_data']['file_name']);
			$extention = $info['extension'];

			$name = uniqid("image_",true).'.'.$extention;

			$config['new_image'] = $this->config->item('path').'userpicture/'.$name;

			$this->load->library('my_image');
			$this->my_image->resize_crop($config);

			//Delete temporary file
			unlink($config['upload_path'].$data['upload_data']['file_name']);

			$this->output->set_output(json_encode(array('status' => "1", 'name' => $name)));
		}
	}
}