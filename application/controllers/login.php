<?php
class Login extends CI_Controller {
	public function index() {
		if($this->session->userdata('loggedin')!=NULL) return redirect('Home');
		if ($this->session->flashdata('errorloginmessage')!=NULL) {
			//Load Error Alert Dialog
			$layout=$this->load->view('alert/error',
				array('alertMessage'=>$this->session->flashdata('errorloginmessage')), true);
			//Load Login View
			$this->load->view('login', array('erroralert'=>$layout));
		}
		else {
			$this->load->view('login');
		}
	}

	public function doLogin() {
		$this->load->model('global_m', 'setting');
		$post = $this->input->post();
		$result = $this->setting->checkLogin($post["UserID"], $post["Password"]);
		if ($result == 1) {
			$User = $this->setting->getInfoLogin($post["UserID"]);
			print_r($User);
			if (count($User) > 0) {
				$this->session->set_userdata('userid', $User[0]['UserID']);
				$this->session->set_userdata('userheadername', $User[0]['Name']);
				$this->session->set_userdata('roleid',$User[0]['RoleID']);
				$this->session->set_userdata('loggedin', true);
				redirect("Home");
			}
		}
		else {
			//Login Failed,Display Error , redisplay form
			switch($result){
				case -1:
					//Password Error
					$this->session->set_flashdata('errorloginmessage', 'Password do not match');
					break;
				case -2:
					//Username Error
					$this->session->set_flashdata('errorloginmessage', 'Username Not Found');
					break;
			}
			redirect('Login');
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect('login');
	}
}
?>