<?php
class Teacher extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();

		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);

		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			if ($value["MenuWebPart"] == "teacher_data") $this->config->set_item('MenuID', $value["MenuID"]);
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}
		if(in_array('teacher_data', $webpart)) {
			// print_r($this->config->item("MenuID"));
			if ($this->session->flashdata('SaveTeacher') != NULL) {
				if ($this->session->flashdata('SaveTeacher') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteTeacher') != NULL) {
				if ($this->session->flashdata('DeleteTeacher') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#student-data'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "teacher_data",
				"webpartData"=> array("year" => $years));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getTeacher() {
		$this->load->model('administrasi/teacher_m', 'teacher');
		$post = $this->input->post("id");
		$data = $this->teacher->getTeacher($post);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveTeacher() {
		$this->load->model('administrasi/teacher_m', 'teacher');
		$post = $this->input->post();
		$result = $this->teacher->saveTeacher($post["userID"], $post["yearID"], $post["name"], $post["birthPlace"], $post["birthDate"], $post["gender"], $post["address"], "ADMIN");
		if($result[0]["Result"] < 0) {
			$this->session->set_flashdata('SaveTeacher', -1);
		}
		else {
			$this->session->set_flashdata('SaveTeacher', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteTeacher() {
		$this->load->model('administrasi/teacher_m', 'teacher');
		$post = $this->input->post("id");
		$result = $this->teacher->deleteTeacher($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteTeacher', -1);
		}
		else {
			$this->session->set_flashdata('DeleteTeacher', 1);
		}
		$this->output->set_output("refresh");
	}
}