<?php
class Classes extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/class_m', 'class');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);

		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$classes = array();
		$class = $this->class->getClass(null);
		foreach ($class as $key => $value) {
			# code...
			$classes[$key]["value"] = $value["ClassID"];
			$classes[$key]["text"] = $value["ClassName"];
		}

		$mappingStudents = array();
		$mappingStudent = $this->class->getStudentMapping($class[0]["ClassID"], $year[0]["YearID"]);
		foreach ($mappingStudent as $key => $value) {
			# code...
			$mappingStudents[$key]["data_attr"] = $value["StudentID"];
			$mappingStudents[$key]["content"] = $value["Name"];
			$mappingStudents[$key]["selected"] = (($value["IsMapping"] == 1) ? true : false);
		}

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}
		if(in_array('class_data', $webpart)) {
			if ($this->session->flashdata('SaveClass') != NULL) {
				if ($this->session->flashdata('SaveClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteClass') != NULL) {
				if ($this->session->flashdata('DeleteClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('MappingClass') != NULL) {
				if ($this->session->flashdata('MappingClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Mapping success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Mapping failed', 'section'=> '#class-data'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "class_data",
				"webpartData"=> array("classes" => $classes, "years" => $years, "mappingStudents" => $mappingStudents));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getClass() {
		$this->load->model('administrasi/class_m', 'class');
		$post = $this->input->post("id");
		$data = $this->class->getClass($post);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveClass() {
		$this->load->model('administrasi/class_m', 'class');
		$post = $this->input->post();
		$result = $this->class->saveClass($post['ClassID'], $post['ClassName'], 'ADMIN');
		if($result[0]["Result"] < 0) {
			$this->session->set_flashdata('SaveClass', -1);
		}
		else {
			$this->session->set_flashdata('SaveClass', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteClass() {
		$this->load->model('administrasi/class_m', 'class');
		$post = $this->input->post("id");
		$result = $this->class->deleteClass($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteClass', -1);
		}
		else {
			$this->session->set_flashdata('DeleteClass', 1);
		}
		$this->output->set_output("refresh");
	}

	public function getStudentMapping() {
		$this->load->model('administrasi/class_m', 'class');
		$post = $this->input->post();
		$data = $this->class->getStudentMapping(
			(isset($post["ClassID"]) ? $post["ClassID"] : null),
			(isset($post["YearID"]) ? $post["YearID"] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveStudentMapping() {
		$this->load->model('administrasi/class_m', 'class');
		$post = json_decode($this->input->post("data"));
		$this->db->trans_begin();
		foreach ($post as $key => $value) {
			# code...
			$result = $this->class->saveStudentMapping($value->UserID, $value->ClassID, $value->YearID);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				break;
			}
			else {
				$this->db->trans_commit();
			}
		}

		if($this->db->trans_status() == 1) {
			$this->session->set_flashdata('MappingClass', 1);
		}
		else {
			$this->session->set_flashdata('MappingClass', -1);
		}

		$this->output->set_output("refresh");
	}
}