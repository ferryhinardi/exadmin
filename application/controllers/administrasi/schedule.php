<?php
class Schedule extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/class_m', 'class');
		$this->load->model('administrasi/course_m', 'course');
		$this->load->model('administrasi/schedule_m', 'schedule');

		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);
		
		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$classes = array();
		$class = $this->class->getClass(null);
		foreach ($class as $key => $value) {
			# code...
			$classes[$key]["value"] = $value["ClassID"];
			$classes[$key]["text"] = $value["ClassName"];
		}

		$courses = array();
		$course = $this->course->getCourse(null);
		foreach ($course as $key => $value) {
			# code...
			$courses[$key]["value"] = $value["CourseID"];
			$courses[$key]["text"] = $value["CourseName"];
		}

		$shifts = array();
		$shift = $this->schedule->getShift(null);
		foreach ($shift as $key => $value) {
			# code...
			$shifts[$key]["value"] = $value["ShiftID"];
			$shifts[$key]["text"] = $value["ShiftName"];
		}

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}

		if(in_array('schedule_data', $webpart)) {
			if ($this->session->flashdata('SaveShift') != NULL) {
				if ($this->session->flashdata('SaveShift') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteShift') != NULL) {
				if ($this->session->flashdata('DeleteShift') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('SaveSchedule') != NULL) {
				if ($this->session->flashdata('SaveSchedule') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteSchedule') != NULL) {
				if ($this->session->flashdata('DeleteSchedule') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#schedule-data'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "schedule_data",
				"webpartData"=> array("years" => $years, "classes" => $classes, "courses" => $courses, "shifts" => $shifts));
		}

		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');
		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getShift() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post("id");
		$data = $this->schedule->getShift($post);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveShift() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post();
		$result = $this->schedule->saveShift((isset($post['ShiftID']) ? $post['ShiftID'] : null), $post['ShiftName'], $post['StartTime'], $post['EndTime'], 'ADMIN');
		if($result[0]["Result"] < 0) {
			$this->session->set_flashdata('SaveShift', -1);
		}
		else {
			$this->session->set_flashdata('SaveShift', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteShift() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post("id");
		$result = $this->schedule->deleteShift($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteShift', -1);
		}
		else {
			$this->session->set_flashdata('DeleteShift', 1);
		}
		$this->output->set_output("refresh");
	}

	public function getSchedule() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post("id");
		$data = $this->schedule->getSchedule($post);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveSchedule() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post();
		$result = $this->schedule->saveSchedule(
			(isset($post['ScheduleID']) ? $post['ScheduleID'] : null), 
			$post['Date'],
			$post['ShiftID'], 
			$post['ClassID'], 
			$post['CourseID'], 
			$post['YearID'], 
			(isset($post['IsVerified']) ? $post['IsVerified'] : false), 
			'ADMIN');
		if($result[0]["Result"] < 0) {
			$this->session->set_flashdata('SaveSchedule', -1);
		}
		else {
			$this->session->set_flashdata('SaveSchedule', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteSchedule() {
		$this->load->model('administrasi/schedule_m', 'schedule');
		$post = $this->input->post("id");
		$result = $this->schedule->deleteSchedule($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteSchedule', -1);
		}
		else {
			$this->session->set_flashdata('DeleteSchedule', 1);
		}
		$this->output->set_output("refresh");
	}
}