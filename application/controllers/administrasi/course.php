<?php
class Course extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/course_m', 'course');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);

		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$courses = array();
		$course = $this->course->getCourse(null);
		foreach ($course as $key => $value) {
			# code...
			$courses[$key]["value"] = $value["CourseID"];
			$courses[$key]["text"] = $value["CourseName"];
		}

		$mappingClasses = array();
		$mappingClass = $this->course->getClassMapping($course[0]["CourseID"], $year[0]["YearID"]);
		foreach ($mappingClass as $key => $value) {
			# code...
			$mappingClasses[$key]["data_attr"] = $value["ClassID"];
			$mappingClasses[$key]["content"] = $value["ClassName"];
			$mappingClasses[$key]["selected"] = (($value["IsMapping"] == 1) ? true : false);
		}

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}
		if(in_array('course_data', $webpart)) {
			if ($this->session->flashdata('SaveCourse') != NULL) {
				if ($this->session->flashdata('SaveCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteCourse') != NULL) {
				if ($this->session->flashdata('DeleteCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('MappingCourse') != NULL) {
				if ($this->session->flashdata('MappingCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Mapping success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Mapping failed', 'section'=> '#course-data'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "course_data",
				"webpartData"=> array("courses" => $courses, "years" => $years, "mappingClasses" => $mappingClasses));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getCourse() {
		$this->load->model('administrasi/course_m', 'course');
		$post = $this->input->post("id");
		$data = $this->course->getCourse($post);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveCourse() {
		$this->load->model('administrasi/course_m', 'course');
		$post = $this->input->post();
		$result = $this->course->saveCourse($post['CourseID'], $post['CourseName'], 'ADMIN');
		if($result[0]["Result"] < 0) {
			$this->session->set_flashdata('SaveCourse', -1);
		}
		else {
			$this->session->set_flashdata('SaveCourse', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteCourse() {
		$this->load->model('administrasi/course_m', 'course');
		$post = $this->input->post("id");
		$result = $this->course->deleteCourse($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteCourse', -1);
		}
		else {
			$this->session->set_flashdata('DeleteCourse', 1);
		}
		$this->output->set_output("refresh");
	}

	public function getClassMapping() {
		$this->load->model('administrasi/course_m', 'course');
		$post = $this->input->post();
		$data = $this->course->getClassMapping(
			(isset($post["CourseID"]) ? $post["CourseID"] : null),
			(isset($post["YearID"]) ? $post["YearID"] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveClassMapping() {
		$this->load->model('administrasi/course_m', 'course');
		$post = json_decode($this->input->post("data"));
		$this->db->trans_begin();
		foreach ($post as $key => $value) {
			# code...
			$result = $this->course->saveClassMapping($value->ClassID, $value->CourseID, $value->YearID);
			if ($this->db->trans_status() === FALSE) {
				$this->db->trans_rollback();
				break;
			}
			else {
				$this->db->trans_commit();
			}
		}

		if($this->db->trans_status() == 1) {
			$this->session->set_flashdata('MappingCourse', 1);
		}
		else {
			$this->session->set_flashdata('MappingCourse', -1);
		}

		$this->output->set_output("refresh");
	}
}