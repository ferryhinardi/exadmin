<?php
class Profile extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('administrasi/profile_m', 'profile');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);

		$profile = $this->profile->getProfile();
		if (count($profile) > 0) $profile = $profile[0];

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}
		if(in_array('profile_school', $webpart)) {
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "profile_school",
				"webpartData"=> array('profile' => $profile));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function saveProfile() {
		$this->load->model('administrasi/profile_m', 'profile');
		$post = $this->input->post();
		$result = $this->profile->saveProfile($post['companyID'], $post['companyName'], $post['companyProfile']);
		$this->output->set_output("refresh");
	}
}