<?php
class Master extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/profile_m', 'profile');
		$this->load->model('administrasi/class_m', 'class');
		$this->load->model('administrasi/course_m', 'course');
		$this->load->model('administrasi/schedule_m', 'schedule');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 5);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 5);

		$profile = $this->profile->getProfile();
		if (count($profile) > 0) $profile = $profile[0];
		
		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$classes = array();
		$class = $this->class->getClass(null);
		foreach ($class as $key => $value) {
			# code...
			$classes[$key]["value"] = $value["ClassID"];
			$classes[$key]["text"] = $value["ClassName"];
		}

		$mappingStudents = array();
		$mappingStudent = $this->class->getStudentMapping($class[0]["ClassID"], $year[0]["YearID"]);
		foreach ($mappingStudent as $key => $value) {
			# code...
			$mappingStudents[$key]["data_attr"] = $value["StudentID"];
			$mappingStudents[$key]["content"] = $value["Name"];
			$mappingStudents[$key]["selected"] = (($value["IsMapping"] == 1) ? true : false);
		}

		$courses = array();
		$course = $this->course->getCourse(null);
		foreach ($course as $key => $value) {
			# code...
			$courses[$key]["value"] = $value["CourseID"];
			$courses[$key]["text"] = $value["CourseName"];
		}

		$mappingClasses = array();
		$mappingClass = $this->course->getClassMapping($course[0]["CourseID"], $year[0]["YearID"]);
		foreach ($mappingClass as $key => $value) {
			# code...
			$mappingClasses[$key]["data_attr"] = $value["ClassID"];
			$mappingClasses[$key]["content"] = $value["ClassName"];
			$mappingClasses[$key]["selected"] = (($value["IsMapping"] == 1) ? true : false);
		}

		$shifts = array();
		$shift = $this->schedule->getShift(null);
		foreach ($shift as $key => $value) {
			# code...
			$shifts[$key]["value"] = $value["ShiftID"];
			$shifts[$key]["text"] = $value["ShiftName"];
		}

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}
		if(in_array('profile_school', $webpart)) {
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "profile_school",
				"webpartData"=> array('profile' => $profile));
		}

		if(in_array('teacher_data', $webpart)) {
			if ($this->session->flashdata('SaveTeacher') != NULL) {
				if ($this->session->flashdata('SaveTeacher') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteTeacher') != NULL) {
				if ($this->session->flashdata('DeleteTeacher') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#student-data'), TRUE);
				}
			}

			$webpartlist[] = array("webpartName"=> "teacher_data",
				"webpartData"=> array("year" => $years));
		}

		if(in_array('student_data', $webpart)) {
			if ($this->session->flashdata('SaveStudent') != NULL) {
				if ($this->session->flashdata('SaveStudent') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#student-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteStudent') != NULL) {
				if ($this->session->flashdata('DeleteStudent') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#student-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#student-data'), TRUE);
				}
			}

			$webpartlist[] = array("webpartName"=> "student_data",
				"webpartData"=> array("year" => $years));
		}

		if(in_array('class_data', $webpart)) {
			if ($this->session->flashdata('SaveClass') != NULL) {
				if ($this->session->flashdata('SaveClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteClass') != NULL) {
				if ($this->session->flashdata('DeleteClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#class-data'), TRUE);
				}
			}

			if ($this->session->flashdata('MappingClass') != NULL) {
				if ($this->session->flashdata('MappingClass') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Mapping success', 'section' =>'#class-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Mapping failed', 'section'=> '#class-data'), TRUE);
				}
			}

			$webpartlist[] = array("webpartName"=> "class_data",
				"webpartData"=> array("classes" => $classes, "years" => $years, "mappingStudents" => $mappingStudents));
		}

		if(in_array('course_data', $webpart)) {
			if ($this->session->flashdata('SaveCourse') != NULL) {
				if ($this->session->flashdata('SaveCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('ImportData') != NULL) {
				if ($this->session->flashdata('ImportData') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Import success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$error = json_decode($this->session->flashdata('ImportData'))->message;
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Error!! Message : '.$error, 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteCourse') != NULL) {
				if ($this->session->flashdata('DeleteCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#course-data'), TRUE);
				}
			}

			if ($this->session->flashdata('MappingCourse') != NULL) {
				if ($this->session->flashdata('MappingCourse') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Mapping success', 'section' =>'#course-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Mapping failed', 'section'=> '#course-data'), TRUE);
				}
			}
			
			$webpartlist[] = array("webpartName"=> "course_data",
				"webpartData"=> array("courses" => $courses, "years" => $years, "mappingClasses" => $mappingClasses));
		}

		if(in_array('schedule_data', $webpart)) {
			if ($this->session->flashdata('SaveShift') != NULL) {
				if ($this->session->flashdata('SaveShift') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteShift') != NULL) {
				if ($this->session->flashdata('DeleteShift') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('SaveSchedule') != NULL) {
				if ($this->session->flashdata('SaveSchedule') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#schedule-data'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteSchedule') != NULL) {
				if ($this->session->flashdata('DeleteSchedule') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#schedule-data'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#schedule-data'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "schedule_data",
				"webpartData"=> array("years" => $years, "classes" => $classes, "courses" => $courses, "shifts" => $shifts));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'master');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}
}
?>