<?php
class Menu extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$menu_sort_level = array();
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$menu_sort_level[$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$menu_sort_level[$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$menu_sort_level[$key]["item"][$key2] = $value2;
				$Level3 = $this->menu->getMenu(3, $value2["MenuID"]);
				$menu_sort_level[$key]["item"][$key2]["item"] = array();
				foreach ($Level3 as $key3 => $value3) {
					# code...
					$menu_sort_level[$key]["item"][$key2]["item"][$key3] = $value3;
				}
			}
		}

		if(in_array('master', array('master'))) {
			if ($this->session->flashdata('MenuTransaction') != NULL) {
				if ($this->session->flashdata('MenuTransaction') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Menu Update success', 'section' =>'#menu-management'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Menu Update failed', 'section'=> '#menu-management'), TRUE);
				}
			}

			if ($this->session->flashdata('MenuDelete') != NULL) {
				if ($this->session->flashdata('MenuDelete') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Menu Delete success', 'section' =>'#menu-management'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Menu Delete failed', 'section'=> '#menu-management'), TRUE);
				}
			}

			if ($this->session->flashdata('MenuAccessTransaction') != NULL) {
				if ($this->session->flashdata('MenuAccessTransaction') != "") {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Menu Access success', 'section' =>'#menu-access'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'MenuID '.$this->session->flashdata('MenuAccessTransaction').' Access failed', 'section'=> '#menu-access'), TRUE);
				}
			}

			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "master",
				"webpartData"=> array("menuAccess" => $menu_sort_level));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'menu');

		$pageData["sideBar"] = $this->menu->getMenu(3, 0);

		$pageData["accessRole"] = array(
									array('MenuID' => 11, 'View' => 1, 'Create' => 1, 'Update' => 1, 'Delete' => 1),
									array('MenuID' => 12, 'View' => 1, 'Create' => 1, 'Update' => 1, 'Delete' => 1));

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getAllMenu() {
		$this->load->model('menu_m', 'menu');
		$data = $this->menu->getMenu(null, null);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function addEditMenu() {
		$this->load->model('menu_m', 'menu');
		$data = $this->input->post();
		$result = $this->menu->createMenu(
			isset($data["MenuID"]) ? $data["MenuID"] : null, 
			isset($data["MenuName"]) ? $data["MenuName"] : null, 
			isset($data["MenuLevel"]) ? $data["MenuLevel"] : null, 
			(isset($data["MenuHeader"]) && $data["MenuHeader"] != 0) ? $data["MenuHeader"] : null, 
			isset($data["MenuWebPart"]) ? $data["MenuWebPart"] : null, 
			isset($data["MenuIndex"]) ? $data["MenuIndex"] : null, 
			isset($data["MenuUrl"]) ? $data["MenuUrl"] : null);

		if($result[0]["Result"] < 0) {
			//failed , set menu data
			$this->session->set_flashdata('MenuTransaction', -1);
		}
		else {
			//success , set menu data
			$this->session->set_flashdata('MenuTransaction', 1);
		}

		//Send refresh command to client
		$this->output->set_output("refresh");
	}

	public function deleteMenu() {
		$this->load->model('menu_m', 'menu');
		$data = $this->input->post();
		$result = $this->menu->deleteMenu(isset($data["MenuID"]) ? $data["MenuID"] : null);

		if($result < 0) {
			//failed
			$this->session->set_flashdata('MenuDelete', -1);
		}
		else {
			//succes
			$this->session->set_flashdata('MenuDelete', 1);
		}

		//Send refresh command to client
		$this->output->set_output("refresh");
	}

	public function getAllRole() {
		$this->load->model('menu_m', 'menu');
		$data = $this->menu->getRole(null);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function addAccessMenu() {
		$this->load->model('menu_m', 'menu');
		$data = $this->input->post("data");
		$result_data = "";
		foreach ($data as $key => $value) {
			# code...
			$result = $this->menu->createUpdateAccessRole(
				isset($value["RoleID"]) ? $value["RoleID"] : 0,
				isset($value["MenuID"]) ? $value["MenuID"] : 0,
				isset($value["View"]) ? $value["View"] : 0,
				isset($value["Insert"]) ? $value["Insert"] : 0,
				isset($value["Update"]) ? $value["Update"] : 0,
				isset($value["Delete"]) ? $value["Delete"] : 0);

			if ($result < 0) {
				$result_data += $value["MenuID"] . ':';
			}
		}

		if ($result_data == "")
			$this->session->set_flashdata('MenuAccessTransaction', $result_data);
		else {
			$this->session->set_flashdata('MenuAccessTransaction', $result_data);
		}

		//Send refresh command to client
		$this->output->set_output("refresh");
	}

	public function getAccessMenu() {
		$this->load->model('menu_m', 'menu');
		$data = $this->input->post();
		$result = $this->menu->getAccessRole(isset($data["RoleID"]) ? $data["RoleID"] : null, null);

		$this->output->set_output(json_encode($result));
	}
}
?>