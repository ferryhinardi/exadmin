<?php
class Home extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();

		//Check Logged in
		if($this->session->userdata('loggedin')==NULL) return redirect('login');

		// Get Menu Header
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			// Get Submenu
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		//Load Webpartlist into page data
		$webpartlist[] = array("webpartName"=> "template",
			"webpartData"=> array());
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'home');

		$pageData["sideBar"] = array(
								array('MenuID' => 13, 'MenuUrl'=> '#', 'MenuName'=> 'Dashboard'),
								array('MenuID' => 14, 'MenuUrl'=> '#', 'MenuName'=> 'Shortcuts'),
								array('MenuID' => 15, 'MenuUrl'=> '#', 'MenuName'=> 'Overview'),
								array('MenuID' => 16, 'MenuUrl'=> '#', 'MenuName'=> 'Events'),
								array('MenuID' => 17, 'MenuUrl'=> '#', 'MenuName'=> 'About'),
								array('MenuID' => 18, 'MenuUrl'=> '#', 'MenuName'=> 'Services'),
								array('MenuID' => 19, 'MenuUrl'=> '#', 'MenuName'=> 'Contact'));

		$pageData["accessRole"] = $this->menu->getAccessRole(isset($data["RoleID"]) ? $data["RoleID"] : 1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getAll() {
		$this->load->model('template_m', 'template');
		$data = $this->template->getAll();
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}
}
?>