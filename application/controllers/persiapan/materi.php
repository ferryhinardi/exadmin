<?php
class Materi extends CI_Controller {
	public function __construct()  {
		parent::__construct();
		$this->load->library('upload');
		$this->load->helper("file");
	}

	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/course_m', 'course');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}
		
		$years = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$years[$key]["value"] = $value["YearID"];
			$years[$key]["text"] = $value["YearName"];
		}

		$courses = array();
		$course = $this->course->getCourse(null);
		foreach ($course as $key => $value) {
			# code...
			$courses[$key]["value"] = $value["CourseID"];
			$courses[$key]["text"] = $value["CourseName"];
		}

		$Level3 = $this->menu->getMenu(3, 6);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 6);

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}

		if(in_array('input_matapelajaran', $webpart)) {
			if ($this->session->flashdata('SaveMateri') != NULL) {
				if ($this->session->flashdata('SaveMateri') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#input-matapelajaran'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#input-matapelajaran'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteMateri') != NULL) {
				if ($this->session->flashdata('DeleteMateri') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#input-matapelajaran'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#input-matapelajaran'), TRUE);
				}
			}

			if ($this->session->flashdata('SaveExam') != NULL) {
				if ($this->session->flashdata('SaveExam') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#input-matapelajaran'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#input-matapelajaran'), TRUE);
				}
			}

			if ($this->session->flashdata('DeleteExam') != NULL) {
				if ($this->session->flashdata('DeleteExam') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Delete success', 'section' =>'#input-matapelajaran'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Delete failed', 'section'=> '#input-matapelajaran'), TRUE);
				}
			}

			if ($this->session->flashdata('SaveTypeScore') != NULL) {
				if ($this->session->flashdata('SaveTypeScore') > 0) {
					$alert = $this->load->view('alert/success',array('alertMessage'=> 'Save success', 'section' =>'#input-matapelajaran'), TRUE);
				}
				else {
					$alert = $this->load->view('alert/error',array('alertMessage'=> 'Save failed', 'section'=> '#input-matapelajaran'), TRUE);
				}
			}
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "input_matapelajaran",
				"webpartData"=> array("course" => $courses, "year" => $years));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'materi');

		$pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getMateri() {
		$this->load->model('persiapan/materi_m', 'materi');
		$post = $this->input->post();
		$data = $this->materi->getMateri(
			(isset($post['MateriID']) ? $post['MateriID'] : null), 
			(isset($post['CourseID']) ? $post['CourseID'] : null),
			(isset($post['YearID']) ? $post['YearID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function FileType() {
		$this->load->model('global_m', 'setting');
		$result = $this->setting->getTypeFile(0);
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $result)));
	}

	public function upload() {
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/course_m', 'course');
		$courseName = "temp";
		$yearName = "temp";
		$typeName = "temp";
		$error = "";
		$result = null;
		if ($this->input->post("save")) {
			$param = json_decode($this->input->post("save")); 
			$fileName = $_FILES['upload']['name'];
			$course = $this->course->getCourse(isset($param->CourseID) ? $param->CourseID : null);
			$year = $this->setting->getYear(isset($param->YearID) ? $param->YearID : null);
			$typeFile = $this->setting->getTypeFile($param->Type);
			if (count($course) > 0) 
				$courseName = $course[0]["CourseName"];
			if (count($year) > 0) 
				$yearName = $year[0]["YearName"];
			if (count($typeFile) > 0) 
				$typeName = $typeFile[0]["TypeName"];
			
			$dir = $this->config->item("directory") . $this->config->item("path") . $typeName . '/' . $courseName. '/' .$yearName;
			if (!file_exists($dir))
				mkdir($dir, 0777, TRUE);

			$ext = explode(".", $fileName)[1];
			$file = explode(".", $fileName)[0]. "_" .date("YmdHis");
			$fileID = uniqid();
			$upload_path = $this->config->item("path") . $typeName . '/' . $courseName. '/' .$yearName . '/';
			$file_name = str_replace(" ", "_", $file. "." .$ext);
			$config['upload_path'] = '.' . $upload_path;
			$config['file_name'] = $file_name;
			$config['allowed_types'] = '*';
			$config['max_size'] = 20000;
			$this->upload->initialize($config);
			
			if(! $this->upload->do_upload('upload') ) 
				$error = $this->upload->display_errors();
			else 
				$result = $this->setting->saveFile($fileID, $fileName, ($upload_path. $file_name), $param->Type, 'ADMIN')[0];
		}
		$this->output->set_output(json_encode(array("result"=> $result, "error"=> $error)));
	}

	public function saveMateri() {
		$this->load->model('persiapan/materi_m', 'materi');
		$post = $this->input->post();
		$result = $this->materi->saveMateri($post["CourseID"], $post["YearID"], $post["MateriID"], $post["FileID"], $post["Subject"], $post["Description"], "ADMIN");
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('SaveMateri', -1);
		}
		else {
			$this->session->set_flashdata('SaveMateri', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteMateri() {
		$this->load->model('persiapan/materi_m', 'materi');
		$post = $this->input->post("id");
		$result = $this->materi->deleteMateri($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteMateri', -1);
		}
		else {
			$this->session->set_flashdata('DeleteMateri', 1);
		}
		$this->output->set_output("refresh");
	}

	public function getOption() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$data = $this->exam->getOption(
			(isset($post['OptionID']) ? $post['OptionID'] : null), 
			(isset($post['ExamID']) ? $post['ExamID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function getExam() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$data = $this->exam->getExam(
			(isset($post['ExamID']) ? $post['ExamID'] : null), 
			(isset($post['CourseID']) ? $post['CourseID'] : null),
			(isset($post['YearID']) ? $post['YearID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function getExercise() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$data = $this->exam->getExercise((isset($post['CourseID']) ? $post['CourseID'] : null),
			(isset($post['YearID']) ? $post['YearID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveExam() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$option = $post["option"];
		
		$error = 0;
		$this->db->trans_begin();
		$result = $this->exam->saveExam($post['CourseID'], $post['YearID'],
			(isset($post['ExamID']) ? $post['ExamID'] : null), 
			((isset($value["FileID"]) && $value["FileID"] != "") ? $value["FileID"] : null), 
			(isset($post['No']) ? $post['No'] : null), 
			(isset($post['Question']) ? $post['Question'] : null), 
			(isset($post['Answer']) ? $post['Answer'] : null), 'ADMIN');

		if ($result[0]["Result"] > 0) {
			$ExamID = $result[0]["NewExamID"];
			foreach ($option as $key => $value) {
				# code...
				if (!$this->exam->optionExists($value['OptionID']) && ($post['ExamID'] != null && $post['ExamID'] != "")) {
					$ExamID = $post['ExamID'];
				}
				$resultOption = $this->exam->saveOption(
					(isset($value['OptionID']) ? $value['OptionID'] : null), 
					$ExamID, 
					((isset($value["FileID"]) && $value["FileID"] != "") ? $value["FileID"] : null), 
					$value["Option"], 
					$value["Description"], 'ADMIN');
				if ($resultOption[0]["Result"] == 0) {
					$error = 1;
					break;
				}
			}
		}
		else {
			$error = 1;
			$this->db->trans_rollback();
		}

		if ($error == 0) {
			$this->db->trans_commit();
			$this->session->set_flashdata('SaveExam', 1);
		}
		else {
			$this->db->trans_rollback();
			$this->session->set_flashdata('SaveExam', -1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteExam() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post("id");
		$result = $this->exam->deleteExam($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteExam', -1);
		}
		else {
			$this->session->set_flashdata('DeleteExam', 1);
		}
		$this->output->set_output("refresh");
	}

	public function getScoreType() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$data = $this->exam->getScoreType(
			(isset($post['TypeScoreID']) ? $post['TypeScoreID'] : null), 
			(isset($post['CourseID']) ? $post['CourseID'] : null),
			(isset($post['YearID']) ? $post['YearID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveScoreType() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post();
		$result = $this->exam->saveScoreType($post["TypeScoreID"], $post["CourseID"], $post["YearID"], $post["Type"], $post["Weight"], "ADMIN");
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('SaveTypeScore', -1);
		}
		else {
			$this->session->set_flashdata('SaveTypeScore', 1);
		}
		$this->output->set_output("refresh");
	}

	public function deleteScoreType() {
		$this->load->model('persiapan/exam_m', 'exam');
		$post = $this->input->post("id");
		$result = $this->exam->deleteScoreType($post);
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('DeleteScoreType', -1);
		}
		else {
			$this->session->set_flashdata('DeleteScoreType', 1);
		}
		$this->output->set_output("refresh");
	}
}
?>