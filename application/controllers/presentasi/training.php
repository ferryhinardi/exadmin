<?php
class Training extends CI_Controller {
	public function index() {
		$this->load->library('TemplateHelper');
		$this->load->model('menu_m', 'menu');
		$this->load->model('global_m', 'setting');
		$this->load->model('administrasi/course_m', 'course');
		
		$header = array();
		$webpartlist = array();
		$header["navBar"] = array();
		
		$Level1 = $this->menu->getMenu(1, null);
		foreach ($Level1 as $key => $value) {
			# code...
			$header["navBar"][$key] = $value;
			$Level2 = $this->menu->getMenu(2, $value["MenuID"]);
			$header["navBar"][$key]["item"] = array();
			foreach ($Level2 as $key2 => $value2) {
				# code...
				$header["navBar"][$key]["item"][$key2] = $value2;
			}
		}

		$Level3 = $this->menu->getMenu(3, 9);
		$accessRoleLevel3 = $this->menu->getAccessRole(1, 9);

		$webpart = array();
		foreach ($Level3 as $key => $value) {
			# code...
			foreach ($accessRoleLevel3 as $key2 => $value2) {
				# code...
				if ($value["MenuID"] == $value2["MenuID"]) {
					if ($value2["View"] == 1) $webpart[$key] = $value['MenuWebPart'];
				}
			}
		}

		$cards_course = array();
		$course = $this->course->getCourse(null);
		foreach ($course as $key => $value) {
			# code...
			$cards_course[$key]["data_attr"] = $value["CourseID"];
			$cards_course[$key]["content"] = $value["CourseName"];
			$cards_course[$key]["cls"] = "course_card no_select";
			$cards_course[$key]["attr"] = "data-goto='year'";
		}

		$cards_year = array();
		$year = $this->setting->getYear(null);
		foreach ($year as $key => $value) {
			# code...
			$cards_year[$key]["data_attr"] = $value["YearID"];
			$cards_year[$key]["content"] = $value["YearName"];
			$cards_year[$key]["cls"] = "year_card no_select";
			$cards_year[$key]["attr"] = "data-goto='main'";
		}

		if(in_array('exam', $webpart)) {
			//Load Webpartlist into page data
			$webpartlist[] = array("webpartName"=> "exam",
				"webpartData"=> array("cards_course" => $cards_course, "cards_year" => $cards_year));
		}
		
		$pageData = $this->templatehelper->webPartLoader($webpartlist,'training');

		// $pageData['sideBar'] = $Level3;
		$pageData["accessRole"] = $this->menu->getAccessRole(1, null);

		//Put our alert to pageContent
		if(isset($alert)){
			$pageData['alert'] = $alert;
		}

		$pageContent = $this->load->view('master/menu-sidebar', $pageData, TRUE);

		$this->load->view('master/main', 
			array('allowedHeader'=> $header, 'pageContent'=> $pageContent, 'name'=> $this->session->userdata('userheadername')));
	}

	public function getTraining() {
		$this->load->model('presentasi/training_m', 'training');
		$post = $this->input->post();
		$data = $this->training->getTraining(
			(isset($post['CourseID']) ? $post['CourseID'] : null), 
			(isset($post['YearID']) ? $post['YearID'] : null));
		$this->output->set_output(json_encode(array("status"=> "OK", "data"=> $data)));
	}

	public function saveScore() {
		$this->load->model('presentasi/exam_m', 'exam');
		$post = $this->input->post();
		$result = $this->exam->saveScore(
			$post["ScoreID"], 
			$post["TypeScoreID"], 
			$post["Score"], 
			$post["UserID"], 
			$post["ClassID"], 
			$post["CourseID"], "ADMIN"); 
		if($result[0]["Result"] == 0) {
			$this->session->set_flashdata('SaveScore', -1);
		}
		else {
			$this->session->set_flashdata('SaveScore', 1);
		}
		$this->output->set_output("refresh");
	}
}
?>