<?php
class Import extends CI_Controller {
	public function __construct()  {
		parent::__construct();
		$this->load->library(array('upload', 'Excel', 'IOFactory'));
		$this->load->helper("file");
	}

	public function index() {
		$fieldYear = array();
		$fieldDateTime = array();
		$yearid = array();
		$data = array();
		$error = array();
		$duplicate_error = "";
		if ($this->input->post("save")) {
			$param = json_decode($this->input->post("save")); 
			$tables = $param->table;

			if (count($tables) > 1) {
				$table_insert = $tables[0];
			}
			else {
				$table_insert = $tables;
			}

			$fileName = $_FILES['import']['name'];
			$ext = explode(".", $fileName)[1];
			$file = explode(".", $fileName)[0]. "_" .date("YmdHis");
			$config['upload_path'] = './media/attachment/document/';
			$config['file_name'] = $file. "." .$ext;
			$config['allowed_types'] = 'xls|xlsx';
			$config['max_size'] = 20000;

			$this->upload->initialize($config);
			
			if(! $this->upload->do_upload() ) 
				$error['error'] = $this->upload->display_errors();

			$inputFileName = './media/attachment/document/'.str_replace(" ", "_", $file. "." .$ext);

			//  Read your Excel workbook
			try {
				$inputFileType = IOFactory::identify($inputFileName);
				$objReader = IOFactory::createReader($inputFileType);
				$objPHPExcel = $objReader->load($inputFileName);
			} catch(Exception $e) {
				$error["error"] = ('Error loading file "'.pathinfo($inputFileName,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			//  Get worksheet dimensions
			$sheet = $objPHPExcel->getSheet(0);
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL,
				TRUE,
				FALSE);

			ini_set('date.timezone', 'Asia/Jakarta');
			$date = date("YmdHis");
			$initColumn = 'A';
			$initRow = '2';

			for ($i=$initColumn; $i <= $highestColumn; $i++) { 
				# code...
				if ($sheet->getCell($i.$initRow)->getValue() == "yearid") {
					array_push($fieldYear, $i);
				}
			}

			for ($j=($initRow+1); $j <= $highestRow; $j++) { 
				for ($i=$initColumn; $i <= $highestColumn; $i++) { 
					$field = $sheet->getCell($i.$initRow)->getValue();
					$value = ($this->getCellFormat($sheet, $i.$initRow) == 'mm-dd-yy') ? date('Y-m-d', PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell($i.$j)->getValue())) : $sheet->getCell($i.$j)->getValue();
					if (count($fieldYear) > 0) {
						if ($i == $fieldYear[0]) {
							$year = $this->db->get_where("MsTerm", array("YearName" => $value));
							if (count($year->result()) > 0) {
								array_push($yearid, $year->result()[0]->YearID);
							}
							else {
								$this->db->select_max("YearID");
								$year = $this->db->get("MsTerm");
								array_push($yearid, $year + 1);
								$this->db->insert("MsTerm", array("YearName" => $value));
							}
						}
					}

					if ($field == "yearid") 
						$data[$field] = $yearid[$j - ($initRow+1)];
					else 
						$data[$field] = $value;

					if ($field == "userid") {
						$dataAdditional =  array("UserID" => $value, 
											"Password" => md5("123456"), 
											"AuditedUser" => "Import", 
											"AuditedDate" => date("Y-m-d H:i:s"), 
											"AuditedActivity" => "A");
						foreach ($tables as $key => $table) {
							if ($table == "MsUser") {
								if ($field == "userid") {
									$users = $this->db->get_where($table, array("UserID" => $value));
									if ($users->num_rows() == 0)
										$this->db->insert($table, $dataAdditional);
								}
							}
						}
					}
				}
				$data["AuditedUser"] = "Import";
				$data["AuditedDate"] = date("Y-m-d H:i:s");
				$data["AuditedActivity"] = "A";

				if (! $this->db->insert($table_insert, $data)) {
					$duplicate_error .= reset($data) . "; ";
				}
			}
			if ($duplicate_error != "")
				$error["error"] = "Insert Failed with id " . $duplicate_error;

			unlink($inputFileName);
			delete_files('./media/attachment/document/', TRUE);
		}

		if (isset($error) && count($error) > 0) {
			$this->session->set_flashdata('ImportData', json_encode(array("message"=> $error['error'])));
			$this->output->set_output(json_encode(array("status"=> "error", "message"=> $error['error'])));
		}
		else {
			$this->session->set_flashdata('ImportData', 1);
			$this->output->set_output(json_encode(array("status"=> "OK")));
		}
	}

	function getCellFormat($excel, $cell) {
		return $excel->getCell($cell)->getParent()->getStyle($excel->getCell($cell)->getCoordinate())->getNumberFormat()->getFormatCode();
	}
}