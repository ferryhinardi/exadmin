/* Validate If Select Option value is -1 */
function validateRequiredSelect(formid, idSelect){
	var scrollid;
	$(idSelect).on("change",function(){
		if($(this).val() == -1 || $(this).val() == ""){
			$(this).addClass('errorSelect');
		}
		else{
			$(this).removeClass('errorSelect');
			$(this).addClass('valid');
		}
		var classError = $(idSelect).siblings();
		if($(idSelect).hasClass('errorSelect')){
			if($(classError).attr('class') == undefined){
				$(idSelect).parent().append("<label for='"+idSelect+"' class='errorSelect errorDisplay' style='display: block;color: red !important;float: none !important;'>This field is required.</label>");
			}
			else{
				$(classError).css({"display":"block"});
				$(classError).addClass('errorDisplay');
			}
		}
		else if($(idSelect).hasClass('valid')){
			$(classError).css({"display":"none"});
			$(classError).removeClass('errorDisplay');
		}
	});	

	if(event != 'load')
		$(idSelect).trigger('change');

	var error = $(formid).find('label.errorDisplay');
	if(error != undefined){
		scrollid = $(error).attr('for');
		$('html, body').animate({
		    scrollTop: $(scrollid).offset().top - 100
		}, 0);
	}
}

function validateRequiredOther(formid,idSelect){
	var scrollid;
	$(idSelect).on("change",function(){
		if($(this).val() == -1 || $(this).val() == ""){
			$(this).addClass('errorSelect');
		}
		else{
			$(this).removeClass('errorSelect');
			$(this).addClass('valid');
		}
		var classError = $(idSelect).siblings();
		var errorText = $(idSelect).parent().find('.errorDisplay').attr('for');
		if($(idSelect).hasClass('errorSelect')){
			if($(idSelect).parent().find('.errorDisplay').attr('class') == undefined){
				$(idSelect).parent().append("<label for='"+idSelect+"' class='errorSelect errorDisplay' style='display: block;color: red !important;float: none !important;'>This field is required.</label>");
			}
			else{
				$(errorText).parent().find('.errorDisplay').css({"display":"block"});
				$(errorText).parent().find('.errorDisplay').addClass('errorDisplay');
			}
		}
		else if($(idSelect).hasClass('valid')){
			$(errorText).parent().find('.errorDisplay').css({"display":"none"});
			$(errorText).parent().find('.errorDisplay').removeClass('errorDisplay');
		}
		return;
	});	
	if(event != 'load')
		$(idSelect).trigger('change');

	var error = $(formid).find('label.errorDisplay');
	if(error != undefined){
		scrollid = $(error).attr('for');
		$('html, body').animate({
		    scrollTop: $(scrollid).offset().top - 100
		}, 0);
	}
}