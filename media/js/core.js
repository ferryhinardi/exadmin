var Apps = {
	"version": "0.0.0",
	"baseUrl": main_domain,
	"selector": "body",
	"prop": [],
	"item": {},
	"defaultEmployeePhoto": "/assets/img/employee/person.png",
	"timeFormat": "hh:mm:ss",
	"dateFormat": "DD-MMM-YYYY",
	"dateTimeFormat": "DD-MMM-YYYY hh:mm:ss",
	"defaultInformationMessage": "Your data has been saved.",
	"defaultErrorMessage": "Sorry, we cannot processing your request.\nPlease, try again later!",
	"Autocomplete": null
};

Apps.LoadForm = function(options) {
	$.each(options || [], function(index, element) {
		var selector = element.selector || Apps.selector + " .main",
		xtype = element.xtype || "form",
		items = element.items || [],
		form = element.name || "",
		table = element.table || [],
		toolbar = element.toolbar || [],
		files = element.files,
		html = "";

		if (typeof element.legend != 'undefined') {
			html += "<legend>"+element.legend;
			$.each(toolbar || [], function (idx, item) {
				var type = item.type || "",
					cls = item.cls || "",
					icon = item.icon || "",
					name = item.name || "",
					text = item.text || "";
					if (type == "button")
						html += "<button id='"+name+"' name='"+name+"' class='btn "+cls+"' style='margin-top:-7px; float:right;'><span class='glyphicon glyphicon-"+icon+"'></span> "+text+"</button>";
			});
			html += "<div style='clear:both'></div>"
			html += "</legend>"
		}

		html += "<form class='form-horizontal' name='"+ form +"' id='"+ form +"' "+ ((files) ? "enctype='multipart/form-data'" : "") +" >";
		$.each(items || [], function (idx, item) {
			var type = item.type || "",
				name = item.name || "",
				text = item.text || "",
				attr = item.attr || "",
				icon = item.icon,
				span = item.span || [],
				space = item.space || [];

			if (type == "spread")
				html += "<hr />";
			else {
				if (span.length == 0)	{
					html += "<div class='form-group'>";
					html += "<label for='"+name+"' class='col-sm-2 control-label'>"+((type.toLowerCase() == "button") ? "" : text)+"</label>";
					html += "<div class='col-sm-10'>";
					html += Apps.GenerateInput(item, type, name, text, icon, attr);
					html += "</div>";
					html += "</div>";
				}
				else {
					var col1 = span[0] || 2,
						col2 = span[1] || (12-col1),
						space1 = "col-md-offset-" + space[0] || "",
						space2 = "col-md-offset-" + space[1] || "";

					html += "<label for='"+name+"' class='col-sm-"+col1+" "+space1+" control-label'>"+((type.toLowerCase() == "button") ? "" : text)+"</label>";
					html += "<div class='col-sm-"+col2+" "+space2+"'>";
					html += Apps.GenerateInput(item, type, name, text, icon, attr);
					html += "</div>";
				}
			}
		});
		html += "</form>";
		
		if (typeof table != 'undefined' && table.length > 0)
			html += Apps.LoadTable(table);

		$("#"+selector).append(html);
	});
};

Apps.LoadTable = function(options) {
	var html = "";
	var onCallback = options.onCallback;
	$.each(options || [], function(index, element) {
		var selector = element.selector || "",
			items = element.items || [],
			name = element.name || "",
			toolbar = element.toolbar || [];

		if (typeof element.legend != 'undefined') {
			html += "<legend>"+element.legend;
			$.each(toolbar || [], function (idx, item) {
				var type = item.type || "",
					cls = item.cls || "",
					icon = item.icon || "",
					name = item.name || "",
					text = item.text || "";
					if (type == "button")
						html += "<button id='"+name+"' name='"+name+"' class='btn "+cls+"' style='margin-top:-7px; float:right;'><span class='glyphicon glyphicon-"+icon+"'></span> "+text+"</button>";
			});
			html += "<div style='clear:both'></div>"
			html += "</legend>"
		}

		html += "<table class='table table-striped table-bordered table-hover' name='"+name+"' id='"+name+"'>";
		$.each(items || [], function (idx, item) {
			var hidden_cell = [];
			html += "<thead>";
			html += "<tr>";
			$.each(item.header || [], function (idx2, item2) {
				if (typeof item2 == 'string' || item2 == null) {
					html += "<th>"+item2+"</th>";
				}
				else if (typeof item2 == 'object') {
					var hidden = item2.hidden,
						text = item2.text,
						attr = item2.attr || "",
						attrHeader = item2.attrHeader || "";
					if (hidden) {
						html += "<th style='display:none;'>"+item2.text+" "+attr+"</th>";
						hidden_cell.push(idx2);
					}
					else html += "<th "+attrHeader+">"+item2.text+" "+attr+"</th>";
				}
			});
			html += "</tr>";
			html += "</thead>";
			html += "<tbody>";
			$.each(item.row || [], function (idx2, item2) {
				html += "<tr>";
				$.each(item2 || [], function (idx3, item3) {
					var hidden_object = false;
					if (hidden_cell.length) {
						if (hidden_cell == idx3)
							hidden_object = true;
					}

					if (typeof item3 == 'string' || item3 == null) {
						html += "<td "+((hidden_object) ? "style='display:none;'" : '')+">"+item3+"</td>";
					}
					else if (typeof item3 == 'object') {
						var type = item3.type || "",
							name = item3.name || "",
							text = item3.text || "",
							attr = item3.attr || "",
							cell = item3.cell || "",
							icon = item3.icon;
						if (typeof item3.custom == 'undefined') {
							html += "<td "+((hidden_object) ? "style='display:none;'" : '')+" "+cell+">";
							html += Apps.GenerateInput(item3, type, name, text, icon, attr);
							html += "</td>";
						}
						else {
							html += "<td "+item3.custom+">"+item3.text+"</td>";
						}
					}
					
				});
				html += "</tr>";
			});
			html += "</tbody>";
		});
		html += "</table>";

		if (selector != "") {
			$("#"+selector).append(html);
			return;
		}
	});
	
	if (typeof onCallback === 'function')
		onCallback();

	return html;
};

Apps.Popup = function(options) {
	var name = options.name || "",
		title = options.title || "",
		body = options.body || "",
		footer = options.footer || "",
		files = options.files,
		width = options.width || "",
		html = "";
	html += "<div class='modal fade' id='"+name+"' tabindex='-1' role='dialog' aria-labelledby='"+name+"'>";
	html += "<div class='modal-dialog' role='document' style='width:"+width+"'>";
	html += "<div class='modal-content'>";
	html += "<div class='modal-header'>";
	html += "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
	html += "<h4 class='modal-title' id='"+name+"'>"+title+"</h4>";
	html += "</div>";
	html += "<div class='modal-body'>";
	if (typeof body === 'string')
		html += body;
	else if (typeof body === 'object'){
		html += "<form "+ ((files) ? "enctype='multipart/form-data'" : "") +">";
		$.each(body || [], function(index, item) {
			var type = item.type || "",
				name = item.name || "",
				text = item.text || "",
				attr = item.attr || "",
				icon = item.icon;
			if (type == "hidden")
				html += Apps.GenerateInput(item, type, name, text, icon, attr);
			else if (type == "spread")
				html += "<hr />";
			else if (type == "table") {
				html += Apps.LoadTable(item.table);
			}
			else {
				html += "<div class='form-group'>";
				html += "<label for='"+name+"' class='control-label'>"+((type.toLowerCase() == "button") ? "" : text)+"</label>";
				html += Apps.GenerateInput(item, type, name, text, icon, attr);
				if (typeof item.additional !== "undefined") {
					var object = item.additional;
					html += Apps.GenerateInput(object, object.type, object.name, "", "", "", "");
				}
				html += "</div>";
			}
		});
		html += "</form>";
	}
	html += "</div>";
	html += "<div class='modal-footer'>";
	html += footer;
	html += "</div>";
	html += "</div>";
	html += "</div>";
	html += "</div>";

	$("body").append(html);
};

Apps.GenerateInput = function(item, type, name, text, icon, attr) {
	var html = "";
	var value = item.value || "";
	var cls = item.cls || "";
	if (type.toLowerCase() == 'radio') {
		$.each(item.items || [], function (idx2, item2) {
			var text2 = item2.text,
				value = item2.value,
				checked = (idx2 == 0) ? "checked" : "";
			html += "<label class='radio-inline'>";
			html += "<input type='" +type+ "' name='"+name+"' id='"+name+"' value="+value+" "+attr+" "+checked+" /> " + text2;
			html += "</label>"
		});
	}
	else if (type.toLowerCase() == 'checkbox') {
		$.each(item.items || [], function (idx2, item2) {
			var text2 = item2.text || "",
				value = item2.value || "";
			if (text == "")
				html += "<input type='" +type+ "' name='"+name+"' id='"+name+"' value="+value+" "+attr+" /> " + text2;
			else {
				html += "<label class='checkbox-inline'>";
				html += "<input type='" +type+ "' name='"+name+"' id='"+name+"' value="+value+" "+attr+" /> " + text2;
				html += "</label>"
			}
		});
	}
	else if (type.toLowerCase() == 'select') {
		html += "<select class='form-control' name='"+name+"' id='"+name+"' "+attr+">"
		$.each(item.items || [], function (idx2, item2) {
			var value = item2.value || "",
				text = item2.text || "";
			html += "<option value='"+value+"'>"+text+"</option>"
		});
		html += "</select>"
	}
	else if (type.toLowerCase() == 'button') {
		var typeButton = item.typeButton || "",
			text = item.text || "",
			name = item.name || "",
			mode = item.mode || "primary",
			attr = item.attr || "",
			items = item.items || [];
		if (typeButton == "button")
			html += "<button name='"+name+"' id='"+name+"' class='btn btn-"+mode+"' "+attr+">"+text+"</button> &nbsp;"
		else if (items.length > 0){
			$.each(items || [], function(index, item) {
				var typeButton = item.typeButton || "",
					text = item.text || "",
					name = item.name || "",
					mode = item.mode || "primary",
					attr = item.attr || "";
				html += "<input type='"+typeButton+"' name='"+name+"' id='"+name+"' class='btn btn-"+mode+"' value='"+text+"' "+attr+" /> &nbsp;"
			});
		}
	}
	else if (type.toLowerCase() == 'upload') {
		html += "<div style='left: 2%;'>";
		if (typeof item.default == 'undefined' || item.default == false)
			html += "<img src='' class='uploadFinder' id='"+name+"' style='height: 150px; width: 150px; padding-bottom: 10px;' /><br />";
		else {
			var dirDefaultPhoto = main_domain + "/media/attachment/default-user-image.png";
			var dirPhoto = item.photo || "";
			html += "<img src='"+((dirPhoto != "") ? main_domain + "/media/attachment/" + dirPhoto : dirDefaultPhoto)+"' class='uploadFinder' id='"+name+"' style='height: 150px; width: 150px; padding-bottom: 10px;' /><br />";
		}
		html += "<div id='fine-uploader-basic' class='btn btn-primary'>Browse</div>";
		html += "</div>";
		html += "<input type='hidden' class='uploadTemp' name='"+name+"' id='"+name+"' />";
	}
	else if (type.toLowerCase() == 'textarea')
		html += "<textarea class='form-control' id='"+name+"' name='"+name+"' rows='"+((typeof item.row == 'undefined') ? 3 : item.row)+"' "+attr+">"+text+"</textarea>";
	else if (type.toLowerCase() == 'datepicker') {
		if (typeof icon == 'undefined' || icon == true) {
			html += "<div class='input-group date datepicker' id='div-"+name+"' "+attr+">";
			html += "<input type='text' class='form-control' id='"+name+"' />";
			html += "<span class='input-group-addon'>";
			html += "<span class='glyphicon glyphicon-calendar'></span>";
			html += "</span>";
			html += "</div>";
		}
		else {
			html += "<input type='text' class='form-control datepicker' id='"+name+"' name='"+name+"' placeholder='"+text+"' "+attr+" />";
		}
	}
	else if (type.toLowerCase() == 'timepicker') {
		if (typeof icon == 'undefined' || icon == true) {
			html += "<div class='input-group date timepicker' id='div-"+name+"' "+attr+">";
			html += "<input type='text' class='form-control' id='"+name+"' />";
			html += "<span class='input-group-addon'>";
			html += "<span class='glyphicon glyphicon-time'></span>";
			html += "</span>";
			html += "</div>";
		}
		else {
			html += "<input type='text' class='form-control timepicker' id='"+name+"' name='"+name+"' placeholder='"+text+"' "+attr+" />";
		}
	}
	else if (type.toLowerCase() == 'label') {
		var label = item.label || "";
		html += "<label class='form-control' name='"+name+"' id='"+name+"'>"+label+"</label>";
	}
	else if (type.toLowerCase() == 'autocomplete') {
		Apps.dataAutocomplete = item.data || [];
		Apps.eventAutocomplete = item.event;
		html += "<input type='text' class='typeahead form-control' name='"+name+"' id='"+name+"' placeholder='"+text+"' "+attr+" />";
	}
	else if (type.toLowerCase() == 'link') {
		var link = item.link || "",
			linkText = item.linkText;
		html += "<a href='"+link+"' name='"+name+"' id='"+name+"'>"+linkText+"</a>";
	}
	else
		html += "<input class='form-control "+cls+"' type='" +type+ "' name='"+name+"' id='"+name+"' placeholder='"+text+"' "+attr+" value='"+value+"' />";

	return html;
};

Apps.Datatable = function(options) {
	var selector = options.selector,
		paging = options.paging,
		sort = options.sort,
		onCallback = options.onCallback;

	if (typeof selector != 'undefined') {
		var oTable = $(selector).DataTable({
			"bRetrieve": true,
			"paging": paging,
			"bSort": sort,
			"fnDrawCallback": function() {
				$("tbody tr.selected").removeClass('selected');
				$("tbody tr").click(function() {
					$("tbody tr.selected").removeClass('selected');
					$(this).addClass('selected');
				});
				$("table").css('width', '100%');

				if (typeof onCallback == 'function')
					onCallback();
			}
		});
	}
}

Apps.DataTableServer = function(options) {
	var selector = options.selector,
		url = options.url,
		onCallback = options.onCallback,
		param = options.param,
		field = options.field || [],
		option = {},
		html = "",
		onDeclare = options.onDeclare,
		toolbar = options.toolbar || [];

	option = {
		"retrieve": true,
		"bAutoWidth": true,
		"bProcessing": true,
		"bServerSide": false,
		"sAjaxSource": url,
		"fnServerParams": function ( aoData ) {
			if (typeof param !== 'undefined' && typeof param == 'object') {
				if (param.length) {
					$.map(param, function(item, index) {
						aoData.push(item);
					})
				}
				else
					aoData.push(param);
			}
		},
		"fnServerData": function ( sSource, aoData, fnCallback, oSettings ) {
			oSettings.jqXHR = $.ajax({
				"dataType": 'json',
				"type": "POST",
				"url": sSource,
				"data": aoData,
				"success": fnCallback
			});
		},
		"columns": field,
		"fnDrawCallback": function() {
			$("table").css('width', '100%');
			if (typeof onCallback == 'function')
				onCallback();
		}
	};

	if ($(selector).prop("tagName") != 'TABLE') {
		if (typeof options.legend != 'undefined') {
			html += "<legend>"+options.legend;
			$.each(toolbar || [], function (idx, item) {
				var type = item.type || "",
					cls = item.cls || "",
					icon = item.icon || "",
					name = item.name || "",
					text = item.text || "";
					if (type == "button")
						html += "<button id='"+name+"' name='"+name+"' class='btn "+cls+"' style='margin-top:-7px; float:right;'><span class='glyphicon glyphicon-"+icon+"'></span> "+text+"</button>";
			});
			html += "<div style='clear:both'></div>"
			html += "</legend>"
		}
		$(selector).append(html);
		$(selector).append('<table class="table table-striped table-bordered table-hover"></table>');
		if (typeof onDeclare == 'undefined' || onDeclare == false)
			Apps.oTable = $(selector + " table").DataTable(option);
		else
			return $(selector + " table").DataTable(option);
	} 
	else {
		if (typeof onDeclare == 'undefined' || onDeclare == false)
			Apps.oTable = $(selector).DataTable(option);
		else
			return $(selector).DataTable(option);
	}
}

Apps.Board = function(options) {
	var selector = options.selector,
		item = options.items || {},
		html = "";

	html += "<div class='well well-lg'>";
		html += "<div id='container'>";
			html += "<div id='board-ratio'>";
				html += "<div id='board'>";
					html += "<div id='board-header'>";
						html += "<div id='title'>"+(item.title || "")+"</div>";
						html += "<div id='subtitle'>";
							html += "<div class='label-subtitle'>"+(item.subtitle || "")+"</div>";
							html += "<div id='timer'></div>";
						html += "</div>";
					html += "</div>";
					html += "<div id='board-content' class='well well-lg'>";
						html += "<div id='case'>";
							html += "<div id='simulation-wrapper'>";
								html += "<div class='wrapper-case'>";
									html += "<div class='label-case no-case'>"+ (item.no || "") +"</div>";
									html += "<div class='case-exam'>";
										if (typeof item.questionImage !== 'undefined')
											html += "<img src='" + item.questionImage + "' width='100%' height='100%' />";
										html += (item.question || "");
									html += "</div>";
								html += "</div>";
								$.map(item.options || [], function(val, i) {
									html += "<div class='wrapper-case'>";
										html += "<div class='label-case no-case-"+val.Option+"'>"+val.Option+"</div>";
										html += "<div class='case-exam'>"
											if (val.FileID != null)
												html += "<img src='" + main_domain + val.File[0].FileURL + "' width='100%' height='100%' />";
											html += (val.Description || "");
										html += "</div>";
									html += "</div>";
								});
							html += "</div>";
						html += "</div>";
						html += "<div id='action'>";
							for (var i = 1; i <= Apps.prop.length; i+=2) {
								html += "<div class='wrapper-answer'>";
									html += "<div class='container-answer'>";
										html += "<div class='odd-answer'>";
											html += "<div class='label-no-answer no-" + i +"'>" + i + "</div>";
											html += "<div class='label-user-answer ans-none'>" + " - " + "</div>";
										html += "</div>";
										if (Apps.prop.length >= (i+1)) {
											html += "<div class='even-answer'>";
												html += "<div class='label-no-answer no-" + (i+1) +"'>" + (i+1) + "</div>";
												html += "<div class='label-user-answer ans-none'>" + " - " + "</div>";
											html += "</div>";
										}
									html += "</div>";
								html += "</div>";
							}
							html += "<input class='wrapper-answer btn-exam' id='btnPass' type='button' value='Pass' />";
							html += "<input class='wrapper-answer btn-exam' id='btnFinish' type='button' value='Finish' />";
						html += "</div>";
					html += "</div>";
				html += "</div>";
			html += "</div>";
		html += "</div>";
	html += "</div>";

	if (typeof selector === 'undefined')
		return html;
	else
		$("#"+selector).append(html);
}

Apps.Container = function(options) {
	var selector = options.selector,
		items = options.items || [],
		html = "";

	html += "<div class='well well-lg'>";
	html += Apps.Thumbnail(items);
	html += "</div>";
	if (typeof selector === 'undefined')
		return html;
	else
		$("#"+selector).append(html);
}

Apps.Thumbnail = function(items) {
	var html = "";
	$.each(items || [], function (idx, item) {
		if (idx % 4 == 0) 
			html += "<div class='row'>"
		if (typeof item.image != 'undefined')
			html += "<img src="+item.image.src+" "+(item.image.attr || "")+">";
		html += "<div class='col-sm-6 col-md-3'>";
		html += "<div class='thumbnail " + item.cls + " "+((item.selected == true) ? "thumbnail-active" : "")+"' "+ (item.attr || "") +" data-attr="+(item.data_attr || "")+">";
		html += "<div class='caption'>";
		html += (item.content || "");
		html += "</div>";
		html += "</div>";
		html += "</div>";
		if (idx % 4 == 3) 
			html += "</div>";
	});
	return html;
}

function AutoComplete (element) {
	var bloodhound, flag = false, event;
	if (typeof Apps.eventAutocomplete == 'function') event = Apps.eventAutocomplete;
	if (typeof Apps.dataAutocomplete !== 'undefined') {
		if (Apps.dataAutocomplete.length > 0) {
			if (typeof Apps.dataAutocomplete[0].url == 'undefined') {
				bloodhound = new Bloodhound({
					datumTokenizer: Bloodhound.tokenizers.whitespace,
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					local: Apps.dataAutocomplete
				});
			}
			else {
				bloodhound = new Bloodhound({
					datumTokenizer: function (datum) {
						return Bloodhound.tokenizers.whitespace(datum.value);
					},
					queryTokenizer: Bloodhound.tokenizers.whitespace,
					limit: 10,
					remote: {
						url: main_domain+'/index.php/menu/getAllMenu',
						filter: filterResponse
					}
				});
				flag = true;
			}
		}
		else {
			bloodhound = new Bloodhound({
				datumTokenizer: Bloodhound.tokenizers.whitespace,
				queryTokenizer: Bloodhound.tokenizers.whitespace,
				local: []
			});
		}
		bloodhound.initialize();
	}	

	if (flag) {
		$(element).typeahead({
			highlight: true,
			minLength: 1
		},
		{
			name: 'value',
			displayKey: 'value',
			source: bloodhound.ttAdapter(),
			templates: {
				empty: [
					'<div class="tt-suggestion">',
					'No Items Found',
					'</div>'
				].join('\n')
			}
		}).on("typeahead:selected", function (e, datum) {
			event();
		});
	}
	else {
		$(element).typeahead({
			highlight: true,
			minLength: 1
		},
		{
			source: bloodhound,
			templates: {
				empty: [
					'<div class="tt-suggestion">',
					'No Items Found',
					'</div>'
				].join('\n')
			}
		}).on("typeahead:selected", function (e, datum) {
			event();
		});
	}

	function filterResponse(parsedResponse) {
		var dataset = [];
		if (parsedResponse.status == "OK") {
			parsedResponse = parsedResponse.data;
			for (var i = 0; i < parsedResponse.length; i++) {
				dataset.push({
					id: parsedResponse[i].MenuID,
					value: parsedResponse[i].MenuName
				});
			}
		}
		else {
			dataset.push({ id: "-1", value: "No results" });
		}

		return dataset;
	}
}