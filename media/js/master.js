$(window).on('load', function() {
	$(".preloader").on('ajaxSend', function() { });
	$(".preloader").on('ajaxComplete', function() { });

	/* Menu Toggle Script */	
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#wrapper").toggleClass("toggled");
	});

	//Initialize Photo Uploader
	 new qq.FileUploaderBasic({
		button: $("#fine-uploader-basic")[0],
		action: main_domain+'/index.php/ajaxserver/douploadimage',
		debug: false,
		allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
		sizeLimit: 2048000,
		forceMultipart:true,
		onUpload: function() { $(".preloader").trigger("ajaxSend"); },
		onComplete: function(id, fileName, data) {
			if(data.status == '1'){
				$("img.uploadFinder").attr('src',main_domain+'/media/attachment/userpicture/'+data.name);  
			}
			$("input.uploadTemp").val(data.name);
			$(".preloader").trigger("ajaxComplete");
		}
	});

	$(".datepicker").datetimepicker({
		defaultDate: moment(new Date()),
		format: Apps.dateFormat
	});

	$(".timepicker").datetimepicker({
		defaultDate: moment(new Date()),
		format: Apps.timeFormat,
	});

	$('ul.sidebar-nav a').click(function(e) {
		e.preventDefault();
		var obj = $(this).attr('href');
		$('html, body').animate({
			scrollTop: $(obj).offset().top - 100
		}, 'slow');
	});

	$(".tempNotification").fadeOut(10000);

	if (typeof $(".typeahead") === 'object') {
		var id = "#" + $(".typeahead").attr('id');
		AutoComplete(id);
	}

	$(document).on("click", ".thumbnail", function(e) {
		e.preventDefault();
		if (!$(this).hasClass('no_select')) {
			if ($(this).hasClass('thumbnail-active')) 
				$(this).removeClass('thumbnail-active');
			else
				$(this).addClass('thumbnail-active');
		}
	});
});